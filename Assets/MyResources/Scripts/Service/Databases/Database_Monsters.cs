﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using LitJson;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class Database_Monsters : Database
{
    public static Database_Monsters Instance { get; private set; }
    private Monsters database_Monsters;
    private JsonData json_monsterData;
    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        Initialise_Monsters_Database();
    }

    public Monster GetMonster(string name)
    {
        return  database_Monsters.monsters.FirstOrDefault(t => t.MonsterName == name);
    }
    public Monster RandomMonster()
    {
        int currentLevel = Main_Manager.Instance.levelManager.currentLevel - 1;

        List<Monster> monsters = database_Monsters.monsters;
        List<Monster> sortedMonsters = new List<Monster>();

        for (int i = 0; i < monsters.Count; i++)
        {
            if (currentLevel.IsBetween(monsters[i].spawnRange.min,monsters[i].spawnRange.max - 1))
                sortedMonsters.Add(monsters[i]);
        }
        Monster monster = sortedMonsters[Random.Range(0, sortedMonsters.Count)];

        SaveSystem.Instance.currentMonster = monster;
        return monster;
    }

    private void InitMonsters()
    {
        database_Monsters = new Monsters(

            new Monster("Unslaved Warrior", 1500, 140, 5, 30, 250, 250, 3, 4, new MinMax(0, 15),
                new List<string> {"Decrepify", "Soul Cry", "Unholy Smite"},
                new List<string> {"Unholy Heal"}, 60,

                new Reward("Magic Rune", 30, 1, "Rune"),
                new Reward("Health Potion", 10, 2),
                new Reward("Rune Scroll", 10, 2),
                new Reward("Gold Ring", 100, 3),
                new Reward("Emerald", 25, 3),
                new Reward("Heart Piercer", 5, 1)
            ),

            new Monster("Raised Skeleton", 1500, 140, 20, 35, 300, 250, 3, 4, new MinMax(0, 20),
                new List<string> {"Decrepify", "Unholy Smite", "Decay"},
                new List<string> {"Unholy Heal"}, 60,

                new Reward("Magic Rune", 30, 1, "Rune"),
                new Reward("Health Potion", 10, 2),
                new Reward("Rune Scroll", 10, 2),
                new Reward("Gold Ring", 100, 3),
                new Reward("Emerald", 50, 3),
                new Reward("Steel Sword", 6, 1)),

            new Monster("Decaying Corpse", 2250, 160, 10, 35, 300, 300, 2, 4, new MinMax(5, 25),
                new List<string> {"Withering", "Soul Cry"},
                new List<string> {"Agony"}, 40,

                new Reward("Magic Rune", 30, 1, "Rune"),
                new Reward("Health Potion", 10, 2),
                new Reward("Rune Scroll", 10, 2),
                new Reward("Gold Ring", 60, 3),
                new Reward("Emerald", 100, 3),
                new Reward("Bloodstealer", 5, 1)),

            new Monster("Necromancer", 3000, 300, 10, 45, 250, 425, 3, 5, new MinMax(15, 30),
                new List<string> {"Withering", "Exhaust"},
                new List<string> {"Life Tap"}, 70,

                new Reward("Magic Rune", 30, 1, "Rune"),
                new Reward("Health Potion", 10, 2),
                new Reward("Rune Scroll", 10, 2),
                new Reward("Diamond", 40, 3),
                new Reward("Emerald", 100, 3),
                new Reward("Staff of Wizardry", 5, 1)),

            new Monster("Cursed Knight", 3000, 200, 30, 60, 325, 225, 3, 4, new MinMax(15, 30),
                new List<string> {"Unholy Smite", "Decay"},
                new List<string> {"Life Tap", "Unholy Heal"}, 60,

                new Reward("Magic Rune", 30, 1, "Rune"),
                new Reward("Health Potion", 10, 2),
                new Reward("Rune Scroll", 10, 2),
                new Reward("Diamond", 60, 3),
                new Reward("Emerald", 100, 3),
                new Reward("Sword of Power", 5, 1)),

            new Monster("Septavus The Lich", 7500, 300, 20, 60, 325, 550, 2, 4, new MinMax(30, 31),
                new List<string> {"Exhaust", "Soul Torment"},
                new List<string> {"Life Tap"}, 60,

                new Reward("Staff of Darkness", 100, 1),
                new Reward("Diamond", 100, 3),
                new Reward("Diamond", 100, 3),
                new Reward("Diamond", 100, 3),
                new Reward("Diamond", 100, 3))
        );

    }

    //Загрзука БД в Unity
    public void Initialise_Monsters_Database()
    {
        string file;

        if (Application.platform == RuntimePlatform.Android)
        {
            file = Application.persistentDataPath + "/" + "Monsters" + ".json";

            if (!File.Exists(file) || File.ReadAllText(file) == "")
            {
                InitMonsters();
                File.WriteAllText(file, PrettyPrint(database_Monsters));
            }
            else
            {
                json_monsterData = JsonMapper.ToObject(
                    File.ReadAllText(Application.persistentDataPath + "/" + "Monsters" + ".json"));

                database_Monsters = JsonMapper.ToObject<Monsters>(json_monsterData.ToJson());
            }

        }
        else
        {
            file = Application.dataPath + "/" + "Monsters" + ".json";

            if (!File.Exists(file))
            {
                InitMonsters();

                File.WriteAllText(Application.dataPath + "/" + "Monsters" + ".json", PrettyPrint(database_Monsters));
            }
            else
            {
                json_monsterData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/" + "Monsters" + ".json"));
                database_Monsters = JsonMapper.ToObject<Monsters>(json_monsterData.ToJson());
            }

        }
    }
}
public class Monsters
{
    public List<Monster> monsters = new List<Monster>();

    public Monsters(params Monster[] monsters)
    {
        this.monsters = monsters.ToList();
    }

    public Monsters()
    {

    }
}
public class Monster
{
    #region Variables
    public string MonsterName { get; set; }
    public List<Stat> stats;
    public double fightTime { get; set; }
    public List<string> regularSpellPool = new List<string>();
    public List<string> threshholdSpellPool = new List<string>();
    public int threshHoldPerecent;
    public List<Reward> Rewards = new List<Reward>();
    public MinMax spawnRange { get; set; }
    private Dictionary<MonsterSprite, Sprite> Sprites;
    #endregion

    //array of monster id's represents the inventory
    public Monster(string monsterName, int health, int mana,
        int defence, int damage, int power, int magic, int crit,
        double fightTime, MinMax spawnRange, List<string> regularSpellPool, List<string> threshholdSpellPool, int threshHoldPerecent, params Reward[] rewards)
    {

        MonsterName = monsterName;
        this.fightTime = fightTime;
        this.spawnRange = spawnRange;
        this.regularSpellPool = regularSpellPool;
        this.threshholdSpellPool = threshholdSpellPool;
        this.threshHoldPerecent = threshHoldPerecent;
        Rewards = rewards.ToList();


        stats = new List<Stat>
        {
            new Stat(health, 0, "Health"),
            new Stat(mana, 0, "Mana"),
            new Stat(damage, 5, 1000, "Damage"),
            new Stat(defence, int.MinValue, 100, "Defence"),
            new Stat(power, 100, 1000, "Power"),
            new Stat(magic, 100, 1000, "Magic"),
            new Stat(crit, 1, crit, "Crit")
        };
    }

    public Monster(Monster monster)
    {
        MonsterName = monster.MonsterName;
        fightTime = monster.fightTime;
        Rewards = monster.Rewards;

        regularSpellPool = monster.regularSpellPool;
        threshholdSpellPool = monster.threshholdSpellPool;
        threshHoldPerecent = monster.threshHoldPerecent;

        stats = new List<Stat>
        {
            new Stat(monster.GetStat("Health").baseValue, 0, "Health"),
            new Stat(monster.GetStat("Mana").baseValue, 0, "Mana"),
            new Stat(monster.GetStat("Damage").baseValue, 5, int.MaxValue, "Damage"),
            new Stat(monster.GetStat("Defence").baseValue, int.MinValue, 100, "Defence"),
            new Stat(monster.GetStat("Power").baseValue, 100, int.MaxValue, "Power"),
            new Stat(monster.GetStat("Magic").baseValue, 100, int.MaxValue, "Magic"),
            new Stat(monster.GetStat("Crit").baseValue, 0, monster.GetStat("Crit").baseValue, "Crit")
        };
    }

    public Monster()
    {
    }

    public Stat GetStat(string name)
    {
        return stats.FirstOrDefault(t => t.name == name);
    }
}
public class Reward
{
    public string itemName;
    public int chanceToGet;
    public int priority;
    public string type;
    public Reward(string itemName, int chanceToGet, int priority)
    {
        this.itemName = itemName;
        this.chanceToGet = chanceToGet;
        this.priority = priority;
    }
    public Reward(string itemName, int chanceToGet, int priority,string type)
    {
        this.itemName = itemName;
        this.chanceToGet = chanceToGet;
        this.priority = priority;
        this.type = type;
    }

    public string GetType()
    {
        return type;
    }
    public Reward()
    {

    }
}