﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Database_Items : Database
{
    public static Database_Items Instance { get; private set; }
    private static Items database_items;
    private JsonData json_itemData;

    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        Initialise_Items_Database();

    }

//Поиск предмета по itemID в базе данных предметов внутри Unity
    public static Item GetItemByID(int itemID)
    {
        return new Item(database_items.items.FirstOrDefault(t => t.id == itemID));
    }

    public static Item GetItemByName(string itemName)
    {
        return new Item(database_items.items.FirstOrDefault(t => t.itemName == itemName));
    }

    //Загрзука БД в Unity
    public void Initialise_Items_Database()
    {
        string file;

        if (Application.platform == RuntimePlatform.Android)
        {
            file = Application.persistentDataPath + "/" + "Items" + ".json";
            if (!File.Exists(file) || File.ReadAllText(file) == "")
            {
                database_items = new Items();
                SetItems();

                File.WriteAllText(file, PrettyPrint(database_items));
            }
            else
            {
                Debug.Log(file);

                json_itemData = JsonMapper.ToObject(
                    File.ReadAllText(Application.persistentDataPath + "/" + "Items" + ".json"));

                database_items = JsonMapper.ToObject<Items>(json_itemData.ToJson());
            }

        }
        else
        {
            file = Application.dataPath + "/" + "Items" + ".json";

            if (!File.Exists(file))
            {
                SetItems();
                File.WriteAllText(Application.dataPath + "/" + "Items" + ".json", PrettyPrint(database_items));
            }
            else
            {
                json_itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/" + "Items" + ".json"));
                database_items = JsonMapper.ToObject<Items>(json_itemData.ToJson());
            }

        }
    }

    private void SetItems()
    {
        database_items = new Items();

        database_items.items = new List<Item>
        {
            new Item(1, "Rune Scroll", "Scroll", "Scroll", "Use to identify rune", 200),
            new Item(2, "Health Potion", "Potion", "Potion", "Restore 50% health", 500),
            new Item(3, "Magic Rune", "Magic Rune", "Rune", "Mysterious Rune", 750, Rarity.Magic),
            new Item(4, "Enchanted Rune", "Enchanted Rune", "Rune", "Mysterious Rune", 1500, Rarity.Enchanted),
            new Item(5, "Sacred Rune", "Sacred Rune", "Rune", "Mysterious Rune", 3000, Rarity.Sacred),
            new Item(6, "Gold Ring", "Gold Ring", "Treasure", "Use to get 100 gold", 100),
            new Item(7, "Emerald", "Emerald", "Treasure", "Use to get 250 gold", 250),
            new Item(8, "Diamond", "Diamond", "Treasure", "Use to get 500 gold", 500),
            new Item(8, "Runic Book", "Runic Book", "Book", "Unlocks Rune Slot", 2000),

            new Item(9, "Board Sword", "Board Sword", "Weapon", "Spell Stealers Friend", 0,
                new Property("Damage", 15, PropertyType.Passive, AdjustType.Point),
                new Property("Attack Speed", 4, PropertyType.Use, AdjustType.Point)),

            new Item(10, "Steel Sword", "Steel Sword", "Weapon", "", 0,
                new Property("Damage", 20, PropertyType.Passive, AdjustType.Point),
                new Property("Attack Speed", 5, PropertyType.Use, AdjustType.Point)),

            new Item(10, "Sword of Power", "Sword of Power", "Weapon", "", 7500,
                new Property("Damage", 20, PropertyType.Passive, AdjustType.Point),
                new Property("Attack Speed", 4, PropertyType.Use, AdjustType.Point),
                new Property("Power", 50, PropertyType.Passive, AdjustType.Point, true)),

            new Item(11, "Bloodstealer", "Bloodstealer", "Weapon", "", 0,
                new Property("Damage", 25, PropertyType.Passive, AdjustType.Point),
                new Property("Attack Speed", 4, PropertyType.Use, AdjustType.Point),
                new Property("Health", 5, PropertyType.Crit, AdjustType.Percentage,true),
                new Property("Health", -20, -20, PropertyType.Passive, AdjustType.Percentage,false)),
                            
            new Item(12, "Staff of Wizardry", "Staff of Wizardry", "Weapon", "", 0,
                new Property("Damage", 10, PropertyType.Passive, AdjustType.Point),
                new Property("Attack Speed", 3, PropertyType.Use, AdjustType.Point),
                new Property("Magic", 100, PropertyType.Passive, AdjustType.Point, true),                
                new Property("Charges", 1, PropertyType.Passive, AdjustType.Point)),

            new Item(13, "Staff of Darkness", "Staff of Darkness", "Weapon", "", 0,
                new Property("Damage", 15, PropertyType.Passive, AdjustType.Point),
                new Property("Attack Speed", 3, PropertyType.Use, AdjustType.Point),
                new Property("Mana", 40, PropertyType.Passive, AdjustType.Point),
                new Property("Magic", 200, PropertyType.Passive, AdjustType.Point, true)),

            new Item(14, "Heart Piercer", "Rapier", "Weapon", "", 0,
                new Property("Damage", 9, PropertyType.Passive, AdjustType.Point),
                new Property("Attack Speed", 8, PropertyType.Use, AdjustType.Point),
                new Property("Crit", -4, -4, PropertyType.Passive, AdjustType.Point, false))
        };
    }
}

public class Items
{
    public List<Item> items = new List<Item>();
    
    public Items()
    {

    }
}
//Класс предмета в Unity
public class Item
{
    public int id { get; set; }
    public string itemName { get; set; }
    public string spriteName { get; set; }
    public string itemType { get; set; }   
    public string description { get; set; }
    public int price { get; set; }
    public List<Property> properties { get; set; }
    public Rarity rarity { get; set; }

    public Item(int id, string itemName, string spriteName, string itemType, string description, int price, params Property[] properties)
    {
        this.id = id;
        this.itemName = itemName;
        this.spriteName = spriteName;
        this.itemType = itemType;
        this.description = description;
        this.price = price;
        this.properties = properties.ToList();
    }
    public Item(int id, string itemName, string spriteName, string itemType, string description, int price,Rarity rarity, params Property[] properties)
    {
        this.id = id;
        this.itemName = itemName;
        this.spriteName = spriteName;
        this.itemType = itemType;
        this.description = description;
        this.price = price;
        this.properties = properties.ToList();
        this.rarity = rarity;
    }
    public Item(Item item)
    {
        this.id = item.id;
        this.itemName = item.itemName;
        this.spriteName = item.spriteName;
        this.itemType = item.itemType;
        this.description = item.description;
        this.price = item.price;
        properties = new List<Property>();
        for (int i = 0; i < item.properties.Count; i++)
        {
            properties.Add(new Property(item.properties[i]));
        }
        this.rarity = item.rarity;
    }
    //Конструктор пустого предмета
    public Item()
    {
        id = 0;
    }

    public virtual void Use(bool active,PropertyType type)
    {
        List<Property> sortedProperties = GetProperties(type);
        for (int i = 0; i < sortedProperties.Count; i++)
        {
            sortedProperties[i].Apply(active);
        }
    }

    public List<Property> GetProperties(PropertyType type)
    {
        return properties.Where(t => t.propertyType == type).ToList();
    }

    public Property GetProperty(string name)
    {
        return properties.FirstOrDefault(t => t.name == name);
    }

    public Property GetProperty(string name, PropertyType type)
    { 
        return properties.FirstOrDefault(t => t.name == name && t.propertyType == type);
    }
    public bool ContainsProperties()
    {
        return properties.Count > 0;
    }
}
public class Property
{
    public string name;
    public int value;
    public PropertyType propertyType;
    public AdjustType adjustType;
    public bool drawPercents;

    public Target target { get; private set; }
    public Property(string name, int minValue,int maxValue,PropertyType propertyType, AdjustType adjustType,bool multiply,bool drawPercents = false) 
    {
        this.name = name;
        if (multiply)
        {
            int difference = maxValue - minValue;

            int randIndex = Random.Range(0, difference / 5 + 1);

            value = randIndex * 5 + minValue;
        }
        else
            value = Random.Range(minValue, maxValue + 1);
        this.drawPercents = drawPercents;
        this.propertyType = propertyType;
        this.adjustType = adjustType;
        target = Target.Player;
    }
    public Property(string name, int value, PropertyType propertyType, AdjustType adjustType,bool drawPercents = false)
    {
        this.name = name;
        this.value = value;
        this.drawPercents = drawPercents;
        this.propertyType = propertyType;
        this.adjustType = adjustType;
        target = Target.Player;
    }
    public Property(string name, int minValue, int maxValue, PropertyType propertyType, AdjustType adjustType,Target target,bool drawPercents = false)
    {
        this.name = name;
        value = Random.Range(minValue, maxValue + 1);
        this.propertyType = propertyType;
        this.adjustType = adjustType;
        this.target = target;
        this.drawPercents = drawPercents;
    }
    public Property(string name, int value,AdjustType adjustType)
    {
        this.name = name;
        this.value = value;
        propertyType = PropertyType.Use;
        this.adjustType = adjustType;
        target = Target.Player;
    }

    public Property(Property property)
    {
        name = property.name;
        value = property.value;
        propertyType = property.propertyType;
        adjustType = property.adjustType;
        drawPercents = property.drawPercents;
        target = property.target;
    }
    public void Apply(bool apply)
    {
        Stat stat = target == Target.Player ? SaveSystem.GetChar().GetStat(name) : MonsterInfo.Instance.GetMonster().GetStat(name);
        Debug.Log(name);
        int adjusted = 0;

        switch (adjustType)
        {
            case AdjustType.Point:
                if (apply)
                {
                    stat.AdjustMax(value);
                    adjusted = stat.ChangeCurrent(value);
                }
                else
                {
                    stat.AdjustMax(-value);
                    stat.ChangeCurrent(-value);
                }                  
                break;
            case AdjustType.Percentage:
                
                if (apply)
                {
                    int adjustAmount = (int)((float)stat.maxValue / 100 * value);

                    stat.AdjustMax(adjustAmount);
                    adjusted = stat.ChangeCurrent(adjustAmount);
                }
                else
                {
                    stat.AdjustMax(-value);
                    stat.ChangeCurrent(-value);
                }                   
                break;
        }

        if (propertyType == PropertyType.Passive)
            value = adjusted;
    }
    public Property()
    {
        
    }
}

public class Modifier
{
    public int value { get; private set; }

    public Modifier(int value)
    {
        this.value = value;
    }
}