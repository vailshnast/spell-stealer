﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
using System.Text;

public class Database_Heroes : Database
{
    public static Database_Heroes Instance { get; private set; }
    private Heroes database_heroes;
    private JsonData json_heroData;
    
    void Awake () {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        Initialise_Heroes_Database();
      
    }
    

    //Поиск предмета по ID в базе данных предметов внутри Unity
    public Hero getHero(int id)
    {
        for (int i = 0; i < database_heroes.heroes.Count; i++)
        {
            if (database_heroes.heroes[i].ID == id)
                return database_heroes.heroes[i];
        }
        return null;

    }
    //Загрзука БД в Unity
    public void Initialise_Heroes_Database()
    {
        string file;

        if (Application.platform == RuntimePlatform.Android)
        {
            file = Application.persistentDataPath + "/" + "Heroes" + ".json";

            if (!File.Exists(file) || File.ReadAllText(file) == "")
            {
                database_heroes = new Heroes();
                SetHero();

                File.WriteAllText(file, PrettyPrint(database_heroes));
            }
            else
            {
                json_heroData = JsonMapper.ToObject(
                    File.ReadAllText(Application.persistentDataPath + "/" + "Heroes" + ".json"));

                database_heroes = JsonMapper.ToObject<Heroes>(json_heroData.ToJson());
            }

        }
        else
        {
            file = Application.dataPath + "/" + "Heroes" + ".json";

            if (!File.Exists(file))
            {
                database_heroes = new Heroes();
                SetHero();
                File.WriteAllText(Application.dataPath + "/" + "Heroes" + ".json", PrettyPrint(database_heroes));
                
                
            }
            else
            {
                json_heroData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/" + "Heroes" + ".json"));
                database_heroes = JsonMapper.ToObject<Heroes>(json_heroData.ToJson());               
            }

        }
    }

    private void SetHero()
    {
        database_heroes.heroes.Add(new Hero(0, 100, 40, 0, 0, 200, 200, 15, 0, 2, 0, 1));
    }

    public int getHeroesCount()
    {
        return database_heroes.heroes.Count;
    }

}

public class Heroes
{
    public List<Hero> heroes = new List<Hero>();
    public Heroes()
    {

    }
}
public class Hero
{
    public int ID { get; set; }
    public int Health { get; set; }
    public int Mana { get; set; }
    public int Defence { get; set; }
    public int Damage { get; set; }
    public int Power { get; set; }  
    public int Magic { get; set; }
    public int Crit { get; set; }
    public int Starting_Gold { get; set; }
    public int RuneSlots { get; set; }
    public int spellManaCost { get; set; }
    public int SpellStealCharges { get; set; }

    public Hero(int id, int health, int mana, int defence, int damage, int power, int magic, int crit, int startingGold, int runeSlots, int spellManaCost, int spellStealCharges)
    {
        ID = id;
        Health = health;
        Mana = mana;
        Defence = defence;
        Damage = damage;
        Power = power;
        Magic = magic;
        Crit = crit;
        Starting_Gold = startingGold;
        RuneSlots = runeSlots;
        this.spellManaCost = spellManaCost;
        SpellStealCharges = spellStealCharges;
    }

    public Hero()
    {

    }

}

