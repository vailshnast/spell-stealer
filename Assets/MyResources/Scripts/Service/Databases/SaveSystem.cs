﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.IO;
using TMPro;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.U2D;
using UnityEngine.UI;



public class SaveSystem : MonoBehaviour
{
    #region Variables

    public static SaveSystem Instance { get; private set; }
    public static bool isPlayerDead;
    public static CharacterStats _char { get; set; }

    public List<Item> runes = new List<Item>();
    public Monster currentMonster { get; set; }
    private Rune_System rune_System;

    [HideInInspector] public Item[] charInventory;
    [HideInInspector] public Spell current_Spell { get; private set; }
    [HideInInspector] public bool blessedByResalia { get; set; }

    [SerializeField]
    private List<SpriteAtlas>  atlases;

    private static List<Sprite[]> sprites;

    #endregion
    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        sprites = new List<Sprite[]>();
        for (int i = 0; i < atlases.Count; i++)
        {
            sprites.Add(new Sprite[atlases[i].spriteCount]);
            atlases[i].GetSprites(sprites[i]);
        }
    }

    void Start()
    {

        charInventory = new Item[10];
        Load();
    }

    #region Save_System

    private void Load()
    {
        Hero hero = Database_Heroes.Instance.getHero(0);
        CharacterStats save = Database_Saves.Instance.GetHero();
        _char = new CharacterStats();
        _char.SetStats(hero.Health, hero.Mana, hero.Damage, hero.Defence, hero.Power, hero.Magic, hero.Crit, hero.RuneSlots, hero.SpellStealCharges, hero.spellManaCost, save.GetStat("Gold").baseValue);
    }

    #endregion

    #region Get/Set

    public void SetSpell(Spell spell)
    {
        current_Spell = spell;
    }

    public void SetCharInventory(Item[] inventory)
    {
        charInventory = inventory;
    }

    public static CharacterStats GetChar()
    {
        return _char;
    }

    public void AddItemInfo(int id, int index)
    {
        for (int i = index; i < Instance.charInventory.Length; i++)
        {
            if (Instance.charInventory[i].id == 0)
            {
                Instance.charInventory[i] = Database_Items.GetItemByID(id);
                break;
            }
        }
    }

    public void AddItemInfo(Item item, int index)
    {
        for (int i = index; i < Instance.charInventory.Length; i++)
        {
            if (Instance.charInventory[i].id == 0)
            {
                Instance.charInventory[i] = item;
                break;
            }
        }
    }

    public bool IdentifyRune(Item item)
    {
        
        if (item.properties.Count > 0)
            return false;
        Debug.Log(item.itemName);
        Debug.Log(item.itemType);

        Dictionary<Property, int> properties = new Dictionary<Property, int>();

        int propertyAmount = 0;
        item.description = "";

        #region Rarity

        switch (item.rarity)
        {
            #region Magic

            case Rarity.Magic:
                 properties.Add(new Property("Health", 20, 40, PropertyType.Passive, AdjustType.Point, true), 25);
                 properties.Add(new Property("Mana", 10, 20, PropertyType.Passive, AdjustType.Point, true), 25);
                 properties.Add(new Property("Defence", 10, 15, PropertyType.Passive, AdjustType.Point, false, true), 20);
                 properties.Add(new Property("Power", 100, 150, PropertyType.Passive, AdjustType.Point, true, true), 25);
                 properties.Add(new Property("Magic", 125, 175, PropertyType.Passive, AdjustType.Point, true, true), 25);
                 properties.Add(new Property("Crit", -2, -3, PropertyType.Passive, AdjustType.Point, false), 10);
                 properties.Add(new Property("Health", 10, 10, PropertyType.Crit, AdjustType.Percentage, false, true), 5);
                 properties.Add(new Property("Mana", 10, 10, PropertyType.Crit, AdjustType.Percentage, false, true), 5);
                 properties.Add(new Property("Health", 10, 10, PropertyType.Block, AdjustType.Percentage, false, true), 5);
                 properties.Add(new Property("Mana", -5, -5, PropertyType.Crit, AdjustType.Percentage, Target.Enemy, true), 5);
                 properties.Add(new Property("Mana Cost", -3, -5, PropertyType.Passive, AdjustType.Point, false), 5);
                 properties.Add(new Property("Charges", 1, 1, PropertyType.Passive, AdjustType.Point, false), 5);
                propertyAmount = 1;
                item.itemName = "Magic Rune";
                break;

            #endregion
            
            #region Enchanted

            case Rarity.Enchanted:
                properties.Add(new Property("Health", 20, 40, PropertyType.Passive, AdjustType.Point, true), 25);
                properties.Add(new Property("Mana", 10, 30, PropertyType.Passive, AdjustType.Point, true), 25);
                properties.Add(new Property("Defence", 6, 9, PropertyType.Passive, AdjustType.Point, false, true), 20);
                properties.Add(new Property("Power", 75, 150, PropertyType.Passive, AdjustType.Point, true, true), 25);
                properties.Add(new Property("Magic", 75, 150, PropertyType.Passive, AdjustType.Point, true, true), 25);
                properties.Add(new Property("Crit", -1, -2, PropertyType.Passive, AdjustType.Point, false), 10);
                properties.Add(new Property("Health", 5, 5, PropertyType.Crit, AdjustType.Percentage, false, true), 5);
                properties.Add(new Property("Mana", 5, 5, PropertyType.Crit, AdjustType.Percentage, false, true), 5);
                properties.Add(new Property("Health", 10, 10, PropertyType.Block, AdjustType.Percentage, false, true), 5);
                properties.Add(new Property("Mana", -7, -7, PropertyType.Crit, AdjustType.Percentage, Target.Enemy, true), 5);
                properties.Add(new Property("Mana Cost", -1, -3, PropertyType.Passive, AdjustType.Point, false), 5);
                propertyAmount = 3;
                item.itemName = "Enchanted Rune";
                break;

            #endregion
            
            #region Sacred

            case Rarity.Sacred:
                properties.Add(new Property("Health", 40, 60, PropertyType.Passive, AdjustType.Point, true), 25);
                properties.Add(new Property("Mana", 25, 40, PropertyType.Passive, AdjustType.Point, true), 25);
                properties.Add(new Property("Defence", 9, 12, PropertyType.Passive, AdjustType.Point, false, true), 20);
                properties.Add(new Property("Power", 125, 200, PropertyType.Passive, AdjustType.Point, true, true), 25);
                properties.Add(new Property("Magic", 125, 200, PropertyType.Passive, AdjustType.Point, true, true), 25);
                properties.Add(new Property("Crit", -1, -2, PropertyType.Passive, AdjustType.Point, false), 10);
                properties.Add(new Property("Health", 7, 7, PropertyType.Crit, AdjustType.Percentage, false, true), 5);
                properties.Add(new Property("Mana", 7, 7, PropertyType.Crit, AdjustType.Percentage, false, true), 5);
                properties.Add(new Property("Health", 15, 15, PropertyType.Block, AdjustType.Percentage, false, true), 5);
                properties.Add(new Property("Mana", -9, -9, PropertyType.Crit, AdjustType.Percentage, Target.Enemy, true), 5);
                properties.Add(new Property("Mana Cost", -3, -5, PropertyType.Passive, AdjustType.Point, false), 5);
                properties.Add(new Property("Charges", 1, 1, PropertyType.Passive, AdjustType.Point, false), 5);
                propertyAmount = 4;
                item.itemName = "Sacred Rune";
                break;

            #endregion
        }

        #endregion

        for (int i = 0; i < propertyAmount; i++)
        {
            Property randomedProperty = WeightedRandomizer.From(properties).Random();
            item.properties.Add(randomedProperty);
            properties.Remove(randomedProperty);
        }
        for (int i = 0; i < item.properties.Count; i++)
        {
            string sign = item.properties[i].value > 0 ? "+" : "";
            string percents = item.properties[i].drawPercents ? "%" : "";

            switch (item.properties[i].propertyType)
            {
                case PropertyType.Passive:
                    item.description += sign + item.properties[i].value + percents + " " + item.properties[i].name + "\n";
                    break;
                case PropertyType.Crit:
                    item.description += sign + item.properties[i].value + percents + " " + item.properties[i].target + " " + item.properties[i].name + " After Crit\n";
                    break;
                case PropertyType.Block:
                    item.description += sign + item.properties[i].value + percents + " " + item.properties[i].target + " " + item.properties[i].name + " After Block\n";
                    break;
            }
        }
        return true;
    }

    public bool PotionExists()
    {
        for (int i = 0; i < 10; i++)
        {
            if (charInventory[i]!=null && charInventory[i].itemName == "Health Potion")
                return true;
        }
        return false;
    }
    public void DestroyPotion()
    {
        for (int i = 0; i < 10; i++)
        {
            if (charInventory[i] != null && charInventory[i].itemName == "Health Potion")
            {
                charInventory[i] = new Item();
                return;
            }                
        }
    }
    public Item GetCurrentWeapon()
    {
        return Database_Items.GetItemByName(Database_Saves.Instance.GetHero().weaponName);
    }
    public static Sprite GetSprite(string name)
    {
        name = name + "(Clone)";

        for (int j = 0; j < sprites.Count; j++)
        {
            for (int i = 0; i < sprites[j].Length; i++)
            {
                if (sprites[j][i].name == name)
                    return sprites[j][i];
            }
        }
        return null;
    }
}

#endregion