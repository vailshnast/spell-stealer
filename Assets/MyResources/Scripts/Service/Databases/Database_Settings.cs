﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using LitJson;
using System.IO;
using UnityEngine.UI;

public class Database_Settings : Database
{
    public static Database_Settings Instance { get; private set; }
    private Setting currentSetting;
    private JsonData json_settingsData;
    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;
        Initialise_Settings_Database();
    }



    public Setting GetSettings()
    {
        return currentSetting;
    }

    public void Initialise_Settings_Database()
    {
        string file;

        if (Application.platform == RuntimePlatform.Android)
        {
            file = Application.persistentDataPath + "/" + "Settings" + ".json";

            if (!File.Exists(file) || File.ReadAllText(file) == "")
            {
                SetSettings();
                File.WriteAllText(file, PrettyPrint(currentSetting));
            }
            else
            {
                json_settingsData = JsonMapper.ToObject(
                    File.ReadAllText(Application.persistentDataPath + "/" + "Settings" + ".json"));

                currentSetting = JsonMapper.ToObject<Setting>(json_settingsData.ToJson());
            }

        }
        else
        {
            file = Application.dataPath + "/" + "Settings" + ".json";

            if (!File.Exists(file))
            {
                SetSettings();
                File.WriteAllText(Application.dataPath + "/" + "Settings" + ".json", PrettyPrint(currentSetting));
            }
            else
            {
                json_settingsData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/" + "Settings" + ".json"));
                currentSetting = JsonMapper.ToObject<Setting>(json_settingsData.ToJson());
            }

        }
    }

    private void SetSettings()
    {
        currentSetting = new Setting(
            new RandomStats(0.0, 0.0),
            new RandomStats(0.0, 0.0),
            new RandomStats(10000.0, 10000.0),
            new RandomStats(0.8, 1.1),
            new RandomStats(200, 400),
            50, 2.5, 0.6, 5, 3, 5);
    }

}

[System.Serializable]
public class Setting
{
    public RandomStats OnClick_Time_To_Appear;
    public RandomStats Static_Time_To_Appear;
    public RandomStats Static_Time_To_Dissappear;
    public int Target_Count;

    public RandomStats circle_radius;    

    public double GuardDelay;
    public RandomStats GuardTime;

    public int lockpick_attempts;
    public int chestslot_amount;

    public Setting(RandomStats onClick, RandomStats static_Appear,
        RandomStats static_Dissappear,RandomStats guard_time,
        RandomStats circle_radius,
        double chance_to_cast, double chance_multiplier, double guard_delay,
        int target_Count,int lockpick_attempts, int chestslot_amount)
    {
        OnClick_Time_To_Appear = onClick;
        Static_Time_To_Appear = static_Appear;
        Static_Time_To_Dissappear = static_Dissappear;
        GuardTime = guard_time;
        this.circle_radius = circle_radius;
        Target_Count = target_Count;
        GuardDelay = guard_delay;
        this.lockpick_attempts = lockpick_attempts;
        this.chestslot_amount = chestslot_amount;
    }
    public Setting()
    {

    }
}
[System.Serializable]
public class RandomStats
{
    public double Min;
    public double Max;

    public RandomStats(double min, double max)
    {
        Max = max;
        Min = min;
    }
    public RandomStats()
    {

    }

    public float Random()
    {
        return UnityEngine.Random.Range((float)Min, (float)Max);
    }
}