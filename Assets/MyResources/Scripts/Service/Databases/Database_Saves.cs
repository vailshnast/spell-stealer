﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;
using System.Linq;

public class Database_Saves : Database
{
    public static Database_Saves Instance { get; private set; }
    private CharacterStats data_save = new CharacterStats();
    private JsonData json_slots_save;

    void Awake()
    {
        Initialise();
    }
    private void Initialise()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        Initialise_Save_Database();        
    }
    public void Initialise_Save_Database()
    {
        string file;

        if (Application.platform == RuntimePlatform.Android)
        {
            file = Application.persistentDataPath + "/" + "Save_slots" + ".json";

            if (!File.Exists(file) || File.ReadAllText(file) == "")
            {
                CreateChar();

                File.WriteAllText(file, PrettyPrint(data_save));
            }
            else
            {
                json_slots_save = JsonMapper.ToObject(
                    File.ReadAllText(Application.persistentDataPath + "/" + "Save_slots" + ".json"));

                data_save = JsonMapper.ToObject<CharacterStats>(json_slots_save.ToJson());
            }

        }
        else
        {
            file = Application.dataPath + "/" + "Save_slots" + ".json";

            if (!File.Exists(file))
            {
                CreateChar();
                File.WriteAllText(Application.dataPath + "/" + "Save_slots" + ".json", PrettyPrint(data_save));
            }
            else
            {
                json_slots_save = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/" + "Save_slots" + ".json"));
                data_save = JsonMapper.ToObject<CharacterStats>(json_slots_save.ToJson());
            }

        }
    }  
    public void Save()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            string filepath = Application.persistentDataPath + "/" + "Save_slots" + ".json";
            Debug.Log("Running system");
            if (!File.Exists(filepath))

            {

                WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + "Save_slots" + ".json");
                //TODO A coroutine instead of this dangerous cliff
                while (!loadDB.isDone) { }
                File.WriteAllBytes(filepath, loadDB.bytes);
            }
            File.WriteAllText(filepath, PrettyPrint(data_save));
        }
        else
        {
            File.WriteAllText(Application.dataPath + "/" + "Save_slots" + ".json", PrettyPrint(data_save));
        }
    } 
    public void ResetSave()
    {
        //Debug.Log("reset");
        CreateChar();
        Save();
    }
    public CharacterStats GetHero()
    {
        return data_save;
    }
    private void CreateChar()
    {
        data_save = new CharacterStats();
    }
}

[Serializable]
public class CharacterStats
{
    public int Experience { get; set; }
    public bool showTutorial { get; set; }
    public List<Stat> stats;
    public string weaponName { get; set; }
    public List<SpellInfo> spellDatabase { get; set; }
    public List<Passive_Save> tree;
    public int[] List_Runes { get; set; }
    public int currentExp { get; set; }

    public double fadeFillerAmount { get; set; }
    public int currentLevel { get; set; }

    public CharacterStats()
    {
        currentExp = 0;
        fadeFillerAmount = 1;
        showTutorial = true;
        weaponName = "Board Sword";

        spellDatabase = new List<SpellInfo>
        {
            new SpellInfo("Unholy Smite"),
            new SpellInfo("Withering"),
            new SpellInfo("Agony"),
            new SpellInfo("Soul Cry"),
            new SpellInfo("Decrepify"),
            new SpellInfo("Life Tap"),
            new SpellInfo("Unholy Heal"),
            new SpellInfo("Exhaust"),
            new SpellInfo("Decay"),
            new SpellInfo("Soul Torment")
        };

        stats = new List<Stat>
        {
            new Stat(0,0,100000, "Gold")
        };
    }

    public void ViewSpell(string spellName)
    {
        spellDatabase.FirstOrDefault(spell => spell.spellName == spellName).View();
    }
    public void StealSpell(string spellName)
    {
        spellDatabase.FirstOrDefault(spell => spell.spellName == spellName).Steal();
    }

    public SpellInfo GetSpellInfo(string spellName)
    {
        return spellDatabase.FirstOrDefault(spell => spell.spellName == spellName);
    }
    public void SetStats(int health,int mana,int damage,int defence,int power,int magic,int crit,int runeSlots,int spellCharges,int spellStealManaCost,int gold)
    {
        stats = new List<Stat>
        {
            new Stat(health,0, "Health"),
            new Stat(mana,0,"Mana"),
            new Stat(damage,5,1000, "Damage"),
            new Stat(defence,int.MinValue,100, "Defence"),
            new Stat(power,100,1000, "Power"),
            new Stat(magic,100,1000, "Magic"),
            new Stat(crit,1,1000, "Crit"),
            new Stat(gold,0,100000, "Gold"),
            new Stat(runeSlots,0,5,"Runeslots"),
            new Stat(spellCharges,1,1000,"Charges"),
            new Stat(spellStealManaCost,-1000,1000,"Mana Cost")
    };
        List_Runes = new int[runeSlots];
        
        currentExp = 0;
        fadeFillerAmount = 1;
    }
    public Stat GetStat(string name)
    {
        return stats.FirstOrDefault(t => t.name == name);
    }

    public void ResetStats()
    {
        for (int i = 0; i < stats.Count; i++)
        {
            stats[i].ResetActualValueList();
        }
    }

    public Passive_Save GetPassive(int tier, PassiveType type)
    {
        return tree.FirstOrDefault(passive => passive.tier == tier && passive.type == type);
    }
    public void SaveTree(List<Passive_Save> tree)
    {
        this.tree = tree;     
    }
    public void SaveFiller(int currentExp, double fadeFillerAmount,int currentLevel)
    {
        this.currentExp = currentExp;
        this.fadeFillerAmount = fadeFillerAmount;
        this.currentLevel = currentLevel;
    }
}
[Serializable]
public class Stat
{
    public int baseValue { get; private set; }
    public int maxValue { get; private set; }
    private int minValue { get; set; }

    private readonly List<Modifier> modifiers = new List<Modifier>();
    public string name;

    public Stat(int baseValue, int minValue, string name)
    {
        this.baseValue = baseValue;
        this.minValue = minValue;
        maxValue = baseValue;
        this.name = name;
    }
    public Stat(int baseValue, int minValue, int maxValue, string name)
    {
        this.baseValue = baseValue;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.name = name;
    }
    public Stat(Stat stat)
    {
        baseValue = stat.baseValue;
        minValue = stat.minValue;
        maxValue = stat.maxValue;
        name = stat.name;
    }
    public Stat()
    {

    }
    public int ChangeCurrent(int value)
    {
        int adjusted = Mathf.Clamp(baseValue + value, minValue, maxValue) - baseValue;
        baseValue += adjusted;
        return adjusted;
    }
    public void AddModifier(Modifier modifier)
    {
        modifiers.Add(modifier);
    }
    public void RemoveModifer(Modifier modifier)
    {
        modifiers.Remove(modifier);
    }
    public int GetAdjusted(int value)
    {
        return Mathf.Clamp(baseValue + value, minValue, maxValue) - baseValue;        
    }
    public void AdjustMax(int value)
    {
        if (maxValue == int.MaxValue)
            return;

        maxValue = Mathf.Clamp(maxValue + value, minValue, int.MaxValue);
    }
    public void Restore()
    {
        baseValue = maxValue;
    }

    public void ResetActualValueList()
    {
        modifiers.Clear();
    }
    public void SetBaseValue(int value)
    {
        baseValue = value;
    }
    public int GetActualValue()
    {
      return  Mathf.Clamp(baseValue + modifiers.Sum(t => t.value), minValue, maxValue);
    }
}

public class SpellInfo
{
    public string spellName { get; set; }
    public bool stolen { get; private set; }
    public bool viewed { get;private set; }


    public SpellInfo(string spellName)
    {
        this.spellName = spellName;
    }
    public void Steal()
    {
        stolen = true;
    }
    public void View()
    {
        viewed = true;
    }
    public SpellInfo()
    {
        
    }
}