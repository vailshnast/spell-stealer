﻿using UnityEngine;
using System.Collections;
using System;
public enum MonsterSprite
{
    Idle, 
    Cast,
    CastAnimation,
    Swing,
    SwingCrit,
    SwingAnimation,
    Death,
    Victory,
    Agony
}
public enum TargetState
{
    Static,
    Clicked,
    Inactive
}
public enum TutorialState
{
    SelectUndeadLair,
    ExploreUndeadLair,
    MoveToNextRoom,
    PressFight,
    WaitForGuard,
    TapOnBlock,
    WaitForSpell,
    ActivateSpellSteal,
    WaitForSteal,
    CastSpell,
    Victory,
    TakeReward,
    Restart
}
public enum BattleState
{
    Prepare,
    StartingScreen,
    Fight,
    Channeling,
    CastAnimation,
    SwingAnimation,
    Swing,
    Victory,
    Defeat    
}
public enum StageState
{
    Map,
    SkillTree,
    Level,
    Reward,
    Battle,
    Chest,
    Merchant,
    Altar,
    Final
}
public enum ChestState
{
    Prepare,
    LockPicking,
    Failed
}
public enum SpellType
{
    Weak,
    Strong
}
public enum Rarity
{
    Magic,
    Enchanted,
    Sacred
}
public enum PropertyType
{
    Passive,
    Crit,
    Block,
    Use
}
public enum AltarType
{
    Warod,
    Enur,
    Resalia
}
public enum TooltipType
{
    Buyable,
    Sellable,
    Usable,
}

public enum ButtonState
{
    Weapon,
    Item,
    Rune
}
public enum PassiveType
{
    Main,
    Magic,
    Attack,
    General
}
public enum IconType
{
    Negative,
    Positive
}
public enum BuffType
{
    Toggled,
    Periodic,
    Phased
}
public enum InfoType
{
    Damage,
    Default,
    Health,
    Mana,
}
public enum DamageType
{
    Pierce,
    Normal
}
public enum Target
{
    Player,
    Enemy
}
public enum User
{ 
    Player,
    Enemy
}
public enum MathfSign
{
    Substract,
    Increase
}
public enum AdjustType
{
    Point,
    Percentage
}
public enum Side
{
    Left,
    Right
}
public delegate void EventHandler();

