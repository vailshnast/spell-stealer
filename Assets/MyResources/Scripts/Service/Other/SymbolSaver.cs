﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LitJson;
using UnityEngine;
using UnityEngine.UI;
using Vectrosity;
using Debug = UnityEngine.Debug;

public class SymbolSaver : Database
{
    public GameObject prefab;
    public VectorObject2D line;


    public bool show;
    public int symbol;
    private SymbolDatabase symbolDatabase;
    private List<Vector2Json> coords = new List<Vector2Json>();

    private GameObject particle;

    void Start()
    {
        InitDatabase();
        TextAsset txtAsset = (TextAsset)Resources.Load("Other/Symbols", typeof(TextAsset));
        particle = GameObject.Find("Trail");

        #region lines

        if (show)
        {
            List<Vector2> corners = new List<Vector2>();
            List<Vector2> linePoints = new List<Vector2>();

            for (int i = 0; i < symbolDatabase.symbols[symbol].points.Count; i++)
            {
                corners.Add(Camera.main.WorldToScreenPoint(new Vector2((float) symbolDatabase.symbols[symbol].points[i].x, (float) symbolDatabase.symbols[symbol].points[i].y)));
            }

            int koef = 3;
            for (int i = 0; i < corners.Count - 1; i++)
            {
                float length = (corners[i] - corners[i + 1]).magnitude;
                float pointsCount = length / koef;
                for (int j = 0; j < pointsCount; j++)
                {
                    linePoints.Add(Vector2.Lerp(corners[i], corners[i + 1], (float)1 / pointsCount * j));
                }
            }

            line.vectorLine.SetColor(new Color32(255, 0, 0, 255));
            line.vectorLine.points2 = linePoints;
            line.vectorLine.Draw();
        }

        #endregion
    }
    public void Click()
    {
        Vector3 pos = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);
        Instantiate(prefab, pos, Quaternion.identity, GameObject.Find("Panel").transform);

    }

    IEnumerator Animate(float speed)
    {
        particle.transform.position = new Vector3((float)symbolDatabase.symbols[symbol].points[0].x, (float)symbolDatabase.symbols[symbol].points[0].y, -7);
        for (int i = 0; i < symbolDatabase.symbols[symbol].points.Count; i++)
        {
            while (Vector3.Distance(particle.transform.position, new Vector3((float)symbolDatabase.symbols[symbol].points[i].x, (float)symbolDatabase.symbols[symbol].points[i].y, -7)) > .0001f)
            {
                particle.transform.position = Vector3.MoveTowards(particle.transform.position, new Vector3((float)symbolDatabase.symbols[symbol].points[i].x, (float)symbolDatabase.symbols[symbol].points[i].y, -7), Time.deltaTime * speed);
                yield return null;
            }
        }
    }

    public void Save()
    {
        string name = GameObject.Find("InputField").GetComponent<InputField>().text;
        Transform panel = GameObject.Find("Panel").transform;
        for (int i = 0; i < panel.childCount; i++)
        {
            coords.Add(new Vector2Json(panel.GetChild(i).transform.position.x, panel.GetChild(i).transform.position.y));
        }

        Symbol symbol = symbolDatabase.symbols.FirstOrDefault(t => t.name == name);

        if (symbol != null)
            symbol.SetCoords(coords);
        else
            symbolDatabase.symbols.Add(new Symbol(name, coords, SpellType.Weak));

        coords.Clear();
        File.WriteAllText(Application.dataPath + "/Resources/Other/Symbols.json", PrettyPrint(symbolDatabase));
    }

    private void InitDatabase()
    {
        string file = Application.dataPath + "/Resources/Other/Symbols.json";
        
        if (!File.Exists(file))
        {
            symbolDatabase = new SymbolDatabase();
            File.WriteAllText(file, PrettyPrint(symbolDatabase));
        }
        else
        {

            symbolDatabase = JsonMapper.ToObject<SymbolDatabase>(JsonMapper.ToObject(
                    File.ReadAllText(Application.dataPath + "/Resources/Other/Symbols.json"))
                .ToJson());
        }
    }
}
public class SymbolDatabase
{
    public List<Symbol> symbols = new List<Symbol>();

    public SymbolDatabase()
    {
        
    }
}
public struct Vector2Json
{
    public double x;
    public double y;

    public Vector2Json(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
}
public class Symbol
{
    public string name { get; set; }
    public List<Vector2Json> points;
    public SpellType type;
    private bool used;
    public Symbol(string name, List<Vector2Json> points,SpellType type)
    {
        this.name = name;
        this.points = new List<Vector2Json>(points);
        this.type = type;
    }

    public void SetCoords(List<Vector2Json> points)
    {
        this.points = new List<Vector2Json>(points);
    }
    public bool IsSymbolUsed()
    {
        return used;
    }
    public void SetUsage(bool usage)
    {
        used = usage;
    }
    public Symbol() 
    {
       
    }
}