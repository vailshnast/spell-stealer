﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
public static class ExtensionMethods
{
    private static Vector3[] worldCorners = new Vector3[4];

    public static Vector2 WorldToRectCoordinate(this RectTransform rectTransform, Vector3 position_world)
    {
        if (!rectTransform) return Vector2.zero;

        rectTransform.GetWorldCorners(worldCorners);

        var position_origin = rectTransform.InverseTransformPoint(worldCorners[0]);
        var position_local = rectTransform.InverseTransformPoint(position_world);

        var position_rect = (Vector2)(position_local - position_origin);

        return position_rect;

    }


    public static Vector3 RectToWorldCoordinate(this RectTransform rectTransform, Vector2 position_rect)
    {
        if (!rectTransform) return Vector3.zero;

        rectTransform.GetWorldCorners(worldCorners);

        var position_origin = rectTransform.InverseTransformPoint(worldCorners[0]);
        var position_local = (Vector3)position_rect + position_origin;

        var position_world = rectTransform.TransformPoint(position_local);

        return position_world;
    }
    public static Transform DestroyChildren(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }
    public static Transform DestroyChildren(this Transform transform, GameObject parentObject)
    {
        foreach (Transform child in parentObject.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }
    public static GameObject getChild(this Transform transform, GameObject objectToLook, string name)
    {
        Transform[] allChildren = objectToLook.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.name == name)
                return child.gameObject;
        }
        return null;
    }
    public static GameObject getChild(this Transform transform, string name)
    {
        Transform[] allChildren = transform.gameObject.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.name == name)
                return child.gameObject;
        }
        return null;
    }
    public static Transform FindDeepChild(this Transform aParent, string aName)
    {
        var result = aParent.Find(aName);
        if (result != null)
            return result;
        foreach (Transform child in aParent)
        {
            result = child.FindDeepChild(aName);
            if (result != null)
                return result;
        }
        return null;
    }
    public static Transform FindDeepChild(this Transform aParent,string aName, GameObject toLook)
    {
        var result = toLook.transform.Find(aName);
        if (result != null)
            return result;
        foreach (Transform child in aParent)
        {
            result = child.FindDeepChild(aName);
            if (result != null)
                return result;
        }
        return null;
    }
    public static Color HexToColor(this MonoBehaviour mono, string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
    public static bool IsBetween(this int value, int left, int right)
    {
        return value >= left && value <= right;
    }

    public static void ChangeAlpha(this Image p_image, float alpha)
    {
        Color tempColor = p_image.color;
        tempColor.a = alpha;
        p_image.color = tempColor;
    }
    public static void ChangeSprite(this Image p_image, Sprite sprite,bool setNativeSize)
    {
        p_image.sprite = sprite;
        if(setNativeSize)
            p_image.SetNativeSize();
    }

}
public static class Debug
{
    public static void Log(object message)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.Log(message);
    }
    public static void Log(object message, UnityEngine.Object context)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.Log(message, context);
    }

    public static void LogError(object message)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.LogError(message);
    }

    public static void LogError(object message, UnityEngine.Object context)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.LogError(message, context);
    }

    public static void LogWarning(object message)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.LogWarning(message.ToString());
    }

    public static void LogWarning(object message, UnityEngine.Object context)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.LogWarning(message.ToString(), context);
    }

    public static void DrawLine(Vector3 start, Vector3 end, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.DrawLine(start, end, color, duration, depthTest);
    }

    public static void DrawRay(Vector3 start, Vector3 dir, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.DrawRay(start, dir, color, duration, depthTest);
    }
    public static void LogException(System.Exception e)
    {
        if (Application.isMobilePlatform) return;
        UnityEngine.Debug.LogException(e);
    }
}

