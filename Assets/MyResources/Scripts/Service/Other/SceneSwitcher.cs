﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SceneSwitcher : MonoBehaviour {

    public static SceneSwitcher Instance { get; private set; }

    public GameObject[] scenes;
    private GameObject currentScene; 

    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;
    }

    private GameObject find_scenes(string name)
    {
        return scenes.FirstOrDefault(looked_scene => looked_scene.name == name);
    }

    private GameObject find_Active_Scene()
    {
        foreach (GameObject looked_scene in scenes)
        {
            if (looked_scene.activeInHierarchy)
                return looked_scene;

        }
        return null;
    }
    private void switch_scene(string sceneName)
    {

        if (currentScene == null)
            currentScene = find_Active_Scene();
        
        currentScene.gameObject.SetActive(false);
        currentScene = find_scenes(sceneName);
        currentScene.gameObject.SetActive(true);
    }

    //Don't wanna make a separated class to switch scenes
    //Scene switchers

    public void Load_Scene(string name)
    {
        switch_scene(name);
    }
    public void close_application()
    {
        Application.Quit();
    }   
    
}
