﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class WeightedRandomizer
{
    public static WeightedRandomizer<R> From<R>(Dictionary<R, int> spawnRate)
    {
        return new WeightedRandomizer<R>(spawnRate);
    }
}
public class WeightedRandomizer<T>
{
    private Dictionary<T, int> _weights;

    /// <summary>
    /// Instead of calling this constructor directly,
    /// consider calling a static method on the WeightedRandomizer (non-generic) class
    /// for a more readable method call, i.e.:
    /// 
    /// <code>
    /// var selected = WeightedRandomizer.From(weights).TakeOne();
    /// </code>
    /// 
    /// </summary>
    /// <param name="weights"></param>
    public WeightedRandomizer(Dictionary<T, int> weights)
    {
        _weights = weights;
    }

    /// <summary>
    /// Randomizes one itemInfo
    /// </summary>
    /// <param name="spawnRate">An ordered list withe the current spawn rates. The list will be updated so that selected items will have a smaller chance of being repeated.</param>
    /// <returns>The randomized itemInfo.</returns>
    public T Random()
    {
        // Sorts the spawn rate list

        // Sums all spawn rates
        int sum = _weights.Sum(x => x.Value);

        var weightsList =  new List<KeyValuePair<T, int>>(_weights);

        // Randomizes a number from Zero to Sum
        int roll = UnityEngine.Random.Range(0, sum);
        // Finds chosen itemInfo based on spawn rate
        T selected = weightsList[weightsList.Count - 1].Key;
        foreach (var spawn in _weights)
        {
            if (roll < spawn.Value)
            {
                selected = spawn.Key;
                break;
            }
            roll -= spawn.Value;
        }
        // Returns the selected itemInfo
        return selected;
    }
}