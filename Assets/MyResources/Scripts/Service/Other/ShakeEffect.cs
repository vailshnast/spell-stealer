﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShakeEffect : MonoBehaviour
{
    float shakeAmount; //The amount to shake this frame.
    float shakeDuration; //The duration this frame.

    //Readonly values...
    float shakePercentage; //A percentage (0-1) representing the amount of shake to be applied when setting rotation.

    float startAmount; //The initial shake amount (to determine percentage), set when ShakeCamera is called.
    float startDuration; //The initial shake duration, set when ShakeCamera is called.

    bool isRunning = false; //Is the coroutine running right now?

    private RectTransform rect;
    private Vector2 originPos;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShakeObject(Random.Range(50,50), 1);
        }
    }

    public void StopShaking()
    {
        StopAllCoroutines();
        isRunning = false;
        if (rect)
            rect.anchoredPosition = originPos;
    }

    public void ShakeObject(float amount, float duration)
    {    
        //shakeAmount += amount; //Add to the current amount.
        shakeAmount = amount;
        startAmount = shakeAmount; //Reset the start amount, to determine percentage.
        //shakeDuration += duration; //Add to the current time.
        shakeDuration = duration;
        startDuration = shakeDuration; //Reset the start time.
        if (!isRunning) StartCoroutine(Shake()); //Only call the coroutine if it isn't currently running. Otherwise, just set the variables.
    }

    IEnumerator Shake()
    {
        isRunning = true;
        
        while (shakeDuration > 0)
        {
            Vector3 randomPos = Random.insideUnitCircle * shakeAmount + originPos; //A Vector3 to add to the Local Rotation

            shakePercentage = shakeDuration / startDuration; //Used to set the amount of shake (% * startAmount).

            shakeAmount = startAmount * shakePercentage; //Set the amount of shake (% * startAmount).
            //shakeDuration = Mathf.Lerp(shakeDuration, 0, Time.deltaTime);//Lerp the time, so it is less and tapers off towards the end.
            shakeDuration -= Time.deltaTime;

            rect.anchoredPosition = new Vector2(randomPos.x,randomPos.y); //Set the local rotation the be the rotation amount.

            yield return null;
        }
        rect.anchoredPosition = originPos; //Set the local rotation to 0 when done, just to get rid of any fudging stuff.
        isRunning = false;
    }

    public void SetShakingObject(RectTransform rect)
    {
        this.rect = rect;
        originPos = rect.anchoredPosition;
    }
}
