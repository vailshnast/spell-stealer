﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class Click_Target : MonoBehaviour {

    private Setting setting;

    private Battle_Manager battle_Manager;
    private MonsterInfo monsterInfo;

    private Image imageComponent;

    private TargetState current_State;

    public EventHandler OnClick;

    // Use this for initialization
    void Awake () {
        battle_Manager = FindObjectOfType<Battle_Manager>();
        setting = Database_Settings.Instance.GetSettings();
        monsterInfo = FindObjectOfType<MonsterInfo>();
        imageComponent = GetComponent<Image>();

        current_State = TargetState.Inactive;
    }

    public void On_Target_Click()
    {

        if (Main_Manager.Instance.showTutorial)
        {
            Time.timeScale = 1;
        }


        ChangeState(TargetState.Clicked);

        int damage = HeroInfo.Instance.GetHero().GetStat("Damage").GetActualValue();

        OnClickEvent();

        //Shake enemy sprite
        monsterInfo.ShakeEnemySprite(0.2f, 25);

        battle_Manager.AddFinalBlowCharge();
        battle_Manager.Create_Message(new Info(monsterInfo.DealDamageToMonster(damage, null, DamageType.Normal).ToString(), Color.red, 50, InfoType.Damage,GetComponent<RectTransform>().anchoredPosition), 0.5f, 3f);
        GameObject slice = Instantiate(Resources.Load("Prefabs/Battle/Slice") as GameObject, new Vector2(0,0), Quaternion.identity, GameObject.Find("Instantiated_Objects").transform);

        if (Battle_Manager.Instance.sliceSide == Side.Left)
        {
            slice.GetComponent<Image>().sprite = SaveSystem.GetSprite("Swing Left");
            Battle_Manager.Instance.sliceSide = Side.Right;
        }
        else
        {
            slice.GetComponent<Image>().sprite = SaveSystem.GetSprite("Swing Right");
            Battle_Manager.Instance.sliceSide = Side.Left;
        }

        RectTransform rect = slice.GetComponent<RectTransform>();
        rect.anchoredPosition3D = new Vector3(0, Random.Range(20,60), 0);
        slice.GetComponent<Image>().CrossFadeAlpha(0, 0.5f, true);
        Destroy(slice,0.5f);
    }
    private IEnumerator OnClickRerandom()
    {
        imageComponent.enabled = false;

        yield return new WaitForSeconds(setting.OnClick_Time_To_Appear.Random());

        imageComponent.enabled = true;

        battle_Manager.RandomPos(gameObject);

        SwitchToStaticState();
    }
    private IEnumerator StaticRerandom()
    {
        imageComponent.enabled = false;

        yield return new WaitForSeconds(setting.Static_Time_To_Appear.Random());

        imageComponent.enabled = true;

        yield return new WaitForSeconds(setting.Static_Time_To_Dissappear.Random());

        imageComponent.enabled = false;
        battle_Manager.RandomPos(gameObject);
        StartCoroutine(StaticRerandom());
    }
    private void OnClickEvent()
    {
        if (OnClick != null)
            OnClick();
    }
    private void EnterState(TargetState state)
    {
        switch (state)
        {
            case TargetState.Static:
                StartCoroutine(StaticRerandom());
                break;
            case TargetState.Clicked:
                StartCoroutine(OnClickRerandom());
                break;
            case TargetState.Inactive:
                imageComponent.enabled = false;
                break;
        }
    }
    private void ExitState(TargetState state)
    {
        switch (state)
        {
            case TargetState.Static:
                StopAllCoroutines();
                break;
            case TargetState.Clicked:
                StopAllCoroutines();
                break;
            case TargetState.Inactive:
                imageComponent.enabled = true;
                break;
        }
    }
    public void ChangeState(TargetState state)
    {
        if (current_State == state)
        {
            //Debug.Log("same state");
            return;
        }

        ExitState(current_State);

        EnterState(state);

        current_State = state;
    }
    public void SwitchToClickedState()
    {
        ChangeState(TargetState.Clicked);
    }
    public void SwitchToStaticState()
    {
        ChangeState(TargetState.Static);
    }
    public void SwitchToInactiveState()
    {
        ChangeState(TargetState.Inactive);
    }   
}
