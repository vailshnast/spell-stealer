﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using TMPro;
using Random = UnityEngine.Random;

public class Battle_Manager : Touch_processor
{
    #region Variables
    #region Instances
    public static Battle_Manager Instance { get; private set; }

    private Setting settings;

    private HeroInfo heroInfo;
    private MonsterInfo monsterInfo;

    private GameObject[] targets;
    private List<GameObject> textPool;

    private GameObject startBattle;
    private Image castFiller;
    private GameObject attackBar;
    private Image potionFiller;
    private GameObject victoryButton;
    private GameObject deathButton;
    #endregion

    //Skills
    //
    //
    [HideInInspector]
    public SkillManager skillManager;
    [HideInInspector]
    public Spell current_Spell;
    private SpellSteal spellSteal;
    private string previousMonsterCastedSpellName;
    private GuardGame guardGame;
    private List<Buff> active_buffs = new List<Buff>();

    private string usedSpell;
    //State
    //
    //
    private float fightTime;
    [HideInInspector]
    public BattleState current_State { get; private set; }
    private Coroutine state_Coroutine { get; set; }
    private Coroutine guard_Coroutine { get; set; }
    private Coroutine potion_Coroutine { get; set; }
    //if coroutine is running
    //
    private bool changingState;
    private bool drinking { get; set; }
    //Other
    //
    //
    private Final_Blow finalBlow;

    private EventHandler OnAttack;
    [HideInInspector]
    public Side sliceSide = Side.Left;
    //Warning
    //
    //If we dont check this in OnEnable, will cause different bugs,
    //because, OnEnable is called before Start().
    private bool initialised;

    #endregion

    #region Main
    // Use this for initialization
    void Start()
    {
        Initialise();

    }
    void Initialise()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        #region Objects
        settings = Database_Settings.Instance.GetSettings();
        monsterInfo = MonsterInfo.Instance;
        heroInfo = HeroInfo.Instance;
        
        attackBar = transform.FindDeepChild("AttackBar").gameObject;
        castFiller = transform.FindDeepChild("AttackBar_Filler").GetComponent<Image>();
        victoryButton = transform.FindDeepChild("Victory").gameObject;
        startBattle = transform.FindDeepChild("Start_Battle").gameObject;
        deathButton = transform.FindDeepChild("Death").gameObject;
        potionFiller = transform.FindDeepChild("PotionFiller").GetComponent<Image>();

        #endregion

        SpawnTargets(10);

        InitEnemySkills();

        finalBlow = new Final_Blow();
        guardGame = new GuardGame(transform);
        spellSteal = new SpellSteal(1, SaveSystem.GetChar().GetStat("Charges").baseValue);

        startBattle.SetActive(true);

        SetPotionFiller();
        CreatePool(30);
        CreateResalia();

        monsterInfo.SetState(MonsterSprite.Idle);
        current_State = BattleState.StartingScreen;

        initialised = true;
    }
    void Update()
    {
        BattlePhaseControl();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            UnityEngine.Debug.Break();
        }
    }
    void OnEnable()
    {
        if (initialised)
        {            
            ChangeState(BattleState.StartingScreen);
        }            
    }

    #endregion
    #region State_Methods
    private IEnumerator ChangeStateCoroutine(BattleState state, float time)
    {

        if (changingState)
        {

            Debug.Log("State is changing already");
            yield break;
        }
        /*if (current_State == BattleState.Victory || current_State == BattleState.Defeat)
        {
            Debug.Log("you are dead or victorious,dont change state");
            yield break;
        }*/

        changingState = true;

        yield return new WaitForSeconds(time);

        changingState = false;
        ChangeState(state);

    }
    public void ChangeState(BattleState state)
    {
        if (current_State == state)
        {
            Debug.Log("same state");
            return;
        }
        /*if (current_State == BattleState.Victory || current_State ==  BattleState.Defeat)
        {
            Debug.Log("you are dead or victorious,dont change state");
            return;
        }*/

        

        ExitState(current_State);
        current_State = state;
        EnterState(state);

    }
    public void ChangeState(BattleState state, float timeToWait)
    {
        if(!SaveSystem.isPlayerDead)
        state_Coroutine = StartCoroutine(ChangeStateCoroutine(state, timeToWait));
    }
    private void ExitState(BattleState state)
    {
        //Debug.Log("exit" + state);
        switch (state)
        {
            case BattleState.StartingScreen:
                startBattle.SetActive(false);
                break;
            case BattleState.Fight:
                break;
            case BattleState.Channeling:
                attackBar.gameObject.SetActive(false);
                spellSteal.Toggle_Sprite(false);
                break;
            case BattleState.Swing:
                castFiller.fillAmount = 1;
                guardGame.effect.SetActive(false);
                ChooseSpell();
                break;

            case BattleState.Victory:
                castFiller.fillAmount = 1;
                victoryButton.SetActive(false);
                         
                SetTargetsPositions();
                
                ResetEvents();

                DisableText();

                break;
            case BattleState.Defeat:
                castFiller.fillAmount = 1;
                deathButton.SetActive(false);

                SetTargetsPositions();

                ResetEvents();

                DisableText();

                break;
        }
    }
    private void EnterState(BattleState state)
    {
        //Debug.Log("enter" + state);
        switch (state)
        {
            case BattleState.StartingScreen:
                monsterInfo.SetState(MonsterSprite.Idle);

                SetPotionFiller();

                spellSteal.SetSpellSteal(SaveSystem.GetChar().GetStat("Charges").baseValue);

                SetTargetCount();

                CreateResalia();
                

                monsterInfo.SetState(MonsterSprite.Idle);
                startBattle.SetActive(true);
                break;

            case BattleState.Prepare:
                //DELAY

                DestroyOnePhaseBuffs();

                if (current_Spell != null)
                {
                    ChangeState(BattleState.Channeling, 0.8f);
                    Create_Message(new Info(current_Spell.spell_name, Color.white, 120, InfoType.Default, new Vector2(0, 170)), 1.8f, 0f);
                }

                else
                    ChangeState(BattleState.Fight);
                            
                    
                
                    
                monsterInfo.SetState(MonsterSprite.Idle);
                break;
            case BattleState.Fight:

                monsterInfo.SetState(MonsterSprite.Idle);
              
                fightTime = (float)monsterInfo.GetMonster().fightTime;

                if (Main_Manager.Instance.showTutorial)
                {
                    if (Tutorial.Instance.currentState == TutorialState.WaitForGuard)
                    {
                        ActivateTargets();
                    }

                    Time.timeScale = 0;
                }
                else
                {
                    if (finalBlow.IsActive)
                        finalBlow.ToggleSprite(true);

                    ActivateTargets();
                }

                break;
            case BattleState.Channeling:

                attackBar.gameObject.SetActive(true);
                monsterInfo.SetState(MonsterSprite.Cast);

                monsterInfo.AdjustMana(-current_Spell.enemyManaCost, AdjustType.Point);

                GameObject.Find("Black_Mask").GetComponent<TextMeshProUGUI>().text = current_Spell.spell_name;
                GameObject.Find("White_Mask").GetComponent<TextMeshProUGUI>().text = current_Spell.spell_name;

                if (Main_Manager.Instance.showTutorial)
                {
                    Tutorial.Instance.Procceed();
                }
                else
                {
                    if (finalBlow.IsActive)
                        finalBlow.ToggleSprite(true);

                    ActivateTargets();
                }
                    
                break;
            case BattleState.CastAnimation:
                DrawUtility.Instance.ToggleGame(false);
                previousMonsterCastedSpellName = current_Spell.spell_name;
                monsterInfo.SetState(MonsterSprite.CastAnimation);
                
                
                //CAST ANIMATION TIME
                ChangeState(BattleState.Fight, 0.5f);

                current_Spell.Cast(User.Enemy);
                current_Spell = null;
                break;

            case BattleState.Swing:                
                monsterInfo.SetState(MonsterSprite.SwingAnimation);
                //ATTACK ANIMATION TIME
                ChangeState(BattleState.Prepare, 0.5f);
                guardGame.guard.SetActive(false);
                break;
            case BattleState.SwingAnimation:
                monsterInfo.SetState(monsterInfo.GetMonster().GetStat("Crit").baseValue == 0 ? MonsterSprite.SwingCrit : MonsterSprite.Swing);
                DisableTargets();
                finalBlow.ToggleSprite(false);

                AudioSource source = GetComponent<AudioSource>();

                source.PlayOneShot(source.clip);
                Guard();
                break;
            case BattleState.Victory:
                DisableTargets();
                if (spellSteal.spell_coroutine != null)
                    StopCoroutine(spellSteal.spell_coroutine);
                spellSteal.Refresh(this);
                Disable_State();

                usedSpell = null;

                DrawUtility.Instance.ToggleGame(false);

                finalBlow.Disable();
                monsterInfo.StopShaking();
                DestroyBuffs();
                guardGame.guard.SetActive(false);
                guardGame.effect.SetActive(false);

                attackBar.gameObject.SetActive(false);
                monsterInfo.SetState(MonsterSprite.Death);
                victoryButton.SetActive(true);
                current_Spell = null;

                SetPotionFiller();
                break;
            case BattleState.Defeat:               
                SaveSystem.isPlayerDead = true;

                DisableTargets();
                if (spellSteal.spell_coroutine != null)
                    StopCoroutine(spellSteal.spell_coroutine);
                spellSteal.Refresh(this);
                Disable_State();

                usedSpell = null;

                DrawUtility.Instance.ToggleGame(false);

                finalBlow.Disable();
                monsterInfo.StopShaking();
                DestroyBuffs();
                guardGame.guard.SetActive(false);
                guardGame.effect.SetActive(false);

                attackBar.gameObject.SetActive(false);
                
                
                monsterInfo.SetState(previousMonsterCastedSpellName == "Agony" ? MonsterSprite.Agony : MonsterSprite.Victory);

                deathButton.SetActive(true);
                
                current_Spell = null;
                
                
                SetPotionFiller();
                break;
        }
    }
    private void BattlePhaseControl()
    {
        //All_Phases
        ProccessBuffsDurations();

        switch (current_State)
        {
            case BattleState.Fight:
                fightTime -= Time.deltaTime;

                if(fightTime <= 0)
                    ChangeState(BattleState.SwingAnimation);
                
                break;
            case BattleState.Channeling:
                if (Main_Manager.Instance.showTutorial)
                    castFiller.fillAmount -= 0f / current_Spell.chanelingTime * Time.deltaTime;
                else
                    castFiller.fillAmount -= 1f / current_Spell.chanelingTime * Time.deltaTime;

                if (castFiller.fillAmount <= 0)
                    ChangeState(BattleState.CastAnimation);
                else if (spellSteal.can_steal_spell && !BuffExists("Silence",Target.Player))
                    spellSteal.Toggle_Sprite(true);

                break;
        }
    }
    private void Disable_State()
    {
        if (state_Coroutine != null)
            StopCoroutine(state_Coroutine);

        if(guard_Coroutine != null)
            StopCoroutine(guard_Coroutine);
        
        if(potion_Coroutine != null)
            StopCoroutine(potion_Coroutine);

        changingState = false;
    }
    #endregion
    #region Targets
    private void SpawnTargets(int count)
    {
        targets = new GameObject[count];

        for (int i = 0; i < count; i++)
        {
            targets[i] = Instantiate(Resources.Load("Prefabs/Battle/Target"), new Vector3(0, 0, 0), Quaternion.identity, GameObject.Find("Targets").transform) as GameObject;
            RectTransform rect = targets[i].GetComponent<RectTransform>();
            rect.anchoredPosition3D = new Vector3(rect.anchoredPosition.x, rect.anchoredPosition.y, 0);
        }
        SetTargetCount();
    }
    public void SetTargetCount()
    {
        int attackSpeed = SaveSystem.Instance.GetCurrentWeapon().GetProperty("Attack Speed").value;

        for (int i = 0; i < targets.Length; i++)
        {
            targets[i].SetActive(false);
        }
        for (int i = 0; i < attackSpeed; i++)
        {
            targets[i].SetActive(true);
        }
    }
    public int AdjustTargetCount(int count, bool add)
    {
        int targetsAdjusted = 0;

        for (int i = 0; i < targets.Length; i++)
        {
            if(count <= 0)
                break;

            if (add && !targets[i].activeInHierarchy && count > 0)
            {
                targets[i].SetActive(true);
                RandomPos(targets[i]);
                if(current_State == BattleState.CastAnimation || current_State == BattleState.Fight || (current_State == BattleState.Channeling && !DrawUtility.Instance.isGameStarted))
                targets[i].GetComponent<Image>().enabled = true;
                else targets[i].GetComponent<Click_Target>().ChangeState(TargetState.Inactive);

                count--;
            }
            if (!add && targets[i].activeInHierarchy && count > 0)
            {
                int active = targets.Count(t => t.activeInHierarchy);

                Debug.Log(active);
                if(active == 1)
                    return targetsAdjusted;

                targets[i].GetComponent<Image>().enabled = false;
                targets[i].SetActive(false);
                targetsAdjusted++;
                count--;
            }
        }
        return targetsAdjusted;
    }
    public void RandomPos(GameObject target)
    {
        Vector2 randomPosition = new Vector2(Random.Range(-200, 200), Random.Range(-200, 300));

        for (int i = 0; i < GetTargetList(target).Count; i++)
        {
            if (Vector2.Distance(randomPosition, GetTargetList(target)[i].GetComponent<RectTransform>().anchoredPosition) < 126)
            {
                randomPosition = new Vector2(Random.Range(-200, 200), Random.Range(-200, 300));
                //Reset loop
                i = -1;
            }
        }
        //Debug.Log(randomPosition);
        target.GetComponent<RectTransform>().anchoredPosition = randomPosition;
    }
    private void SetTargetsPositions()
    {
        for (int i = 0; i < targets.Length; i++)
        {
            if(targets[i].activeInHierarchy)
            RandomPos(targets[i]);
        }
    }
    private List<GameObject> GetTargetList(GameObject target)
    {
        return targets.Where(t => t.activeInHierarchy && t != target).ToList();
    }
    private void AddTargetEvent(EventHandler ev)
    {
        for (int i = 0; i < targets.Length; i++)
        {
            if(targets[i].activeInHierarchy)
            targets[i].GetComponent<Click_Target>().OnClick += ev;
        }
    }
    private void RemoveTargetEvent(EventHandler ev)
    {
        for (int i = 0; i < targets.Length; i++)
        {
            if(targets[i].activeInHierarchy)
            targets[i].GetComponent<Click_Target>().OnClick -= ev;
        }
    }
    private void ResetEvents()
    {
        for (int i = 0; i < targets.Length; i++)
        {
            targets[i].GetComponent<Click_Target>().OnClick = null;
        }
        OnAttack = null;
    }
    private void ActivateTargets()
    {
      
        if (finalBlow.IsActive || current_State == BattleState.Victory || current_State == BattleState.Defeat)
            return;

        for (int i = 0; i < targets.Length; i++)
        {
            if(targets[i].activeInHierarchy)
            targets[i].GetComponent<Click_Target>().SwitchToStaticState();
        }
        SetTargetsPositions();
    }
    private void DisableTargets()
    {
        
        for (int i = 0; i < targets.Length; i++)
        {
            if(targets[i].activeInHierarchy)
            targets[i].GetComponent<Click_Target>().SwitchToInactiveState();
        }
    }
    #endregion
    #region Final_Blow
    public void ActivateFinalBlow()
    {
        int damage = monsterInfo.DealDamageToMonster(heroInfo.GetHero().GetStat("Damage").GetActualValue(), heroInfo.GetHero().GetStat("Power"), DamageType.Normal);

        monsterInfo.ShakeEnemySprite(0.2f, 35);

        Create_Message(new Info(damage.ToString(), this.HexToColor("C48C28FF"), 100, InfoType.Damage), 0.5f, 3f);

        Statistics.Instance.statisticInfo.critsLanded.value++;
       
        Create_Message(new Info(heroInfo.HealHero(ProcessItems(PropertyType.Crit, Target.Player, "Health")), this.HexToColor("00b600"), 50, InfoType.Health), 0.5f, 3f);
        Create_Message(new Info(heroInfo.AdjustMana(ProcessItems(PropertyType.Crit, Target.Player, "Mana"),AdjustType.Percentage), this.HexToColor("00cbc9"), 50, InfoType.Mana), 0.5f, 3f);
        monsterInfo.AdjustMana(ProcessItems(PropertyType.Crit, Target.Enemy, "Mana"),AdjustType.Percentage);

        GameObject slice = Instantiate(Resources.Load("Prefabs/Battle/CritSlice") as GameObject, new Vector2(0, 0), Quaternion.identity, GameObject.Find("Instantiated_Objects").transform);

        RectTransform rect = slice.GetComponent<RectTransform>();
        rect.anchoredPosition3D = new Vector3(0, 100, 0);

        slice.GetComponent<Image>().CrossFadeAlpha(0, 0.7f, true);
        Destroy(slice, 0.7f);

        finalBlow.Disable();
        ActivateTargets();
    }
    public void AddFinalBlowCharge()
    {
        finalBlow.AddCharges();

        if (finalBlow.IsActive)
            DisableTargets();
    }
    #endregion
    #region Other
    public void UsePotion()
    {
        Debug.Log(SaveSystem.Instance.PotionExists());
        if(!drinking && SaveSystem.Instance.PotionExists())
        potion_Coroutine = StartCoroutine(PotionCoroutine(3));
    }
    private IEnumerator PotionCoroutine(float time)
    {
        SaveSystem.Instance.DestroyPotion();
        potionFiller.CrossFadeAlpha(0.2f,0,true);
        Create_Message(new Info(heroInfo.HealHero(50), this.HexToColor("00b600"), 50, InfoType.Health), 0.5f, 3f);
        drinking = true;

        float t = 0f;

        while (t <= 1)
        {
            t += Time.deltaTime / time;
            potionFiller.fillAmount = Mathf.Lerp(0, 1, t);
            yield return null;
        }
        potionFiller.CrossFadeAlpha(SaveSystem.Instance.PotionExists() ? 1 : 0.2f, 0, true);
        drinking = false;
    }
    private void SetPotionFiller()
    {
        potionFiller.fillAmount = 1;
        drinking = false;
        potionFiller.CrossFadeAlpha(SaveSystem.Instance.PotionExists() ? 1 : 0.2f, 0, true);
    }
    private int ProcessItems(PropertyType type,Target target,string statName)
    {
        List<Item> runes = SaveSystem.Instance.runes;
        int statAmount = 0;


        Stat stat = target == Target.Enemy ? monsterInfo.GetMonster().GetStat(statName) : heroInfo.GetHero().GetStat(statName);
       
        for (int i = 0; i < runes.Count; i++)
        {
            for (int j = 0; j < runes[i].properties.Count; j++)
            {
                if (runes[i].properties[j].propertyType == type && runes[i].properties[j].name == statName && runes[i].properties[j].target == target)
                {
                    statAmount += runes[i].properties[j].value;
                    Debug.Log(statAmount);
                }
            }
        }

        Property property = SaveSystem.Instance.GetCurrentWeapon().GetProperty(statName, type);

        if (target == Target.Player && property != null)
            statAmount += property.value;

        return statAmount;
    }
    public void DealDamage()
    {
        if (!guardGame.isInTargetArea)
        {
            if (Main_Manager.Instance.showTutorial)
                return;

            StopCoroutine(guard_Coroutine);

            ChangeState(BattleState.Swing);

            int damage;
            if (monsterInfo.GetMonster().GetStat("Crit").baseValue == 0)
            {
                damage = monsterInfo.GetMonster().GetStat("Damage").GetActualValue() *
                         (int) ((float) monsterInfo.GetMonster().GetStat("Power").GetActualValue() / 100);

                monsterInfo.GetMonster().GetStat("Crit").Restore();
                Debug.Log(monsterInfo.GetMonster().GetStat("Crit").baseValue);

            }
            else
            {                
                damage = monsterInfo.GetMonster().GetStat("Damage").GetActualValue();
                monsterInfo.GetMonster().GetStat("Crit").ChangeCurrent(-1);
            }



            Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Normal), Color.red, 50,
                InfoType.Health), 0.5f, 3f);



            OnAttackEvent();

        }
        else
        {
            ChangeState(BattleState.Swing);

            if (monsterInfo.GetMonster().GetStat("Crit").baseValue == 0)
                monsterInfo.GetMonster().GetStat("Crit").Restore();
            else
                monsterInfo.GetMonster().GetStat("Crit").ChangeCurrent(-1);

            Create_Message(new Info(heroInfo.HealHero(ProcessItems(PropertyType.Block, Target.Player, "Health")), this.HexToColor("00b600"), 50, InfoType.Health), 0.5f, 3f);
            guardGame.effect.SetActive(true);
            StopCoroutine(guard_Coroutine);
            Statistics.Instance.statisticInfo.successBlocks.value++;


            if (Main_Manager.Instance.showTutorial)
            {
                Time.timeScale = 1;
                Tutorial.Instance.Procceed();
            }
        }


    }
    public void Start_Battle()
    {
        if(Main_Manager.Instance.showTutorial)
            Tutorial.Instance.Procceed();
        ChangeState(BattleState.Prepare);
    }
    private void OnAttackEvent()
    {
        if (OnAttack != null)
            OnAttack();
    }
    public void Guard()
    {
       guard_Coroutine = StartCoroutine(guardGame.Play());
    }
    public void Create_Message(Info message,float liveTime, float speed)
    {
        //To Do: if Player is dead make a new scene?
        if (SaveSystem.isPlayerDead)
            return;

        GameObject text = textPool.FirstOrDefault(t => !t.activeInHierarchy);

        text.SetActive(true);

        text.GetComponent<TextMeshProUGUI>().text = message.info_Data;
        text.GetComponent<TextMeshProUGUI>().color = message.color;
        text.GetComponent<TextMeshProUGUI>().fontSize = message.size;

        switch (message.info_type)
        {
            case InfoType.Health:
                text.GetComponent<RectTransform>().pivot = new Vector2(1, 0.5f);
                text.GetComponent<RectTransform>().anchoredPosition = new Vector2(-70, -350);
                break;
            case InfoType.Mana:
                text.GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
                text.GetComponent<RectTransform>().anchoredPosition = new Vector2(70, -350);
                break;
            case InfoType.Damage:
                text.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                text.GetComponent<RectTransform>().anchoredPosition = message.coords == Vector2.zero ? new Vector2(Random.Range(-200, 200), 250) : message.coords;
                break;
            case InfoType.Default:
                text.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                text.GetComponent<RectTransform>().anchoredPosition = message.coords;
                break;
        }
        if (message.info_type != InfoType.Default)
            text.GetComponent<MonoBehaviour>().StartCoroutine(MoveText(text, liveTime, speed));
        else
            text.GetComponent<MonoBehaviour>().StartCoroutine(FadeText(text, liveTime));
    }
    private IEnumerator MoveText(GameObject text, float live_time, float speed)
    {
        RectTransform rect = text.GetComponent<RectTransform>();
        float t = 0f;

        while (t <= 1)
        {
            t += Time.deltaTime / live_time;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y + speed);
            yield return null;
        }

        t = 0;
        text.GetComponent<TextMeshProUGUI>().CrossFadeAlpha(0, live_time, true);

        while (t <= 1)
        {
            t += Time.deltaTime / live_time;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y + speed);
            yield return null;
        }
        text.GetComponent<MonoBehaviour>().StopAllCoroutines();
        //Resources.UnloadUnusedAssets();
        text.SetActive(false);
    }
    private IEnumerator FadeText(GameObject text, float live_time)
    {
        text.GetComponent<TextMeshProUGUI>().CrossFadeAlpha(0, live_time, true);
        yield return new WaitForSeconds(live_time);
        text.GetComponent<MonoBehaviour>().StopAllCoroutines();
        //Resources.UnloadUnusedAssets();
        text.SetActive(false);
    }
    private void CreatePool(int textAmount)
    {
        textPool = new List<GameObject>();

        for (int i = 0; i < textAmount; i++)
        {
            GameObject text = Instantiate(Resources.Load("Prefabs/Battle/Damage_Text") as GameObject, new Vector2(0, 0), Quaternion.identity, GameObject.Find("Instantiated_Objects").transform);

            RectTransform rect = text.GetComponent<RectTransform>();
            rect.anchoredPosition3D = new Vector3(rect.anchoredPosition.x, rect.anchoredPosition.y, 0);

            text.SetActive(false);

            textPool.Add(text);
        }
    }
    public void KillMonster()
    {
        Statistics.Instance.statisticInfo.monstersKilled.value++;
        ChangeState(BattleState.Victory);
    }

    public void Victory()
    {
        SceneSwitcher.Instance.Load_Scene("UI");
        if(Main_Manager.Instance.showTutorial)
        Tutorial.Instance.Procceed();
    }
    #endregion
    #region Spell_Methods;
    public void ActivateDrawGame()
    {
        if (current_State == BattleState.Channeling && !BuffExists("Silence", Target.Player) && spellSteal.can_steal_spell && !DrawUtility.Instance.isGameStarted)
        {          
            DisableTargets();
            finalBlow.ToggleSprite(false);
            DrawUtility.Instance.ToggleGame(true);
            if(Main_Manager.Instance.showTutorial)
            Tutorial.Instance.Procceed();          
        }
    }
    public void StealSpell(bool success)
    {        
        if (success)
        {
            Statistics.Instance.statisticInfo.spellsStolen.value++;
            Database_Saves.Instance.GetHero().StealSpell(current_Spell.spell_name);
            Database_Saves.Instance.Save();
            spellSteal.Steal_Spell(current_Spell, this);
            ResetSpell();

            if (Main_Manager.Instance.showTutorial)
            {
                Tutorial.Instance.Procceed();
            }
        }
        else
            ChangeState(BattleState.CastAnimation);
    }
    private void ResetSpell()
    {
        if (current_State == BattleState.Channeling)
        {
            current_Spell = null;
            ChangeState(BattleState.Fight);
        }
    }
    public void CastSpell()
    {
        if (current_State == BattleState.Channeling || current_State == BattleState.CastAnimation || current_State == BattleState.Fight)
        {
            if (Main_Manager.Instance.showTutorial && spellSteal.spell != null)
            {
                KillMonster();
                Time.timeScale = 1;
                Tutorial.Instance.Procceed();
            }
            else
                spellSteal.CastSpell(this);
        }
            
    }
    private void InitEnemySkills()
    {
        skillManager = new SkillManager(
            //
            //Buffs
            new[]
            {
                new Buff("Withering", 1, WitheringDebuff),
                new Buff("Soul Cry", new StatInfo("Damage", "Power", 7.5f, 12.5f, AdjustType.Percentage)),
                new Buff("Decay", new StatInfo("Defence", "Power", 10, 8)),
                new Buff("Decrepify"),
                new Buff("Exhaust", 1, ExhaustDebuff),
                new Buff("Soul Torment", 1, SoulTormentDebuff,
                    new StatInfo("Defence", "Power", 15, 15),
                    new StatInfo("Damage", "Power", 10, 10, AdjustType.Percentage),
                    new StatInfo("Power", "Magic", 50, 50),
                    new StatInfo("Magic", "Magic", 50, 50),
                    new StatInfo("Crit", "Magic", -1, -1))
            },
            new[]
            {
                new Spell("Unholy Smite", UnholySmite, 1, 20, 20, SpellType.Strong, 4f, 1),
                new Spell("Withering", Withering, 3, 20, 25, SpellType.Strong, 4f, 1),
                new Spell("Agony", Agony, 3, 5, 10, SpellType.Weak, 3f, 1),
                new Spell("Soul Cry", SoulCry, 3, 20, 15, SpellType.Weak, 3f, 1),
                new Spell("Decrepify", Decrepify, 0.1f, 20, 15, SpellType.Weak, 3f, 1),
                new Spell("Life Tap", LifeTap, 1, 20, 25, SpellType.Strong, 4f, 1),
                new Spell("Unholy Heal", UnholyHeal, 2, 20, 15, SpellType.Weak, 3f, 1),
                new Spell("Exhaust", Exhaust, 1, 20, 30, SpellType.Weak, 5f, 2),
                new Spell("Decay", Decay, 1, 20, 15, SpellType.Strong, 4f, 1),
                new Spell("Soul Torment", SoulTorment, 25, 40, 40, SpellType.Strong, 6f, 2)
            }
        );
    }
    private void ChooseSpell()
    {
        current_Spell = null;
 
        List<string> regularSpellPool = new List<string>(monsterInfo.GetMonster().regularSpellPool);
        List<string> availableSpellPool = new List<string>();

        int monsterCurrentHP = monsterInfo.GetMonster().GetStat("Health").baseValue;
        int monsterMaxHP = monsterInfo.GetMonster().GetStat("Health").maxValue;

        if (monsterCurrentHP <= monsterMaxHP / 100 * monsterInfo.GetMonster().threshHoldPerecent)           
        {
            regularSpellPool.AddRange(monsterInfo.GetMonster().threshholdSpellPool);
            Debug.Log("Threshhold added");
        }

        if (usedSpell != null)
        {
            availableSpellPool = regularSpellPool;
            availableSpellPool.Remove(usedSpell);
        }
        else
            availableSpellPool = regularSpellPool;

   

        for (int i = availableSpellPool.Count - 1; i >= 0; i--)
        {
            if (skillManager.GetSpell(availableSpellPool[i]).enemyManaCost > monsterInfo.GetMonster().GetStat("Mana").baseValue)
                availableSpellPool.RemoveAt(i);
        }


        if (availableSpellPool.Count > 0)
        {
            current_Spell = skillManager.GetSpell(availableSpellPool[Random.Range(0, availableSpellPool.Count)]);
            usedSpell = current_Spell.spell_name;
        }
        else
        {
            availableSpellPool = regularSpellPool;

            for (int i = availableSpellPool.Count - 1; i >= 0; i--)
            {
                if (skillManager.GetSpell(availableSpellPool[i]).enemyManaCost > monsterInfo.GetMonster().GetStat("Mana").baseValue)
                    availableSpellPool.RemoveAt(i);
            }

            current_Spell = availableSpellPool.Count > 0 ? skillManager.GetSpell(availableSpellPool[Random.Range(0, availableSpellPool.Count)]) : null;

            if (current_Spell != null)
                usedSpell = current_Spell.spell_name;
        }
    }
    private void ProcessBuff(string buffName, Target target, float duration, IconType type)
    {
        Buff current_Buff = new Buff(skillManager.GetBuff(buffName));

        if (!BuffExists(buffName, target))
        {
            Debug.Log("Duration of " + current_Buff.skill_Name + " is: " + duration);
            current_Buff.SetDuration(duration);
            current_Buff.SetTarget(target);

            SetBuffIcon(current_Buff, type);

            active_buffs.Add(current_Buff);

            current_Buff.ToggleBuff(true, current_Buff.target);
        }
        else
        {
            current_Buff = active_buffs.FirstOrDefault(b => b.skill_Name == current_Buff.skill_Name && b.target == target);
            Debug.Log("Duration of " + current_Buff.skill_Name + " is: " + duration);
            current_Buff.SetDuration(duration);

            if (current_Buff.skill_Name == "Decrepify" && target == Target.Player)
                fightTime += (float) heroInfo.GetHero().GetStat("Magic").GetActualValue() / 100;

            if (current_Buff.skill_Name == "Soul Torment" && target == Target.Player)
                fightTime += (float) heroInfo.GetHero().GetStat("Magic").GetActualValue() / 100 + (float) heroInfo.GetHero().GetStat("Power").GetActualValue() / 100;


            Debug.Log(current_Buff.skill_Name + " already exists");
        }
    }
    private void SetBuffIcon(Buff buff, IconType type)
    {
        GameObject buff_Slot = null;

        switch (type)
        {
            case IconType.Negative:
                buff_Slot = Instantiate(Resources.Load("Prefabs/Battle/Buff_Slot"), new Vector2(0, 0), Quaternion.identity) as GameObject;
                buff_Slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("Negative_Buff");
                break;
            case IconType.Positive:
                buff_Slot = Instantiate(Resources.Load("Prefabs/Battle/Buff_Slot"), new Vector2(0, 0), Quaternion.identity) as GameObject;
                buff_Slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("Positive_Buff");
                break;
        }
        switch (buff.target)
        {
            case Target.Player:
                buff_Slot.transform.SetParent(GameObject.Find("Player_Buffs").transform);
                break;
            case Target.Enemy:
                buff_Slot.transform.SetParent(GameObject.Find("Enemy_Buffs").transform);
                break;
        }
        RectTransform rect = buff_Slot.GetComponent<RectTransform>();
        rect.anchoredPosition3D = new Vector3(rect.anchoredPosition.x, rect.anchoredPosition.y, 0);

        buff_Slot.name = buff.skill_Name;
        buff_Slot.transform.SetAsFirstSibling();
        buff_Slot.transform.localScale = Vector3.one;
        buff_Slot.transform.GetChild(0).GetComponent<Image>().sprite = SaveSystem.GetSprite(buff.skill_Name);
        buff.SetSlot(buff_Slot);
    }
    private void CreateBuffIcon(IconType type, Target target,string skillName)
    {
        GameObject buff_Slot = null;

        string slotPath = "Sprites/CorePrototype/Battle/Battle Sprites";
        string spritePath = "Sprites/CorePrototype/Battle/Buffs/Spell Icons";

        switch (type)
        {
            case IconType.Negative:
                buff_Slot = Instantiate(Resources.Load("Prefabs/Battle/Buff_Slot"), new Vector2(0, 0), Quaternion.identity) as GameObject;
                buff_Slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("Negative_Buff");
                break;
            case IconType.Positive:
                buff_Slot = Instantiate(Resources.Load("Prefabs/Battle/Buff_Slot"), new Vector2(0, 0), Quaternion.identity) as GameObject;
                buff_Slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("Positive_Buff");
                break;
        }
        switch (target)
        {
            case Target.Player:
                buff_Slot.transform.SetParent(GameObject.Find("Player_Buffs").transform);
                break;
            case Target.Enemy:
                buff_Slot.transform.SetParent(GameObject.Find("Enemy_Buffs").transform);
                break;
        }
        buff_Slot.transform.FindDeepChild("Fade").GetComponent<Image>().enabled = false;
        RectTransform rect = buff_Slot.GetComponent<RectTransform>();
        rect.anchoredPosition3D = new Vector3(rect.anchoredPosition.x, rect.anchoredPosition.y, 0);

        buff_Slot.name = skillName;
        buff_Slot.transform.SetAsFirstSibling();
        buff_Slot.transform.localScale = Vector3.one;
        buff_Slot.transform.GetChild(0).GetComponent<Image>().sprite = SaveSystem.GetSprite(skillName);
    }
    private void ProccessBuffsDurations()
    {
        for (int i = active_buffs.Count - 1; i > -1; i--)
        {
            if (active_buffs[i].buff_Type == BuffType.Periodic)
            {
                active_buffs[i].periodicDuration.value -= Time.deltaTime;
                if (active_buffs[i].periodicDuration.value <= 0)
                {
                    active_buffs[i].TogglePeriodicAction(active_buffs[i].target);
                    active_buffs[i].ResetPeriodicDuration();
                }
            }

            active_buffs[i].baseDuration.value -= Time.deltaTime;
            active_buffs[i].buff_slot.transform.GetChild(1).GetComponent<Image>().fillAmount = active_buffs[i].baseDuration.value / active_buffs[i].baseDuration.maxValue;

           

            if (active_buffs[i].baseDuration.value <= 0)
            {
                active_buffs[i].ToggleBuff(false, active_buffs[i].target);

                Destroy(active_buffs[i].buff_slot);
                active_buffs.RemoveAt(i);
            }
        }
    }
    public bool BuffExists(string buff_name, Target target)
    {
        return active_buffs.Exists(b => b.skill_Name == buff_name && b.target == target);
    }
    private void DestroyBuffs()
    {
        for (int i = active_buffs.Count - 1; i > -1; i--)
        {
            active_buffs[i].ToggleBuff(false, active_buffs[i].target);

            Destroy(active_buffs[i].buff_slot);
            active_buffs.RemoveAt(i);
        }
        
        SaveSystem.GetChar().ResetStats();

        DestroyOnePhaseBuffs();
    }
    private void DestroyOnePhaseBuffs()
    {
        Transform enemyBuffs = GameObject.Find("Enemy_Buffs").transform;
        Transform playerBuffs = GameObject.Find("Player_Buffs").transform;


        for (int i = 0; i < playerBuffs.childCount; i++)
        {
            if(!BuffExists(playerBuffs.GetChild(i).name,Target.Player) && playerBuffs.GetChild(i).name != "ResaliaBuff")
            Destroy(playerBuffs.GetChild(i).gameObject);
        }

        for (int i = 0; i < enemyBuffs.childCount; i++)
        {
            if (!BuffExists(enemyBuffs.GetChild(i).name, Target.Enemy))
                Destroy(enemyBuffs.GetChild(i).gameObject);
        }
    }
    private void DisableText()
    {
        Transform objects = GameObject.Find("Instantiated_Objects").transform;
        for (int i = 0; i < objects.childCount; i++)
        {
            objects.GetChild(i).gameObject.SetActive(false);
        }
    }
    private void CreateResalia()
    {
        if(SaveSystem.Instance.blessedByResalia && GameObject.Find("ResaliaBuff") == null)
            CreateBuffIcon(IconType.Positive, Target.Player, "ResaliaBuff");
    }
    #region Skills
    #region Spells
    private void UnholySmite(User user)
    {
        switch (user)
        {
            case User.Player:
                monsterInfo.ShakeEnemySprite(0.2f, 50);
                Create_Message(new Info(monsterInfo.DealDamageToMonster(75, heroInfo.GetHero().GetStat("Power"), DamageType.Pierce).ToString(), Color.cyan, 100, InfoType.Damage), 0.5f, 3f);                
                break;
            case User.Enemy:
                Create_Message(new Info(heroInfo.DealDamageToHero(24, monsterInfo.GetMonster().GetStat("Power"), DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                break;
        }
    }
    private void Withering(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Withering", Target.Enemy, heroInfo.GetHero().GetStat("Magic").GetActualValue()/100 * 2, IconType.Negative);
                break;
            case User.Enemy:
                ProcessBuff("Withering", Target.Player, monsterInfo.GetMonster().GetStat("Magic").GetActualValue()/100 * 2, IconType.Negative);
                break;
        }
    }
    private void Agony(User user)
    {
        int damage;
        switch (user)
        {
            case User.Player:
                if (SaveSystem.Instance.blessedByResalia)
                {
                    damage = heroInfo.GetHero().GetStat("Health").maxValue;
                    Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);

                    damage = monsterInfo.GetMonster().GetStat("Health").maxValue;
                    Create_Message(new Info(monsterInfo.GetMonster().GetStat("Health").baseValue.ToString(), Color.red, 120, InfoType.Damage), 0.5f, 3f);
                    Create_Message(new Info(monsterInfo.DealDamageToMonster(damage, null, DamageType.Pierce).ToString(), Color.red, 50, InfoType.Health), 0.5f, 3f);
                    
                }
                else
                {
                    damage = heroInfo.GetHero().GetStat("Health").maxValue;
                    Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                    Create_Message(new Info(monsterInfo.GetMonster().GetStat("Health").baseValue.ToString(), Color.red, 120, InfoType.Damage), 0.5f, 3f);
                    monsterInfo.Agony();
                }
                break;

            case User.Enemy:
                if (SaveSystem.Instance.blessedByResalia)
                {
                    damage = heroInfo.GetHero().GetStat("Health").maxValue;
                    Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);

                    damage = monsterInfo.GetMonster().GetStat("Health").maxValue;
                    Create_Message(new Info(monsterInfo.GetMonster().GetStat("Health").baseValue.ToString(), Color.red, 120, InfoType.Damage), 0.5f, 3f);
                    Create_Message(new Info(monsterInfo.DealDamageToMonster(damage, null, DamageType.Pierce).ToString(), Color.red, 50, InfoType.Health), 0.5f, 3f);

                }
                else
                {
                    damage = heroInfo.GetHero().GetStat("Health").maxValue;
                    Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                    Create_Message(new Info(monsterInfo.GetMonster().GetStat("Health").baseValue.ToString(), Color.red, 120, InfoType.Damage), 0.5f, 3f);
                    monsterInfo.Agony();
                }
                break;
        }
    }
    private void SoulCry(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Soul Cry", Target.Enemy, (float)heroInfo.GetHero().GetStat("Magic").GetActualValue()/100 * 5f, IconType.Negative);
                break;
            case User.Enemy:
                ProcessBuff("Soul Cry", Target.Player, (float)monsterInfo.GetMonster().GetStat("Magic").GetActualValue()/100 * 10, IconType.Negative);
                break;
        }
    }
    private void LifeTap(User user)
    {
        int damage = 0;
        switch (user)
        {
            case User.Player:

                monsterInfo.ShakeEnemySprite(0.2f, 50);

                damage = (int) (75 * (float) monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100);

                Create_Message(new Info(monsterInfo.DealDamageToMonster(75, heroInfo.GetHero().GetStat("Magic"), DamageType.Pierce).ToString(), Color.cyan, 100, InfoType.Damage), 0.5f, 3f);

                Create_Message(new Info(heroInfo.HealHero(Mathf.FloorToInt(damage * 0.2f)), this.HexToColor("00b600"), 50, InfoType.Health), 0.5f, 3f);
                
                
                break;
            case User.Enemy:
                damage = (int) (25 * (float) monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100);

                Create_Message(new Info(heroInfo.DealDamageToHero(damage,null,DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);

                monsterInfo.HealMonster(7 * damage, AdjustType.Point);

                break;
        }
    }
    private void Decrepify(User user)
    {
        switch (user)
        {
            case User.Player:
                fightTime += (float) heroInfo.GetHero().GetStat("Magic").GetActualValue() / 100;

                CreateBuffIcon(IconType.Negative, Target.Enemy, "Decrepify");
                break;
            case User.Enemy:
                ProcessBuff("Decrepify", Target.Player, monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100 * 5, IconType.Negative);
                StartCoroutine(Decrepify());
                break;
        }
    }
    private void UnholyHeal(User user)
    {
        switch (user)
        {
            case User.Player:
                Create_Message(new Info(heroInfo.DealDamageToHero((int)((float)heroInfo.GetHero().GetStat("Health").maxValue / 100 * heroInfo.GetHero().GetStat("Magic").GetActualValue()/100 * 8)
                    , null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                break;
            case User.Enemy:
                monsterInfo.HealMonster((int)(18 * (float)monsterInfo.GetMonster().GetStat("Magic").GetActualValue()/100), AdjustType.Percentage);
                break;
        }
    }
    private void Exhaust(User user)
    {
        switch (user)
        {
            case User.Player:
                monsterInfo.ShakeEnemySprite(0.2f, 50);
                ProcessBuff("Exhaust", Target.Enemy, heroInfo.GetHero().GetStat("Magic").GetActualValue() / 100, IconType.Negative);
                Create_Message(new Info(monsterInfo.DealDamageToMonster(100, heroInfo.GetHero().GetStat("Magic"), DamageType.Pierce).ToString(), Color.cyan, 100, InfoType.Damage), 0.5f, 3f);               
                break;
            case User.Enemy:
                ProcessBuff("Exhaust", Target.Player, monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100, IconType.Negative);
                Create_Message(new Info(heroInfo.DealDamageToHero(20, monsterInfo.GetMonster().GetStat("Magic"), DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                break;
        }
    }
    private void Decay(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Decay", Target.Enemy, (float)heroInfo.GetHero().GetStat("Magic").GetActualValue() / 100 * 4, IconType.Negative);
                break;
            case User.Enemy:
                ProcessBuff("Decay", Target.Player, (float)monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100 * 10f, IconType.Negative);
                break;
        }
    }
    private void SoulTorment(User user)
    {
        switch (user)
        {
            case User.Player:
                fightTime += (float) heroInfo.GetHero().GetStat("Magic").GetActualValue() / 200 + (float) heroInfo.GetHero().GetStat("Power").GetActualValue() / 200;
                ProcessBuff("Soul Torment", Target.Enemy,
                    (int)(((float) heroInfo.GetHero().GetStat("Magic").GetActualValue() / 100 + (float)heroInfo.GetHero().GetStat("Power").GetActualValue() / 100) * 2f) , IconType.Negative);
                break;
            case User.Enemy:
                ProcessBuff("Soul Torment", Target.Player,
                    (int)(((float) monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100 + (float) monsterInfo.GetMonster().GetStat("Power").GetActualValue() / 100) * 2) , IconType.Negative);
                StartCoroutine(SoulTorment());
                break;
        }
    }

    /*private void Fireball(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Ignite", Target.Enemy);
                Create_Message(new Info(monsterInfo.DealDamageToMonster(75, heroInfo.GetHero().GetStat("Magic"), DamageType.Pierce).ToString(), this.HexToColor("FF6F00FF"), 100, InfoType.Damage), 0.5f, 3f);
                monsterInfo.ShakeEnemySprite(0.2f, new Vector2(0.2f, 0.2f));
                ResetSpell();
                break;
            case User.Enemy:
                ProcessBuff("Ignite", Target.Player);
                Create_Message(new Info(heroInfo.DealDamageToHero(10, monsterInfo.GetMonster().GetStat("Magic"), DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);

                finalBlow.Disable();
                break;
        }
    }
    private void PowerStrike(User user)
    {
        switch (user)
        {
            case User.Player:
                Create_Message(new Info(monsterInfo.DealDamageToMonster(75, heroInfo.GetHero().GetStat("Power"), DamageType.Pierce).ToString(), Color.cyan, 100, InfoType.Damage), 0.5f, 3f);
                monsterInfo.ShakeEnemySprite(0.2f, new Vector2(0.2f, 0.2f));
                ResetSpell();
                break;
            case User.Enemy:
                Create_Message(new Info(heroInfo.DealDamageToHero(30, monsterInfo.GetMonster().GetStat("Power"), DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                finalBlow.Disable();
                break;
        }
    }
    private void ManaSplash(User user)
    {
        switch (user)
        {
            case User.Player:
                Create_Message(new Info(monsterInfo.DealDamageToMonster(90, heroInfo.GetHero().GetStat("Magic"), DamageType.Pierce).ToString(), Color.cyan, 100, InfoType.Damage), 0.5f, 3f);
                monsterInfo.ShakeEnemySprite(0.2f, new Vector2(0.2f, 0.2f));
                ResetSpell();
                break;
            case User.Enemy:
                Create_Message(new Info(heroInfo.DealDamageToHero(12, monsterInfo.GetMonster().GetStat("Magic"), DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                finalBlow.Disable();
                break;
        }
    }
    private void LesserHeal(User user)
    {
        switch (user)
        {
            case User.Player:
                Create_Message(new Info(heroInfo.HealHero(20), this.HexToColor("00b600"), 50, InfoType.Health), 0.5f, 3f);
                break;
            case User.Enemy:
                monsterInfo.HealMonster(20);
                break;
        }
    }
    private void ManaLeak(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Mana Leak", Target.Player);
                break;
            case User.Enemy:
                ProcessBuff("Mana Leak", Target.Enemy);
                break;
        }
    }
    private void BloodLust(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Bloodlust", Target.Player);
                break;
            case User.Enemy:
                ProcessBuff("Bloodlust", Target.Enemy);
                break;
        }
    }
    private void Silence(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Silence", Target.Enemy);
                ResetSpell();
                break;
            case User.Enemy:
                ProcessBuff("Silence", Target.Player);
                break;
        }
    }
    private void Devour(User user)
    {
        int manaBurnt;

        switch (user)
        {
            case User.Player:

                manaBurnt = (int) Math.Ceiling((float) monsterInfo.GetMonster().GetStat("Mana").maxValue / 100 * 25);
                Debug.Log(manaBurnt);
                manaBurnt = monsterInfo.AdjustMana(-manaBurnt, AdjustType.Point);
                Debug.Log(manaBurnt);
                Create_Message(new Info(heroInfo.AdjustMana(-manaBurnt, AdjustType.Point), this.HexToColor("00cbc9"), 50, InfoType.Mana), 0.5f, 3f);
                break;
            case User.Enemy:
                manaBurnt = (int) Math.Ceiling((float) heroInfo.GetHero().GetStat("Mana").maxValue / 100 * 25);

                manaBurnt = heroInfo.GetHero().GetStat("Mana").GetAdjusted(-manaBurnt);

                monsterInfo.AdjustMana(-manaBurnt, AdjustType.Point);

                Create_Message(new Info(heroInfo.AdjustMana(manaBurnt, AdjustType.Point), this.HexToColor("00cbc9"), 50, InfoType.Mana), 0.5f, 3f);
                break;
        }
    }
    private void Curse(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Curse", Target.Enemy);
                break;
            case User.Enemy:
                ProcessBuff("Curse", Target.Player);
                break;
        }
    }
    private void DefensiveStance(User user)
    {
        switch (user)
        {
            case User.Player:
                ProcessBuff("Defensive Stance", Target.Player);
                break;
            case User.Enemy:
                ProcessBuff("Defensive Stance", Target.Enemy);
                break;
        }
    }*/
    #endregion
    #region Buffs/Debuffs
    private IEnumerator Decrepify()
    {
        int targetCount = (int)(1.25f * monsterInfo.GetMonster().GetStat("Magic").GetActualValue()/100);

        targetCount = AdjustTargetCount(targetCount,false);

        yield return new WaitUntil(() => !BuffExists("Decrepify",Target.Player) || current_State == BattleState.Defeat || current_State == BattleState.Victory);

        AdjustTargetCount(targetCount, true);
    }
    private IEnumerator SoulTorment()
    {
        int targetCount = (int)(0.7f * monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100);

        targetCount = AdjustTargetCount(targetCount, false);

        yield return new WaitUntil(() => !BuffExists("Soul Torment",Target.Player));

        AdjustTargetCount(targetCount, true);
    }
    private void WitheringDebuff(Target target)
    {
        int damage;

        switch (target)
        {
            case Target.Player:
                damage = (int)((float)heroInfo.GetHero().GetStat("Health").maxValue / 100 * 5); 
                Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                break;

            case Target.Enemy:
                damage = (int)((float)monsterInfo.GetMonster().GetStat("Health").maxValue / 100 * 3);
                Create_Message(new Info(monsterInfo.DealDamageToMonster(damage, null, DamageType.Pierce).ToString(), Color.red, 50, InfoType.Damage), 0.5f, 3f);
                break;
        }
    }
    private void ExhaustDebuff(Target target)
    {
        int damage;
        switch (target)
        {
            case Target.Player:
                damage = (int)((float)heroInfo.GetHero().GetStat("Magic").GetActualValue() / 100 * 3);
                Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                break;

            case Target.Enemy:
                damage = (int)((float)monsterInfo.GetMonster().GetStat("Magic").GetActualValue() / 100 * 8);
                Create_Message(new Info(monsterInfo.DealDamageToMonster(damage, null, DamageType.Pierce).ToString(), Color.red, 50, InfoType.Damage), 0.5f, 3f);
                break;
        }
    }
    private void SoulTormentDebuff(Target target)
    {
        int damage;

        switch (target)
        {
            case Target.Player:
                damage = (int)((float)heroInfo.GetHero().GetStat("Health").maxValue / 100);
                Create_Message(new Info(heroInfo.DealDamageToHero(damage, null, DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                break;

            case Target.Enemy:
                damage = (int)((float)monsterInfo.GetMonster().GetStat("Health").maxValue / 100);
                Create_Message(new Info(monsterInfo.DealDamageToMonster(damage, null, DamageType.Pierce).ToString(), Color.red, 50, InfoType.Damage), 0.5f, 3f);
                break;
        }
    }
    /*private void Silence_Debuff(bool activate,Target target)
    {
        switch (target)
        {
            case Target.Player:

                break;

            case Target.Enemy:

                break;
        }
    }
    private void ManaLeak_Buff(bool activate, Target target)
    {
        switch (target)
        {
            case Target.Player:
                if (activate)
                {
                    AddTargetEvent(RestoreHeroMana);
                }
                else
                {
                    RemoveTargetEvent(RestoreHeroMana);
                }
                break;

            case Target.Enemy:
                if (activate)
                {
                    OnAttack += RestoreMonsterMana;
                }
                else
                {
                    OnAttack -= RestoreMonsterMana;
                }
                break;
        }
    }
    private void Ignite_Debuff(bool activate, Target target)
    {
        switch (target)
        {
            case Target.Player:
                Create_Message(new Info(heroInfo.DealDamageToHero(1, monsterInfo.GetMonster().GetStat("Magic"), DamageType.Pierce), Color.red, 50, InfoType.Health), 0.5f, 3f);
                break;

            case Target.Enemy:
                Create_Message(new Info(monsterInfo.DealDamageToMonster(5, heroInfo.GetHero().GetStat("Magic"), DamageType.Pierce).ToString(), this.HexToColor("FF6F00FF"), 50, InfoType.Damage), 0.5f, 3f);
                break;
        }
    }*/
    #endregion
    #endregion
    #endregion
}

#region classes
public class Info
{
    public Color color { get; private set; }
    public string info_Data { get; private set; }
    public int size { get; private set; }
    public InfoType info_type { get; private set; }

    public Vector2 coords { get; private set; }


    public Info(string info_Data, Color color, int size, InfoType info_type)
    {
        this.color = color;
        this.info_Data = info_Data;
        this.size = size;
        this.info_type = info_type;
    }
    public Info(string info_Data, Color color, int size, InfoType info_type, Vector2 coords)
    {
        this.color = color;
        this.info_Data = info_Data;
        this.size = size;
        this.info_type = info_type;
        this.coords = coords;
    }
}
public class GuardGame
{
    private GameObject target_circle { get; set; }
    private GameObject big_circle { get; set; }
    private GameObject small_circle { get; set; }
    public GameObject guard { get;private set; }
    public GameObject effect { get; private set; }
    public bool isInTargetArea { get; private set; }

    public GuardGame(Transform transform)
    {
        target_circle = transform.FindDeepChild("TargetCircle").gameObject;
        big_circle = transform.FindDeepChild("BigCircle").gameObject;
        small_circle = transform.FindDeepChild("SmallCircle").gameObject;
        guard = transform.FindDeepChild("Guard").gameObject;
        effect = transform.FindDeepChild("Guard_Effect").gameObject;
        effect.SetActive(false);
        guard.SetActive(false);
    }

    public IEnumerator Play()
    {
        Setting setting = Database_Settings.Instance.GetSettings();

        yield return new WaitForSeconds((float)setting.GuardDelay);

        guard.SetActive(true);
        effect.SetActive(false);

        isInTargetArea = false;

        target_circle.GetComponent<Image>().SetNativeSize();

        RectTransform big = big_circle.GetComponent<RectTransform>();
        RectTransform small = small_circle.GetComponent<RectTransform>();
        RectTransform target = target_circle.GetComponent<RectTransform>();

        small_circle.GetComponent<Image>().color = Color.cyan;
        big_circle.GetComponent<Image>().color = Color.cyan;

        float size = setting.circle_radius.Random();
        float checkDist = size + size / 35;

        big.sizeDelta = new Vector2(size, size);
        small.sizeDelta = new Vector2(big.sizeDelta.x - big.sizeDelta.x / 5, big.sizeDelta.y - big.sizeDelta.y / 5);



        float t = 0;
        float guard_time = setting.GuardTime.Random();

        while (t <= 1)
        {
            t += Time.deltaTime / guard_time;
            target.sizeDelta = Vector2.Lerp(new Vector2(530, 530), Vector2.zero, t);

            if (target.sizeDelta.x < checkDist && target.sizeDelta.x > small.sizeDelta.x)
            {
                small_circle.GetComponent<Image>().color = Color.green;
                big_circle.GetComponent<Image>().color = Color.green;
                isInTargetArea = true;

                if (Main_Manager.Instance.showTutorial)
                {
                    if(target.sizeDelta.x < (big.sizeDelta.x + small.sizeDelta.x)/2)
                    {
                        Time.timeScale = 0;
                        if(Tutorial.Instance.currentState == TutorialState.WaitForGuard)
                        Tutorial.Instance.Procceed();
                    }                        
                }

            }
            else if (target.sizeDelta.x < checkDist && target.sizeDelta.x < small.sizeDelta.x)
            {
                small_circle.GetComponent<Image>().color = Color.red;
                big_circle.GetComponent<Image>().color = Color.red;
                isInTargetArea = false;
            }
            yield return null;
        }
        Battle_Manager.Instance.DealDamage();
    }
}
public class Final_Blow
{
    private Image finalBlowImage { get; set; }
    private Image finalBlowFiller { get; set; }
    private float finalBlowCharge { get; set; }

    private bool active { get; set; }
    public bool IsActive
    {
        get
        {
            return active;
        }
    }


    public Final_Blow()
    {
        finalBlowImage = GameObject.Find("FinalBlowTarget").GetComponent<Image>();
        finalBlowFiller = GameObject.Find("Final_Blow_Filler").GetComponent<Image>();
        finalBlowCharge = 0;
    }

    public void Disable()
    {
        finalBlowImage.enabled = false;
        finalBlowCharge = 0;
        finalBlowFiller.fillAmount = 0;
        active = false;
    }
    public void ToggleSprite(bool active)
    {
        finalBlowImage.enabled = active;
    }

    
    public void AddCharges()
    {
        finalBlowCharge++;

        finalBlowFiller.fillAmount = finalBlowCharge / HeroInfo.Instance.GetHero().GetStat("Crit").baseValue;

        if (finalBlowCharge == HeroInfo.Instance.GetHero().GetStat("Crit").baseValue)
        {
            finalBlowImage.enabled = true;
            active = true;
        }
    }
}
#region Skills
public class SkillManager
{
    public Buff[] buffs;
    public Spell[] spells;

    public SkillManager(Buff[] buffs, Spell[] spells)
    {
        this.buffs = buffs;
        this.spells = spells;
    }
    public Buff GetBuff(string skillName)
    {
        return Array.Find(buffs, skill => skill.skill_Name == skillName);
    }
    public Spell GetSpell(string spell_name)
    {
        return new Spell(Array.Find(spells, spell => spell.spell_name == spell_name));
    }
}
public class StatInfo
{
    private AdjustType adjustType { get; set; }
    private Modifier mod { get; set; }
    
    private string changingStatName;
    private string dependentStatName;

    private float playerMultiplier { get; set; }
    private float enemyMultiplier { get; set; }

    private Stat changingStat;

    public StatInfo(string changingStatName,string dependentStatName, float playerMultiplier, float enemyMultiplier,AdjustType adjustType = AdjustType.Point)
    {
        this.changingStatName = changingStatName;
        this.enemyMultiplier = enemyMultiplier;
        this.dependentStatName = dependentStatName;
        this.playerMultiplier = playerMultiplier;
        this.adjustType = adjustType;
    }

    public void ChangeStat(bool apply,Target target)
    {
        if (apply)
        {
            Stat dependentStat;

            switch (target)
            {
                case Target.Player:

                    changingStat = HeroInfo.Instance.GetHero().GetStat(changingStatName);
                    dependentStat = MonsterInfo.Instance.GetMonster().GetStat(dependentStatName);

                    switch (adjustType)
                    {
                        case AdjustType.Point:
                            mod = new Modifier((int) ((float) -dependentStat.GetActualValue() / 100 * playerMultiplier));
                            break;

                        case AdjustType.Percentage:
                            mod = new Modifier((int) ((float) -changingStat.baseValue / 100 * dependentStat.GetActualValue() / 100 * enemyMultiplier));
                            break;
                    }
                    break;

                case Target.Enemy:

                    changingStat = MonsterInfo.Instance.GetMonster().GetStat(changingStatName);
                    dependentStat = HeroInfo.Instance.GetHero().GetStat(dependentStatName);

                    switch (adjustType)
                    {
                        case AdjustType.Point:
                            mod = new Modifier((int) ((float) -dependentStat.GetActualValue() / 100 * playerMultiplier));
                            break;

                        case AdjustType.Percentage:
                            mod = new Modifier((int) ((float) -changingStat.baseValue / 100 * dependentStat.GetActualValue() / 100 * playerMultiplier));
                            break;
                    }
                    break;
            }
            changingStat.AddModifier(mod);
        }
        else
            changingStat.RemoveModifer(mod);



        /*if (apply)
        {
            switch (type)
            {
                case AdjustType.Point:
                    mod = new Modifier(statValue);
                    break;
                case AdjustType.Percentage:
                    mod = new Modifier((int)((float)stat.baseValue/100*statValue));
                    break;
            }
            stat.AddModifier(mod);
        }
        else
            stat.RemoveModifer(mod);*/
    }
}
public class Buff 
{
    #region Variables

    public delegate void PeriodicAction(Target target);
    public BuffType buff_Type { get; private set; }
    public GameObject buff_slot { get; private set; }
    public StatInfo[] statStats;
    public TimeValue baseDuration;
    public TimeValue periodicDuration;
    public Target target;
    public PeriodicAction periodicAction;

    public string skill_Name;

    #endregion
    public Buff(string skill_Name,params StatInfo[] statStats)
    {
        this.skill_Name = skill_Name;
        periodicDuration = new TimeValue(0);
        this.statStats = statStats;
        buff_Type = BuffType.Toggled;
    }
    public Buff(string skill_Name,float periodicDuration,  PeriodicAction periodicAction, params StatInfo[] statStats)
    {
        this.skill_Name = skill_Name;
        this.periodicDuration = new TimeValue(periodicDuration);
        this.statStats = statStats;
        this.periodicAction = periodicAction;
        buff_Type = BuffType.Periodic;
    }

    public Buff(Buff buff)
    {
        skill_Name = buff.skill_Name;
        if(buff.buff_Type == BuffType.Periodic)
        periodicDuration = new TimeValue(buff.periodicDuration);

        periodicAction = buff.periodicAction;
        buff_Type = buff.buff_Type;
        statStats = buff.statStats;
    }
    public void ToggleBuff(bool activate, Target target)
    {
        for (int i = 0; i < statStats.Length; i++)
        {
            statStats[i].ChangeStat(activate,target);
        }
    }

    public void TogglePeriodicAction(Target target)
    {
        periodicAction(target);
    }

    public void SetDuration(float duration)
    {
        baseDuration = new TimeValue(duration);
    }
    public void ResetPeriodicDuration()
    {
        periodicDuration.Reset();
    }
    public void SetSlot(GameObject buff_slot)
    {
        this.buff_slot = buff_slot;
    }

    public void SetTarget(Target target)
    {
        this.target = target;
    }
}
public class Spell
{
    public string spell_name { get; private set; }
    public TimeValue cooldown { get; private set; }
    public float chanelingTime { get; private set; }
    public SpellType type { get; private set; }
    public int symbolCount { get; private set; }
    public int enemyManaCost { get; set; }
    public int playerManaCost { get; set; }
    public delegate void Effect(User user);
    private Effect effect;

    public Spell(string spell_name, Effect effect, float cooldown, int enemyManaCost, int playerManaCost, SpellType type, float chanelingTime, int symbolCount)
    {
        this.spell_name = spell_name;
        this.effect = effect;
        this.cooldown = new TimeValue(cooldown);
        this.enemyManaCost = enemyManaCost;
        this.playerManaCost = playerManaCost;
        this.type = type;
        this.symbolCount = symbolCount;
        this.chanelingTime = chanelingTime;
    }

    public Spell(Spell spell)
    {
        spell_name = spell.spell_name;
        effect = spell.effect;
        cooldown = new TimeValue(spell.cooldown);
        enemyManaCost = spell.enemyManaCost;
        playerManaCost = spell.playerManaCost;
        type = spell.type;
        symbolCount = spell.symbolCount;
        chanelingTime = spell.chanelingTime;
    }
    public void Cast(User user)
    {
        effect(user);
    }
}
public class SpellSteal
{
    public RangeValue charges { get; private set; }
    public TimeValue cooldown { get; private set; }
    public Spell spell { get;private set; }
    public GameObject spell_slot { get; private set; }
    public GameObject spell_steal_slot { get; private set; }
    public bool canCastSpell { get; private set; }
    public bool can_steal_spell { get; private set; }
    public Coroutine spell_coroutine;
    public SpellSteal( float cooldown, int charges)
    {
        this.cooldown = new TimeValue(cooldown);
        this.charges = new RangeValue(charges);
        canCastSpell = true;
        can_steal_spell = true;
        spell_slot = GameObject.Find("Spell_Slot");       
        spell_steal_slot = GameObject.Find("Spell_Steal_Slot");
    }
    public void Steal_Spell(Spell spell, MonoBehaviour mono)
    { 
        can_steal_spell = false;
        this.spell = spell;
        spell_steal_slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        charges.Restore();
        GameObject.Find("Spell_Charges").GetComponent<TextMeshProUGUI>().text = "<size=45>charges</size>\n" + charges.actualValue;
        spell_slot.transform.GetChild(0).GetComponent<Image>().enabled = true;
        spell_slot.transform.GetChild(0).GetComponent<Image>().sprite = Find_Sprite(spell.spell_name);       
        spell.cooldown.value = 0;
        spell_slot.transform.getChild("Panel").GetComponent<Image>().fillAmount = spell.cooldown.value / spell.cooldown.maxValue;
        SaveSystem.Instance.SetSpell(spell);
        mono.StartCoroutine(Activate_Spell_Steal());
    }
    public void Refresh(MonoBehaviour mono)
    {
        if(spell!= null)
        {
            spell.cooldown.value = 0;
            spell_slot.transform.getChild("Panel").GetComponent<Image>().fillAmount = 0;

            spell_slot.transform.GetChild(0).GetComponent<Image>().enabled = false;

            canCastSpell = true;
            spell = null;
            SaveSystem.Instance.SetSpell(null);
            if(spell_coroutine != null)
            mono.StopCoroutine(spell_coroutine);

            GameObject.Find("Spell_Charges").GetComponent<TextMeshProUGUI>().text = "<size=45>charges</size>\n" + 0;
        }
        canCastSpell = true;
        cooldown.value = 0;
        spell_steal_slot.transform.getChild("Panel").GetComponent<Image>().fillAmount = 0;

    }

    public void CastSpell(MonoBehaviour mono)
    { 
        if(spell == null)
            return;

        int manaCost = spell.playerManaCost + SaveSystem.GetChar().GetStat("Mana Cost").baseValue;

        if (manaCost < 0)
            manaCost = 0;

        if(manaCost > SaveSystem.GetChar().GetStat("Mana").baseValue)
            return;

        if (canCastSpell && charges.actualValue > 0)
        {
            Battle_Manager.Instance.Create_Message(new Info(HeroInfo.Instance.AdjustMana(-manaCost, AdjustType.Point), mono.HexToColor("00cbc9"), 50, InfoType.Mana), 0.5f, 3f);
            canCastSpell = false;
            charges.actualValue--;
            spell_coroutine = mono.StartCoroutine(Activate_Spell());
            spell.Cast(User.Player);           
            GameObject.Find("Spell_Charges").GetComponent<TextMeshProUGUI>().text = "<size=45>charges</size>\n" + charges.actualValue;            
            if (charges.actualValue == 0)
            {
                //return slot images
                spell_slot.transform.GetChild(0).GetComponent<Image>().enabled = false;
                spell_slot.transform.getChild("Panel").GetComponent<Image>().fillAmount = 0;
                canCastSpell = true;
                spell = null;
                SaveSystem.Instance.SetSpell(null);
                mono.StopCoroutine(spell_coroutine);
            }            
        }


    }
    public IEnumerator Activate_Spell_Steal()
    {
        while (cooldown.value > 0)
        {
            cooldown.value -= Time.deltaTime;
            spell_steal_slot.transform.getChild("Panel").GetComponent<Image>().fillAmount = cooldown.value / cooldown.maxValue;
            yield return null;
        }
        can_steal_spell = true;
        cooldown.value = cooldown.maxValue;
    }
    public IEnumerator Activate_Spell()
    {
        spell.cooldown.value = spell.cooldown.maxValue;
        while (spell.cooldown.value > 0)
        {
            spell.cooldown.value -= Time.deltaTime;
            spell_slot.transform.getChild("Panel").GetComponent<Image>().fillAmount = spell.cooldown.value / spell.cooldown.maxValue;
            yield return null;
        }
        canCastSpell = true;       
    }
    private Sprite Find_Sprite(string name)
    {
        return SaveSystem.GetSprite(name);
    }
    public void Toggle_Sprite(bool active)
    {
        if (active)
        {
            spell_steal_slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
        }
        else
        {
            spell_steal_slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        }

    }

    public void SetSpellSteal(int charges)
    {
        this.charges.baseValue = charges;
    }
}
public class TimeValue
{
    public float value { get; set; }
    public float maxValue { get; set; }

    public TimeValue(float value)
    {
        this.value = value;
        maxValue = value;
    }
    public TimeValue(TimeValue tv)
    {
        value = tv.value;
        maxValue = tv.maxValue;
    }
    public void Reset()
    {
        value = maxValue;
    }

}
public class RangeValue
{
    public int baseValue { get;  set; }
    public int actualValue { get; set; }

    public RangeValue(int baseValue)
    {
        this.baseValue = baseValue;
        actualValue = baseValue;
    }
    public void Restore()
    {
        actualValue = baseValue;
    }
}
public class MinMax
{
    public int min { get; set; }
    public int max { get; set; }

    public MinMax(int min, int max)
    {
        this.min = min;
        this.max = max;
    }

    public MinMax()
    {
    }
}
#endregion
#endregion