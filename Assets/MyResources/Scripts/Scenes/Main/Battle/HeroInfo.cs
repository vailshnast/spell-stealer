﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using TMPro;

public class HeroInfo : MonoBehaviour
{
    public static HeroInfo Instance { get; private set; }

    private MonsterInfo monsterInfo;


    //If we dont check this in OnEnable, will cause different bugs,
    //because, OnEnable is called before Start().
    private bool initialised;

    private IEnumerator animate_damage;
    private IEnumerator animate_mana;
    // Use this for initialization
    void Start()
    {
        Initialise();
    }
    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;
    }
    void Initialise()
    {
        monsterInfo = GetComponent<MonsterInfo>();
        monsterInfo.SetMonsterParameters(Database_Monsters.Instance.RandomMonster());
        SaveSystem.GetChar().GetStat("Health").Restore();
        SaveSystem.GetChar().GetStat("Mana").Restore();
        SetUIParameters();

        initialised = true;
    }
    void OnEnable()
    {
        if (initialised)
        {
            monsterInfo.SetMonsterParameters(Database_Monsters.Instance.RandomMonster());

            SaveSystem.GetChar().GetStat("Health").Restore();
            SaveSystem.GetChar().GetStat("Mana").Restore();

            SetUIParameters();
            GameObject.Find("Damage_Filler").GetComponent<Image>().fillAmount = GameObject.Find("Player_Health_Filler")
                .GetComponent<Image>()
                .fillAmount;
            GameObject.Find("Damage_Mana_Filler").GetComponent<Image>().fillAmount = GameObject.Find("Player_Mana_Filler")
                .GetComponent<Image>()
                .fillAmount;
        }
    }

    public string AdjustMana(int mana_amount, AdjustType type)
    {

        if (type == AdjustType.Percentage)
            mana_amount = (int)Math.Ceiling((float)GetHero().GetStat("Mana").maxValue / 100 * mana_amount);

        int adjusted_mana = GetHero().GetStat("Mana").ChangeCurrent(mana_amount);

        string manaText = "";

        if (adjusted_mana > 0)
            manaText = "+" + adjusted_mana;
        else if (adjusted_mana < 0)
            manaText = adjusted_mana.ToString();
            

        if (animate_mana != null)
            StopCoroutine(animate_mana);

        animate_mana = AnimateMana(1);
        StartCoroutine(animate_mana);

        SetUIParameters();

        return manaText;
    }
    public void Death()
    {
        if (SaveSystem.GetChar().GetStat("Health").baseValue > 0)
            return;

        if (SaveSystem.Instance.blessedByResalia)
        {
            Battle_Manager.Instance.Create_Message(new Info(HealHero(50), this.HexToColor("00b600"), 150, InfoType.Health), 0.5f, 3f);
            SaveSystem.Instance.blessedByResalia = false;
            Destroy(GameObject.Find("ResaliaBuff"));
            return;
        }


        Battle_Manager.Instance.ChangeState(BattleState.Defeat);
    }
    public CharacterStats GetHero()
    {
        return SaveSystem.GetChar();
    }
    public string DealDamageToHero(int damage, Stat stat, DamageType type)
    {
        if (stat != null)
            damage = (int)(damage * ((float)stat.baseValue / 100));

        //Debug.Log("Before dmg " + damage);
        if (type != DamageType.Pierce)
            damage -= (int)((float)damage / 100 * GetHero().GetStat("Defence").GetActualValue());

        //Debug.Log("After dmg " + damage);

        SaveSystem.GetChar().GetStat("Health").ChangeCurrent(-damage);

        if (animate_damage != null)
            StopCoroutine(animate_damage);
        animate_damage = AnimateDamage(1);
        StartCoroutine(animate_damage);


        Death();

        return "-"+damage;
    }
    public string HealHero(int healPercentage)
    {

        int healAmount = (int)Math.Ceiling((float)GetHero().GetStat("Health").maxValue / 100 * healPercentage);
        int healedAmount = GetHero().GetStat("Health").ChangeCurrent(healAmount);
        string healedText = "+" + healedAmount;

        if (healedAmount <= 0)
            healedText = "";

        if (animate_damage != null)
            StopCoroutine(animate_damage);
        animate_damage = AnimateDamage(1);
        StartCoroutine(animate_damage);


        SetUIParameters();

        return healedText;
    }
    private IEnumerator AnimateDamage(float TotalTime)
    {     
        Image damage_Filler = GameObject.Find("Damage_Filler").GetComponent<Image>();
        Image health_Filler = GameObject.Find("Player_Health_Filler").GetComponent<Image>();       

        SetUIParameters();
        if(damage_Filler.fillAmount > health_Filler.fillAmount)
        {
            float ElapsedTime = 0.0f;
            float currentAmmount = damage_Filler.fillAmount;

            while (ElapsedTime < TotalTime)
            {
                ElapsedTime += Time.deltaTime;
                damage_Filler.fillAmount = Mathf.Lerp(currentAmmount, health_Filler.fillAmount, (ElapsedTime / TotalTime));
                yield return null;
            }
        }
        else
        {
            damage_Filler.fillAmount = health_Filler.fillAmount;
        }
    }
    private IEnumerator AnimateMana(float TotalTime)
    {
        Image manaUse_filer = GameObject.Find("Damage_Mana_Filler").GetComponent<Image>();
        Image mana_Filler = GameObject.Find("Player_Mana_Filler").GetComponent<Image>();


        SetUIParameters();
        if (manaUse_filer.fillAmount > mana_Filler.fillAmount)
        {
            float ElapsedTime = 0.0f;
            float currentAmmount = manaUse_filer.fillAmount;

            while (ElapsedTime < TotalTime)
            {
                ElapsedTime += Time.deltaTime;
                manaUse_filer.fillAmount = Mathf.Lerp(currentAmmount, mana_Filler.fillAmount, (ElapsedTime / TotalTime));
                yield return null;
            }
        }
        else
        {
            manaUse_filer.fillAmount = mana_Filler.fillAmount;
        }

    }
    public void SetUIParameters()
    {
        GameObject.Find("EnemyName_Text").GetComponent<TextMeshProUGUI>().text = monsterInfo.GetMonster().MonsterName;

        GameObject.Find("Health_Amount").GetComponent<TextMeshProUGUI>().text = SaveSystem.GetChar().GetStat("Health").baseValue.ToString();
        
        GameObject.Find("Mana_Amount").GetComponent<TextMeshProUGUI>().text = SaveSystem.GetChar().GetStat("Mana").baseValue.ToString();

        GameObject.Find("Player_Health_Filler").GetComponent<Image>().fillAmount = (float)SaveSystem.GetChar().GetStat("Health").baseValue / SaveSystem.GetChar().GetStat("Health").maxValue;

        GameObject.Find("Player_Mana_Filler").GetComponent<Image>().fillAmount = (float)SaveSystem.GetChar().GetStat("Mana").baseValue / SaveSystem.GetChar().GetStat("Mana").maxValue;        
    }
}
