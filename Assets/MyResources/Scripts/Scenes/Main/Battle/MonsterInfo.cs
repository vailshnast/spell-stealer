﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class MonsterInfo : ShakeEffect {
    public static MonsterInfo Instance { get; private set; }
    private Monster monster;
    private Image monsterImage;

    private RectTransform healthFiller;
    private RectTransform manaFiller;


    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;
        Initialise();
    }
	// Update is called once per frame
   
    void Initialise()
    {       
        monsterImage = GameObject.Find("Enemy_Sprite").GetComponent<Image>();
        healthFiller = GameObject.Find("Enemy_Health_Filler").GetComponent<RectTransform>();
        manaFiller = GameObject.Find("Enemy_Mana_Filler").GetComponent<RectTransform>();
        SetShakingObject(monsterImage.GetComponent<RectTransform>());
    }

    public Monster GetMonster()
    {
        return monster;
    }
    public int DealDamageToMonster(int damage,Stat stat,DamageType type)
    {
        if (stat != null)
            damage = (int) (damage *  ((float)stat.baseValue / 100)); 

        //Debug.Log("Before dmg " + damage);
        if (type != DamageType.Pierce)
            damage -= (int)((float)damage / 100 * monster.GetStat("Defence").GetActualValue());
        //Debug.Log("After dmg " + damage);

        monster.GetStat("Health").ChangeCurrent(-damage);

        healthFiller.sizeDelta = new Vector2((float) monster.GetStat("Health").baseValue / monster.GetStat("Health").maxValue * 700, 83);

        if (monster.GetStat("Health").baseValue <= 0)
            Battle_Manager.Instance.KillMonster();

        return damage;
    }

    public int HealMonster(int amount, AdjustType type)
    {
        if(type == AdjustType.Percentage)
        amount = (int)((float)monster.GetStat("Health").maxValue / 100 * amount);

        int healed_amount = GetMonster().GetStat("Health").ChangeCurrent(amount);


        healthFiller.sizeDelta = new Vector2((float)monster.GetStat("Health").baseValue / monster.GetStat("Health").maxValue * 700, 83);
        return healed_amount;
    }
    public int AdjustMana(int mana_amount,AdjustType type)
    {
        if(type == AdjustType.Percentage)
        mana_amount = (int)((float)monster.GetStat("Mana").maxValue / 100 * mana_amount);
        int adjusted_mana = GetMonster().GetStat("Mana").ChangeCurrent(mana_amount);

        manaFiller.sizeDelta = new Vector2((float)monster.GetStat("Mana").baseValue / monster.GetStat("Mana").maxValue * 644, 26);

        return adjusted_mana;
    }
    public void SetMonsterParameters(Monster monster)
    {
        this.monster = new Monster(monster);
        healthFiller.sizeDelta = new Vector2((float)monster.GetStat("Health").baseValue / monster.GetStat("Health").maxValue * 700, 83);
        manaFiller.sizeDelta = new Vector2((float)monster.GetStat("Mana").baseValue / monster.GetStat("Mana").maxValue * 644, 26);
    }
    public void ShakeEnemySprite(float time , float amount)
    {
        ShakeObject(amount,time);  
    }
    private void ChangeSprite(Sprite sprite)
    {
        monsterImage.sprite = sprite;
        monsterImage.SetNativeSize();
    }
    public void SetState(MonsterSprite sprite)
    {
        switch (sprite)
        {
            case MonsterSprite.Idle:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName +" Idle"));
                break;
            case MonsterSprite.Swing:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " Swing"));
                break;
            case MonsterSprite.SwingCrit:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " SwingCrit"));
                break;
            case MonsterSprite.SwingAnimation:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " SwingAnimation"));
                break;
            case MonsterSprite.Cast:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " Cast"));
                break;
            case MonsterSprite.CastAnimation:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " CastAnimation"));
                break;           
            case MonsterSprite.Death:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " Death"));
                break;
            case MonsterSprite.Victory:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " Swing"));
                break;
            case MonsterSprite.Agony:
                ChangeSprite(SaveSystem.GetSprite(monster.MonsterName + " Agony"));
                break;
        }
    }

    public void Agony()
    {
        monster.GetStat("Health").ChangeCurrent(-monster.GetStat("Health").baseValue);

        healthFiller.sizeDelta = new Vector2((float)monster.GetStat("Health").baseValue / monster.GetStat("Health").maxValue * 700, 83);
    }
}
