﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Altar : MonoBehaviour
{
    #region Refereces

    private AltarType currentType;
    private GameObject tooltip;
    private TextMeshProUGUI title;
    private Image altarImage;
    private GameObject glowImage;
    private bool initialised;

    #endregion

    #region Warod
    private Dictionary<Blessing,int> blessings = new Dictionary<Blessing, int>();
    

    #endregion

    private bool waiting;

    // Use this for initialization
    void Start()
    {
        altarImage = transform.FindDeepChild("Altar_Image").GetComponent<Image>();
        glowImage = transform.FindDeepChild("Altar_Glow").gameObject;
        title = transform.FindDeepChild("Title_Bar").GetChild(0).GetComponent<TextMeshProUGUI>();
        tooltip = transform.FindDeepChild("Tooltip_Altar").gameObject;

        CharacterStats ch = SaveSystem.GetChar();

        ChooseAltar();

        blessings.Add(new Blessing(ch.GetStat("Health"),50, "maximum Health"), 1);
        blessings.Add(new Blessing(ch.GetStat("Mana"), 25, "maximum Mana"), 1);
        blessings.Add(new Blessing(ch.GetStat("Defence"), 20, null), 1);
        blessings.Add(new Blessing(ch.GetStat("Damage"), 5, null), 1);
        blessings.Add(new Blessing(ch.GetStat("Power"), 100, null), 1);
        blessings.Add(new Blessing(ch.GetStat("Magic"), 125, null), 1);
        blessings.Add(new Blessing(ch.GetStat("Crit"), -3, "hit less for Critical Strike"), 1);

        initialised = true;
    }

    void OnEnable()
    {
        if (initialised)
        {
            ChooseAltar();
            tooltip.SetActive(false);
            glowImage.SetActive(false);
        }
    }

    private void ChooseAltar()
    {
        if (!SaveSystem.Instance.blessedByResalia)
            currentType = (AltarType) Random.Range(0, 3);
        else
            currentType = (AltarType) Random.Range(0, 2);


        string spritePath = "Sprites/CorePrototype/Events";

        switch (currentType)
        {
            case AltarType.Warod:
                title.text = "Warod";
                altarImage.sprite = SaveSystem.GetSprite("Warod");
                break;
            case AltarType.Enur:
                title.text = "Enur";
                altarImage.sprite = SaveSystem.GetSprite("Enur");
                break;
            case AltarType.Resalia:
                title.text = "Resalia";
                altarImage.sprite = SaveSystem.GetSprite("Resalia");
                break;
        }
        altarImage.SetNativeSize();
    }

    public void ActivateAltar()
    {
        tooltip.SetActive(true);
        tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "Make offering to " + currentType + "?";
        tooltip.transform.FindDeepChild("Button_Panel").gameObject.SetActive(true);
        tooltip.transform.FindDeepChild("Price_Panel").gameObject.SetActive(true);
        Main_Manager.Instance.ToggleButtons(false);
        switch (currentType)
        {
            case AltarType.Warod:
                tooltip.transform.FindDeepChild("Amount").GetComponent<TextMeshProUGUI>().text = "1000";
                break;
            case AltarType.Enur:
                tooltip.transform.FindDeepChild("Amount").GetComponent<TextMeshProUGUI>().text = "1000";
                break;
            case AltarType.Resalia:
                tooltip.transform.FindDeepChild("Amount").GetComponent<TextMeshProUGUI>().text = "1000";
                break;
        }
    }

    public void CloseTooltip()
    {
        tooltip.SetActive(false);
        Main_Manager.Instance.ToggleButtons(true);
    }
    private IEnumerator UseAltarCoroutine()
    {
        CharacterStats character = SaveSystem.GetChar();

        Stat gold = SaveSystem.GetChar().GetStat("Gold");
        tooltip.transform.FindDeepChild("Button_Panel").gameObject.SetActive(false);
        tooltip.transform.FindDeepChild("Price_Panel").gameObject.SetActive(false);
        altarImage.raycastTarget = false;
        waiting = true;
        Main_Manager.Instance.ToggleButtons(false);
        switch (currentType)
        {
            case AltarType.Warod:
                if (gold.baseValue >= 1000)
                {

                    glowImage.SetActive(true);
                    gold.ChangeCurrent(-1000);

                    Blessing blessing = WeightedRandomizer.From(blessings).Random();

                    blessing.Bless();

                    tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "<color=#FFAE00FF>" + currentType + " accepts your offering</color>\n" +
                                                                                                           blessing.GetBlessingDescription();

                }
                else
                    tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "Not enough gold for offering";
                break;
            case AltarType.Enur:

                if (gold.baseValue >= 1000)
                {


                    if (character.GetStat("Runeslots").baseValue >= 5)
                    {
                        SaveSystem.Instance.AddItemInfo(3, 0);
                        tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "<color=#FFAE00FF>" + currentType + " accepts your offering</color>\n" +
                                                                                                               "You received a rune";
                    }
                    else
                    {
                        tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "<color=#FFAE00FF>" + currentType + " accepts your offering</color>\n" +
                                                                                                               "Your runeslot was unlocked";
                        character.GetStat("Runeslots").ChangeCurrent(1);
                    }



                    glowImage.SetActive(true);
                    gold.ChangeCurrent(-1000);
                }
                else
                    tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "Not enough gold for offering";

                break;
            case AltarType.Resalia:
                if (gold.baseValue >= 1000)
                {
                    SaveSystem.Instance.blessedByResalia = true;
                    tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "<color=#FFAE00FF>" + currentType + " accepts your offering</color>\n" +
                                                                                                           "Spirit of Life now guards you!";
                    glowImage.SetActive(true);
                    gold.ChangeCurrent(-1000);
                }
                else
                    tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "Not enough gold for offering";
                break;
        }

        yield return new WaitForSeconds(2);
        altarImage.raycastTarget = true;
        waiting = false;
        Main_Manager.Instance.Skip();
    }
    public void UseAltar()
    {
        if(waiting) return;

        StartCoroutine(UseAltarCoroutine());
    }


}

public class Blessing
{
    public Stat stat { get; set; }
    public int value { get; set; }
    public string description { get; set; }

    public Blessing(Stat stat, int value, string description)
    {
        this.stat = stat;
        this.value = value;
        this.description = description;
    }

    public void Bless()
    {
        stat.AdjustMax(value);
        stat.ChangeCurrent(value);        
    }

    public string GetBlessingDescription()
    {
        string amount = value > 0 ? "+ " + value : value.ToString();

        return description != null ? amount + " " + description : amount + " " + stat.name;
    }
}