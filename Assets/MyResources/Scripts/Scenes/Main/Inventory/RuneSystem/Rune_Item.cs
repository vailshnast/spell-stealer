﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Rune_Item : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        InventoryTooltip.Instance.Activate(transform.parent.GetComponent<Rune_Slot>());
    }
}
