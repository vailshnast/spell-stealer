﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Rune_Slot : MonoBehaviour
{
    public bool runeSlotIsEmpty;
    private Item _runeItem;

    void Start()
    {
        runeSlotIsEmpty = true;
    }

    public void InsertRune(Item item)
    {
        _runeItem = item;

        transform.GetChild(0).GetComponent<Image>().enabled = true;
        transform.GetChild(0).GetComponent<Image>().sprite = SaveSystem.GetSprite("I" + item.spriteName);

        runeSlotIsEmpty = false;
    }
    public void DestroyRune()
    {
        transform.GetChild(0).GetComponent<Image>().enabled = false;
        _runeItem.Use(false,PropertyType.Passive);
        SaveSystem.Instance.runes.Remove(_runeItem);
        runeSlotIsEmpty = true;
    }

    public Item GetRuneItem()
    {
        return _runeItem;
    }

}
