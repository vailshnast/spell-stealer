﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class Rune_System : MonoBehaviour {

    public static Rune_System Instance { get; private set; }
    private Rune_Slot[] allRuneSlots;
    private List<Rune_Slot> runeSlots = new List<Rune_Slot>();
    private InventoryTooltip tooltip;


    //If we dont check this in OnEnable, will cause different bugs,
    //because, OnEnable is called before Start().
    private bool initialised = false;

    // Use this for initialization
    void Start () {
        allRuneSlots = FindObjectsOfType<Rune_Slot>();
        tooltip = FindObjectOfType<InventoryTooltip>();
        SetRunes();
        initialised = true;        
	}
    void OnEnable()
    {
        if(initialised)
        SetRunes();
    }
    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;
    }
    public void SetRunes()
    {
        SelectRuneLayout(SaveSystem.GetChar().GetStat("Runeslots").baseValue);
    }
    
    public void SelectRuneLayout(int runeSlots)
    {
        //Sort

        allRuneSlots = allRuneSlots.OrderBy(a => a.transform.GetSiblingIndex()).ToArray();


        for (int i = 0; i < allRuneSlots.Length; i++)
        {
            SetRune(allRuneSlots[i], false);
        }

        for (int i = 0; i < runeSlots; i++)
        {
            SetRune(allRuneSlots[i], true);
            this.runeSlots.Add(allRuneSlots[i]);
        }
    }

    private void SetRune(Rune_Slot slot,bool active)
    {
        Image img = slot.GetComponent<Image>();

        img.sprite = active ? SaveSystem.GetSprite("RuneSlot") : SaveSystem.GetSprite("RuneslotBlocked");
    }

    public bool SlotsAreFull()
    {
        return runeSlots.All(t => !t.runeSlotIsEmpty);
    }

    public List<Rune_Slot> GetRuneSlots()
    {
        return runeSlots;
    }

    public void InsertRune(Item rune)
    {
        if(SlotsAreFull()) return;


        if (rune.ContainsProperties())
            ProccessRune(rune);
        else
        {
            SaveSystem.Instance.IdentifyRune(rune);
            ProccessRune(rune);
        }              
    }

    private void ProccessRune(Item rune)
    {
        rune.Use(true, PropertyType.Passive);
        SaveSystem.Instance.runes.Add(rune);

        for (int i = 0; i < runeSlots.Count; i++)
        {
            if (runeSlots[i].runeSlotIsEmpty)
            {
                runeSlots[i].InsertRune(rune);                
                InventoryTooltip.Instance.DestroyItem();
                tooltip.Activate(runeSlots[i]);
                Inventory.Instance.LoadInfo();
                return;
            }
        }
    }
}

