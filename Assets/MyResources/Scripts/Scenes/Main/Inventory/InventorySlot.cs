﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class InventorySlot : MonoBehaviour, IDropHandler, IPointerClickHandler
{

    private Inventory inventory;
    public int item_id;

    void Start ()
    {
        inventory = FindObjectOfType<Inventory>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        InventoryItem buffered_item = eventData.pointerDrag.GetComponent<InventoryItem>();
        if (inventory.inventory_item_list[item_id].id == 0)
        {
            inventory.inventory_item_list[buffered_item.slot] = new Item();
            inventory.inventory_item_list[item_id] = buffered_item.itemInfo;
            buffered_item.slot = item_id;
        }
        else
        {
            if (transform.childCount > 0)
            {
                Transform duplicated_item = transform.GetChild(0);

                duplicated_item.GetComponent<InventoryItem>().slot = buffered_item.slot;
                duplicated_item.transform.SetParent(inventory.inventory_slot_list[buffered_item.slot].transform, false);
                duplicated_item.position = inventory.inventory_slot_list[buffered_item.slot].transform.position;

                buffered_item.slot = item_id;
                buffered_item.transform.SetParent(transform, false);
                buffered_item.transform.position = transform.position;

                inventory.inventory_item_list[buffered_item.slot] = duplicated_item.GetComponent<InventoryItem>().itemInfo;
                inventory.inventory_item_list[item_id] = buffered_item.itemInfo;
            }
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (transform.childCount == 0)
        {
            for (int i = 0; i < inventory.inventory_slot_list.Count; i++)
            {
                if (inventory.inventory_slot_list[i] != transform.parent)
                {
                    inventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
                }
            }
            FindObjectOfType<InventoryTooltip>().scrollIsUsed = false;
            FindObjectOfType<InventoryTooltip>().Deactivate();
        }

    }

}
