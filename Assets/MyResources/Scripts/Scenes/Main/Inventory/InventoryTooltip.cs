﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class InventoryTooltip : MonoBehaviour
{

    public static InventoryTooltip Instance { get; private set; }


    private Inventory inventory;
    private GameObject tooltip;
    private GameObject panel_tooltip;

    private InventoryItem currentItem;
    private GameObject text_item;

    private Rune_Slot currentRuneSlot;

    private TextMeshProUGUI buttonText;

    public bool scrollIsUsed { get; set; }

    private GameObject potion_panel;
    private GameObject destroyButton;

    private ButtonState buttonState;

    void Start()
    {
        Init();
    }

    private void Init()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        inventory = FindObjectOfType<Inventory>();

        tooltip = transform.FindDeepChild("Tooltip_Explore").gameObject;

        panel_tooltip = transform.FindDeepChild("Panel_tooltip").gameObject;

        destroyButton = transform.FindDeepChild("Destroy_Button").gameObject;

        text_item = transform.FindDeepChild("Tooltip_Text").gameObject;

        potion_panel = transform.FindDeepChild("Potion_Panel").gameObject;

        buttonText = GameObject.Find("Button").transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        buttonText.transform.parent.gameObject.SetActive(false);

        tooltip.SetActive(false);
        panel_tooltip.SetActive(false);
        potion_panel.SetActive(false);
    }

    private void Information_Set(InventoryItem inventoryItem)
    {


        if (inventoryItem.itemInfo.itemType != "Weapon")
            text_item.GetComponent<TextMeshProUGUI>().text = "<color=orange><b><size=60>" + inventoryItem.itemInfo.itemName + "</color></b></size>\n" + inventoryItem.itemInfo.description;
        else
            text_item.GetComponent<TextMeshProUGUI>().text = "<color=orange><b><size=60>" + inventoryItem.itemInfo.itemName + "</color></b></size>\n" + GetDescription(inventoryItem.itemInfo);

        if (Main_Manager.Instance.currentState != StageState.SkillTree && Main_Manager.Instance.currentState != StageState.Map)
            Panel_Processor(inventoryItem);
    }

    private void Panel_Processor(InventoryItem inventoryItem)
    {
        buttonText.transform.parent.gameObject.SetActive(true);
        potion_panel.SetActive(false);

        switch (inventoryItem.itemInfo.itemType)
        {
            case "Scroll":
                buttonText.text = "Use";
                break;
            case "Weapon":
                buttonText.text = "Equip";
                break;
            case "Treasure":
                buttonText.text = "Use";
                break;
            case "Book":
                buttonText.text = "Use";
                break;
            case "Rune":
                buttonText.text = "Insert";
                break;
            default:
                buttonText.text = "Close";
                break;
        }
    }

    private void ButtonUse()
    {
        switch (buttonState)
        {
            case ButtonState.Weapon:
                Deactivate();
                break;
            case ButtonState.Item:
                switch (currentItem.itemInfo.itemType)
                {
                    case "Scroll":
                        scrollIsUsed = true;
                        for (int i = 0; i < inventory.inventory_slot_list.Count; i++)
                        {
                            if (inventory.inventory_slot_list[i].transform.childCount > 0)
                            {
                                Item item = inventory.inventory_slot_list[i].transform.GetChild(0).GetComponent<InventoryItem>().itemInfo;

                                //If checking slot contains unidentified rune
                                if (item.itemType == "Rune" && !item.ContainsProperties())
                                {
                                    inventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
                                }
                            }
                        }
                        Deactivate();
                        break;

                    case "Rune":
                        Rune_System.Instance.InsertRune(currentItem.itemInfo);
                        break;
                    case "Book":
                        if (SaveSystem.GetChar().GetStat("Runeslots").baseValue < 6)
                        {
                            SaveSystem.GetChar().GetStat("Runeslots").ChangeCurrent(1);
                            Rune_System.Instance.SetRunes();
                        }


                        DestroyItem();
                        Deactivate();
                        break;
                    case "Treasure":
                        SaveSystem.GetChar().GetStat("Gold").ChangeCurrent(+currentItem.itemInfo.price);
                        DestroyItem();
                        Debug.Log("Here");
                        Deactivate();
                        GameObject.Find("Panel_Coins").transform.Find("Text_Amount").GetComponent<TextMeshProUGUI>().text = SaveSystem.GetChar().GetStat("Gold").baseValue.ToString();
                        break;
                    case "Weapon":
                        transform.FindDeepChild("Weapon_Slot").GetChild(0).GetComponent<Image>().sprite = SaveSystem.GetSprite(currentItem.itemInfo.spriteName);

                        currentItem.itemInfo.Use(true, PropertyType.Passive);
                        Database_Items.GetItemByName(Database_Saves.Instance.GetHero().weaponName).Use(false, PropertyType.Passive);

                        string selectedWeaponName = currentItem.itemInfo.itemName;

                        currentItem.itemInfo = Database_Items.GetItemByName(Database_Saves.Instance.GetHero().weaponName);
                        currentItem.GetComponent<Image>().sprite = SaveSystem.GetSprite(currentItem.itemInfo.spriteName);

                        inventory.LoadInfo();

                        Database_Saves.Instance.GetHero().weaponName = selectedWeaponName;
                        Database_Saves.Instance.Save();

                        Deactivate();
                        break;
                    default:
                        Deactivate();
                        break;

                }
                break;
            case ButtonState.Rune:
                Deactivate();
                break;
        }              
    }

    public void DestroyTooltipItem()
    {
        switch (buttonState)
        {
            case ButtonState.Rune:
                if (!currentItem)
                {
                    currentRuneSlot.DestroyRune();
                    currentRuneSlot = null;
                    Deactivate();
                    Inventory.Instance.LoadInfo();
                }
                break;
            default:
                DeleteTrash();
                Deactivate();
                break;
        }
    }
    public void ButtonPressed()
    {
        ButtonUse();
    }

    public void Activate(InventoryItem inventoryItem)
    {
        //Link for a deactivation
        buttonState = ButtonState.Item;
        currentItem = inventoryItem;
        currentRuneSlot = null;
        tooltip.SetActive(true);
        Information_Set(inventoryItem);
        panel_tooltip.SetActive(true);

        if (Main_Manager.Instance.currentState != StageState.Map)
            destroyButton.SetActive(true);
    }

    public void Activate(Rune_Slot slot)
    {
        buttonState = ButtonState.Rune;
        tooltip.SetActive(true);
        potion_panel.SetActive(false);

        currentRuneSlot = slot;
        currentItem = null;
        text_item.GetComponent<TextMeshProUGUI>().text = "<color=orange><b><size=60>" + slot.GetRuneItem().itemName + "</color></b></size>\n" + slot.GetRuneItem().description;

        buttonText.text = "Close";

        panel_tooltip.SetActive(true);

        if (Main_Manager.Instance.currentState != StageState.Map)
            destroyButton.SetActive(true);
    }

    public void ShowWeaponTooltip()
    {
        buttonState = ButtonState.Weapon;
        currentRuneSlot = null;
        Item weapon = Database_Items.GetItemByName(Database_Saves.Instance.GetHero().weaponName);
        text_item.GetComponent<TextMeshProUGUI>().text = "<color=orange><b><size=60>" + weapon.itemName + "</color></b></size>\n" + GetDescription(weapon);
        buttonText.text = "Close";
        tooltip.SetActive(true);
        panel_tooltip.SetActive(true);

        destroyButton.SetActive(false);
    }

    public string GetDescription(Item item)
    {
        string description = "";
        for (int i = 0; i < item.properties.Count; i++)
        {
            string sign = item.properties[i].value > 0 ? "+" : "";
            string percents = item.properties[i].drawPercents ? "%" : "";

            switch (item.properties[i].propertyType)
            {
                case PropertyType.Passive:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].name + "\n";
                    break;
                case PropertyType.Use:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].name + "\n";
                    break;
                case PropertyType.Crit:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].target + " " + item.properties[i].name + " After Crit\n";
                    break;
                case PropertyType.Block:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].target + " " + item.properties[i].name + " After Block\n";
                    break;
            }
        }
        return description;
    }

    public void SetDescription(string description)
    {
        text_item.GetComponent<TextMeshProUGUI>().text = "<color=orange>" + currentItem.name + "</color>\n" + description;
    }

    public InventoryItem getItem()
    {
        return currentItem;
    }

    public void Deactivate()
    {
        if (currentItem)
            currentItem.transform.parent.GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");

        tooltip.SetActive(false);
        panel_tooltip.SetActive(false);
    }

    public void DestroyItem()
    {
        Destroy(inventory.inventory_slot_list[currentItem.slot].transform.GetChild(0).gameObject);
        inventory.inventory_item_list[currentItem.slot] = new Item();
        Deactivate();
    }

    public void DeactivateAndDisableScroll(bool panelActive)
    {
        scrollIsUsed = false;
        for (int i = 0; i < inventory.inventory_slot_list.Count; i++)
        {
            inventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        }

        tooltip.SetActive(false);
        panel_tooltip.SetActive(panelActive);
    }

    public void SetRuneSlot(Rune_Slot slot)
    {
        currentRuneSlot = slot;
    }

    public void SetItem(InventoryItem inventoryItem)
    {
        currentItem = inventoryItem;
    }

    public void DeleteTrash()
    {
        Destroy(currentItem.gameObject);
        inventory.inventory_item_list[currentItem.slot] = new Item();
    }
}
