﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class Inventory : MonoBehaviour
{

    public static Inventory Instance { get; private set; }

    private Database_Items database_items;
    private GameObject inventory_panel_slot;
    private GameObject inventory_slot;
    
    [HideInInspector]
    public List<GameObject> inventory_slot_list = new List<GameObject>();
    private GameObject inventory_item;
    public List<Item> inventory_item_list = new List<Item>();

    //If we dont check this in OnEnable, will cause different bugs,
    //because, OnEnable is called before Start().
    private bool initialised = false;

    void Start () {
        Init();
    }

    private void Init()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        database_items = Database_Items.Instance;

        inventory_slot = Resources.Load("Prefabs/Slot_Explore", typeof(GameObject)) as GameObject;
        inventory_item = Resources.Load("Prefabs/Item_Explore", typeof(GameObject)) as GameObject;
        inventory_panel_slot = GameObject.Find("Slots_Panel");

        LoadWeapon();
        Initiate_Inventory();
        LoadInfo();

        //GameObject.Find("Stats_panel").SetActive(false);
        initialised = true;
    }
    void OnEnable()
    {
        if (!initialised)
            return;

        InventoryTooltip.Instance.Deactivate();
        LoadInfo();
        DeleteItems(0);
        AddSavedItems();
    }

    private void AddSavedItems()
    {       
        for (int i = 0; i < SaveSystem.Instance.charInventory.Length; i++)
        {
            if (SaveSystem.Instance.charInventory[i] != null && SaveSystem.Instance.charInventory[i].id != 0)
                Add_Item(SaveSystem.Instance.charInventory[i], i);
        }
    }

    private void DeleteItems(int index)
    {
        for (int i = index; i < inventory_slot_list.Count; i++)
        {
            if (inventory_slot_list[i].transform.childCount > 0)
                Destroy(inventory_slot_list[i].transform.GetChild(0).gameObject);
            inventory_item_list[i] = new Item();
        }

    }
    private void Initiate_Inventory()
    {
        for (int i = 0; i < 10; i++)
        {
            inventory_item_list.Add(new Item());
            inventory_slot_list.Add(Instantiate(inventory_slot));
            inventory_slot_list[i].name = "Slot#" + (i + 1);

            inventory_slot_list[i].transform.GetComponent<InventorySlot>().item_id = i;
            inventory_slot_list[i].transform.SetParent(inventory_panel_slot.transform, false);
        }
        AddSavedItems();
    }
    private void Add_Item(Item item , int index)
    {
        Item candidateItem = new Item(item);
        for (int i = index; i < inventory_item_list.Count; i++)
        {
            if (inventory_item_list[i].id == 0)
            {
                // To do: Fix the itemInfo sorting bug
                inventory_item_list[i] = candidateItem;
                GameObject new_item = Instantiate(inventory_item);
                new_item.GetComponent<InventoryItem>().itemInfo = candidateItem;
                new_item.GetComponent<InventoryItem>().slot = i;
                new_item.GetComponent<Image>().sprite = SaveSystem.GetSprite(candidateItem.spriteName);
                new_item.name = candidateItem.itemName;
                new_item.transform.SetParent(inventory_slot_list[i].transform, false);
                new_item.GetComponent<RectTransform>().anchoredPosition = new Vector2(58,58);
                break;
            }
        }
    }

    public void LoadInfo()
    {
        GameObject.Find("Stats_Amount").GetComponent<TextMeshProUGUI>().text =
            "<align=center><line-height=110%>Stats</align>\n</line-height>" +
            "Health: " + SaveSystem.GetChar().GetStat("Health").maxValue + "\n" +
            "Mana: " + SaveSystem.GetChar().GetStat("Mana").maxValue  + "\n" +
            "Defence: " + SaveSystem.GetChar().GetStat("Defence").baseValue + "%\n" +
            "Damage: " + SaveSystem.GetChar().GetStat("Damage").baseValue + "\n" +
            "Power: " + SaveSystem.GetChar().GetStat("Power").baseValue + "%" + "\n" +
            "Magic: " + SaveSystem.GetChar().GetStat("Magic").baseValue + "%" + "\n" +
            "Hits to Crit: " + SaveSystem.GetChar().GetStat("Crit").baseValue;

        GameObject.Find("Panel_Coins").transform.Find("Text_Amount").GetComponent<TextMeshProUGUI>().text = SaveSystem.GetChar().GetStat("Gold").baseValue.ToString();
    }
    private void LoadWeapon()
    {
        Item weapon = Database_Items.GetItemByName(Database_Saves.Instance.GetHero().weaponName);
        transform.FindDeepChild("Weapon_Slot").GetChild(0).GetComponent<Image>().sprite = SaveSystem.GetSprite(weapon.spriteName);
    }
    private Item[] parse_inventory()
    {
        Item[] inventory = new Item[10];
        for (int a = 0; a < 10; a++)
        {
            //inventory[a] = inventory_item_list[a].ID;
            if (inventory_slot_list[a].transform.childCount > 0)
            {
                inventory[a] = inventory_slot_list[a].transform.GetChild(0).GetComponent<InventoryItem>().itemInfo;
                //Debug.Log(inventory_slot_list[a].transform.GetChild(0).GetComponent<InventoryItem>().itemInfo.Title);
            }
            else
                inventory[a] = new Item();
        }

        return inventory;
    }
    public void parse_information()
    {
        SaveSystem.Instance.SetCharInventory(parse_inventory());

    }
    public Database_Items getDatabaseItems()
    {
        return database_items;
    }
}
