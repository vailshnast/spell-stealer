﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
{

    public Item itemInfo;
    public int amount;
    public int slot;

    private Inventory inventory;
    public InventoryTooltip tooltip;

    private Vector3 offset;
    private Vector3 screenPoint;

    void Start()
    {
        inventory = FindObjectOfType<Inventory>();
        tooltip = FindObjectOfType<InventoryTooltip>();
    } 

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Main_Manager.Instance.currentState == StageState.SkillTree || Main_Manager.Instance.currentState == StageState.Map)
            return;

        FindObjectOfType<InventoryTooltip>().Deactivate();
        InventoryTooltip.Instance.SetItem(this);
        if (itemInfo != null)
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

            this.transform.SetParent(transform.parent.parent.parent.parent.parent, false);

            

            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Main_Manager.Instance.currentState == StageState.SkillTree || Main_Manager.Instance.currentState == StageState.Map)
            return;

        for (int i = 0; i < inventory.inventory_slot_list.Count; i++)
        {
            inventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        }
        if (itemInfo != null)
        {
            Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
            transform.position = cursorPosition;
        }

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (Main_Manager.Instance.currentState == StageState.SkillTree || Main_Manager.Instance.currentState == StageState.Map)
            return;

        gameObject.transform.SetParent(inventory.inventory_slot_list[slot].transform, false);
        transform.position = inventory.inventory_slot_list[slot].transform.position;
        GetComponent<RectTransform>().sizeDelta = new Vector2(94, 94);
        GetComponent<CanvasGroup>().blocksRaycasts = true;

    }

    public void OnPointerClick(PointerEventData eventData)
    {       
        if (itemInfo.itemType == "Rune" && tooltip.scrollIsUsed)
        {
            if (SaveSystem.Instance.IdentifyRune(itemInfo))
            {
                tooltip.scrollIsUsed = false;
                tooltip.DestroyItem();
            }
            else
                tooltip.scrollIsUsed = false;

        }
        else
            tooltip.scrollIsUsed = false;

        for (int i = 0; i < inventory.inventory_slot_list.Count; i++)
        {
            if (inventory.inventory_slot_list[i] != transform.parent)
            {
                inventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
            }
        }

        transform.parent.GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
        tooltip.Activate(this);

    }
}
