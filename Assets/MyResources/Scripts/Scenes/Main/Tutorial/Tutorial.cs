﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{

    public static Tutorial Instance { get; private set; }
    [HideInInspector]
    public TutorialState currentState;
    private GameObject tutorialTooltip;
    private TextMeshProUGUI tutorialText;

	// Use this for initialization
    void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        tutorialTooltip = transform.FindDeepChild("Tutorial").gameObject;

        tutorialText = transform.FindDeepChild("Tutorial Tooltip").FindDeepChild("Tooltip Text").GetComponent<TextMeshProUGUI>();

        if (Main_Manager.Instance.showTutorial)
            EnterState(TutorialState.SelectUndeadLair);
        else
            tutorialTooltip.SetActive(false);


    }

    public void Procceed()
    {
        Debug.Log("proceed");
        ChangeState((TutorialState) ((int) currentState + 1));
    }

    public void ChangeState(TutorialState state)
    {
        currentState = state;
        Debug.Log(currentState);
        EnterState(currentState);
    }
    /*
     
1. Select the Undead Lair.
2. Press this button to enter Undead Lair.
3. Press Move button, to explore the next room.
4. Tap to the targets to deal damage.
5. Tap the screen right in this moment for a successful block.
6. Press the Spell Steal button, to start a mini-game
7. Draw a symbol
8. Now cast a spell! Press this button.
9. Take a reward
10. Restart the game.
 
     */
    public void EnterState(TutorialState state)
    {
        switch (state)
        {
            case TutorialState.SelectUndeadLair:
                ChangeTooltipText("Select the Undead Lair.");
                break;
            case TutorialState.ExploreUndeadLair:
                ChangeTooltipText("Press this button to enter Undead Lair.");
                break;
            case TutorialState.MoveToNextRoom:
                ChangeTooltipText("Press Move button, to explore the next room.");
                GetButton("Move_Button").button.GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
                break;
            case TutorialState.PressFight:
                ChangeTooltipText("Press fight button and tap to the targets to deal damage.");
                tutorialTooltip.transform.SetParent(transform.FindDeepChild("Battle"), false);
                tutorialTooltip.transform.SetAsLastSibling();
                tutorialTooltip.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 300);
                break;
            case TutorialState.WaitForGuard:
                tutorialTooltip.SetActive(false);               
                break;           
            case TutorialState.TapOnBlock:
                tutorialTooltip.SetActive(true);
                ChangeTooltipText("Tap the screen right in this moment for a successful block.");
                break;
            case TutorialState.WaitForSpell:
                tutorialTooltip.SetActive(false);
                break;
            case TutorialState.ActivateSpellSteal:
                tutorialTooltip.SetActive(true);
                tutorialTooltip.transform.SetSiblingIndex(1);
                tutorialTooltip.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -125);
                ChangeTooltipText("Press the Spell Steal button, to start a mini-game");
                break;
            case TutorialState.WaitForSteal:
                tutorialTooltip.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 350);
                ChangeTooltipText("Draw a symbol");
                break;
            case TutorialState.CastSpell:
                tutorialTooltip.SetActive(true);
                tutorialTooltip.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -125);
                ChangeTooltipText("Now my dear Harry Potter cast your spell!");
                break;
            case TutorialState.Victory:
                tutorialTooltip.SetActive(false);
                break;
            case TutorialState.TakeReward:
                tutorialTooltip.SetActive(true);
                tutorialTooltip.transform.SetParent(transform.FindDeepChild("UI"), false);
                tutorialTooltip.transform.SetSiblingIndex(6);
                tutorialTooltip.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -125);
                ChangeTooltipText("Take a reward");
                break;
        }
    }

    private Button GetButton(string name)
    {
        return Main_Manager.Instance.GetButton(name);
    }
    private void ChangeTooltipText(string text)
    {
        tutorialText.text = text;
    }

    private void SetSprite(Image img, string name)
    {
        img.sprite = SaveSystem.GetSprite(name);
    }

    private GameObject GetGameObject(string name)
    {
        return transform.FindDeepChild(name).gameObject;
    }
}
