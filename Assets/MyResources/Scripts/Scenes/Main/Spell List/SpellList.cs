﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpellList : MonoBehaviour
{
    public static SpellList Instance;

    private bool initialised;
    private GameObject tooltip;

    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        tooltip = transform.FindDeepChild("Spell Tooltip").gameObject;
        ProccessSpellSlots();
        initialised = true;
    }

    void OnEnable()
    {
        if(initialised)
            ProccessSpellSlots();
    }

    private void ProccessSpellSlots()
    {
        List<SpellInfo> spellInfoList = Database_Saves.Instance.GetHero().spellDatabase;

        Transform spellObject;

        for (int i = 0; i < spellInfoList.Count; i++)
        {
            spellObject = transform.FindDeepChild(spellInfoList[i].spellName);

            if (spellInfoList[i].stolen)
            {
                spellObject.FindDeepChild("Spell Image").GetComponent<Image>().sprite = SaveSystem.GetSprite(spellInfoList[i].spellName);
                spellObject.FindDeepChild("Spell Name").GetComponent<TextMeshProUGUI>().text = spellInfoList[i].spellName;


                if (!spellInfoList[i].viewed)
                    spellObject.FindDeepChild("Spell Slot").GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
                else
                    spellObject.FindDeepChild("Spell Slot").GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
            }
            else
            {
                spellObject.FindDeepChild("Spell Image").GetComponent<Image>().sprite = SaveSystem.GetSprite("Unknown");
                spellObject.FindDeepChild("Spell Name").GetComponent<TextMeshProUGUI>().text = "?????????";
                spellObject.FindDeepChild("Spell Slot").GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
            }
                          

            
        }
    }

    private string GetDescription(string spellName)
    {
        string description = "";

        switch (spellName)
        {
            case "Unholy Smite":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Deal <color=red>" + 75 * SaveSystem.GetChar().GetStat("Power").baseValue / 100 + "</color> damage to an enemy" + "\n" +
                              "ManaCost: " + GetManaCost(20 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 1 sec";
                break;
            case "Withering":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Reduces enemy health by 3% per 1 sec" + "\n" +
                              "Debuff duration: <color=#00DFFFFF>" + 2 * (float) SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "</color> sec" + "\n" +
                              "ManaCost: " + GetManaCost(25 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 3 sec";
                break;
            case "Agony":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Kills yourself and deals devastating damage to an enemy" + "\n" +
                              "ManaCost: " + GetManaCost(10 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 3 sec";
                break;
            case "Soul Cry":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Reduces enemy damage by <color=red>" + 7.5f * SaveSystem.GetChar().GetStat("Power").baseValue / 100 + "%</color>" + "\n" +
                              "Debuff duration: <color=#00DFFFFF>" + 5 * (float)SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "</color> sec" + "\n" +
                              "ManaCost: " + GetManaCost(15 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 3 sec";
                break;
            case "Decrepify":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Delays enemy attack by <color=#00DFFFFF>" +  (float)SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "</color> sec" + "\n" +
                              "ManaCost: " + GetManaCost(15 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 3 sec";
                break;
            case "Life Tap":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Deal <color=#00DFFFFF>" + 75 * SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "</color> damage to an enemy" + "\n" +
                              "Restores " + (int)(75 * (float)SaveSystem.GetChar().GetStat("Magic").baseValue / 100 * 0.2f) + " health to yourself" + "\n" +
                              "ManaCost: " + GetManaCost(25 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 1 sec";
                break;
            case "Unholy Heal":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Deal <color=#00DFFFFF>" + (int)((float)SaveSystem.GetChar().GetStat("Health").maxValue / 100 + SaveSystem.GetChar().GetStat("Magic").baseValue / 100 * 8)
                              + "</color> damage to yourself" + "\n" +
                              "ManaCost: " + GetManaCost(15 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 2 sec";
                break;
            case "Exhaust":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Deal <color=#00DFFFFF>" + 100 * SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "</color> damage to an enemy" + "\n" +
                              "Deal <color=#00DFFFFF>" + (int)(8 * (float)SaveSystem.GetChar().GetStat("Magic").baseValue / 100) + "</color> damage per sec to an enemy" + "\n" +
                              "Debuff duration: <color=#00DFFFFF>" + 1 * (float)SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "</color> sec" + "\n" +
                              "ManaCost: " + GetManaCost(30 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 1 sec";
                break;
            case "Decay":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "Reduces enemy defence by <color=red>" + 8 * SaveSystem.GetChar().GetStat("Power").baseValue / 100 + "%</color>" + "\n" +
                              "Debuff duration: <color=#00DFFFFF>" + 4 * (float)SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "</color> sec" + "\n" +
                              "ManaCost: " + GetManaCost(15 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 1 sec";
                break;
            case "Soul Torment":
                description = "<size=55><b><color=#FFA400FF>" + spellName + "</color></b></size>\n" +
                              "<align=left>" + "<align=left>" + "Reduces enemy health by 1% per 1 sec" + "\n" +
                              "Reduces enemy defence by <color=red>" + 15 * SaveSystem.GetChar().GetStat("Power").baseValue / 100 + "%</color>" + "\n" +
                              "Reduces enemy damage by <color=red>" + 10 * SaveSystem.GetChar().GetStat("Power").baseValue / 100 + "%</color>" + "\n" +
                              "Reduces enemy power by <color=#00DFFFFF>" + 50 * SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "%</color>" + "\n" +
                              "Reduces enemy magic by <color=#00DFFFFF>" + 50 * SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + "%</color>" + "\n" +
                              "Delays enemy attack by <color=#9A20FFFF>" + ((float)SaveSystem.GetChar().GetStat("Magic").baseValue / 200 + (float)SaveSystem.GetChar().GetStat("Power").baseValue / 200) + "</color> sec" + "\n" +
                              "Debuff duration: <color=#9A20FFFF>" + ((float)SaveSystem.GetChar().GetStat("Magic").baseValue / 100 + (float)SaveSystem.GetChar().GetStat("Power").baseValue / 100) * 2f + "</color> sec" + "\n" +
                              "ManaCost: " + GetManaCost(40 + SaveSystem.GetChar().GetStat("Mana Cost").baseValue) + "\n" +
                              "Cooldown: 25 sec";
                break;
        }


        return description;
    }

    public void ActivateTooltip(string spellName)
    {
        SpellInfo spellInfo = Database_Saves.Instance.GetHero().GetSpellInfo(spellName);

        if(!spellInfo.stolen)
            return;
        
        tooltip.SetActive(true);

        tooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = GetDescription(spellName);

        Transform spellObject = transform.FindDeepChild(spellName);

        spellObject.FindDeepChild("Spell Slot").GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");

        Database_Saves.Instance.GetHero().ViewSpell(spellName);
        Database_Saves.Instance.Save();

    }

    private int GetManaCost(int manacost)
    {
        return manacost < 0 ? 0 : manacost;
    }
    public void Close()
    {
        tooltip.SetActive(false);
    }
}
