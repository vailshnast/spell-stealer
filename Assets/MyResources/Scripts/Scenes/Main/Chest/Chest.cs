﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class Chest : MonoBehaviour
{
    private bool initialised;

    private readonly List<GameObject> slots = new List<GameObject>();
    private Chest_Slot current_Slot
    {
        get
        {
            return (from t in slots where t.GetComponent<Chest_Slot>().slot_ID == 0 select t.GetComponent<Chest_Slot>()).FirstOrDefault();
        }
    }

    private Setting setting;

    private GameObject chest_panel;
    private TextMeshProUGUI attemptsText;
    private TextMeshProUGUI title;

    public float distance = 15;
    public float size = 30;
    public float forward_scroll_time = 0.75f;
    public float backward_scroll_time = 0.2f;

    private int slots_amount { get; set; }
    private int attempts { get; set; }

    private bool scrolling;
    private ChestState currentState;

    void Start()
    {
        Init();
    }

    void Init()
    {
        setting = Database_Settings.Instance.GetSettings();

        attemptsText = transform.FindDeepChild("Count").GetComponent<TextMeshProUGUI>();
        chest_panel = transform.FindDeepChild("Chest_Panel").gameObject;
        title = transform.parent.FindDeepChild("Title_Bar").GetChild(0).GetComponent<TextMeshProUGUI>();

        slots_amount = setting.chestslot_amount;
        attempts = setting.lockpick_attempts;

        attemptsText.text = attempts.ToString();

        initialised = true;
    }
    private void OnEnable()
    {
        if(!initialised) return;

        Refresh();
    }

    private void Refresh()
    {
        transform.FindDeepChild("Chest_Image").GetComponent<Image>().sprite = SaveSystem.GetSprite("Chest_Locked");
        chest_panel.SetActive(false);
        currentState = ChestState.Prepare;
        attempts = setting.lockpick_attempts;
        attemptsText.text = attempts.ToString();
        scrolling = false;
        Clear();
    }

    private void Create_Slots(int amount)
    {
        slots.Clear();

        if (amount < 2)
            amount = 2;

        for (int i = 0; i < amount; i++)
        {
            slots.Add(Instantiate(Resources.Load("Prefabs/Chest/Slot"), GameObject.Find("Slots").transform, false) as GameObject);
        }
        Init_Slots();
    }

    private void Init_Slots()
    {        
        for (int i = 0; i < slots.Count; i++)
        {
            RectTransform slot_Rect = slots[i].GetComponent<RectTransform>();
            RectTransform previous_Slot_Rect = i > 0 ? slots[i-1].GetComponent<RectTransform>() : null;
            slots[i].GetComponent<Chest_Slot>().slot_ID = i;

            float local_Size = i * size < size*2 ? i * size : size*2;
            //size
            slot_Rect.sizeDelta = new Vector2(slot_Rect.sizeDelta.x - local_Size, slot_Rect.sizeDelta.x - local_Size);
            slot_Rect.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(slot_Rect.sizeDelta.x, slot_Rect.sizeDelta.y);
            //position
            slot_Rect.anchoredPosition = i == 0 ? new Vector2(0, 391) : new Vector2(previous_Slot_Rect.anchoredPosition.x + previous_Slot_Rect.sizeDelta.x / 2 + slot_Rect.sizeDelta.x / 2 + distance, 391);
            //color
            slot_Rect.GetComponent<Image>().color = i == 0 ? slot_Rect.GetComponent<Image>().color : this.HexToColor("787878FF");
            slot_Rect.GetChild(0).GetComponent<Image>().color = slot_Rect.GetComponent<Image>().color;
            //set direction
            slots[i].GetComponent<Chest_Slot>().SetDirection();
        }
    }
    
    public void Unlock(Arrow_Direction direction)
    {
        if(scrolling || currentState == ChestState.Failed) return;

        if (current_Slot && current_Slot.Unlocked(direction))
        {
            Scroll_Forward();

            if (slots[slots.Count - 1].GetComponent<Chest_Slot>().unlocked)
                Open_Chest();
        }
        else
        {
            if (currentState == ChestState.Failed) return;

            StartCoroutine(MultiScroll());     
            Fail();     
        }         
    }

    private void Clear()
    {
        StopAllCoroutines();
        slots.Clear();        
        GameObject.Find("Slots").transform.DestroyChildren();
    }

    private void Fail()
    {
        attempts--;
        attemptsText.text = attempts.ToString();

        if (attempts <= 0)
        {
            StopAllCoroutines();
            StartCoroutine(Fade(1.25f));
            currentState = ChestState.Failed;
        }
    }
    private IEnumerator Fade(float time)
    {
        Graphic[] graphicComponents = GetComponentsInChildren<Graphic>();

        for (int i = 0; i < graphicComponents.Length; i++)
        {
            graphicComponents[i].CrossFadeAlpha(0,1.25f,true);
        }

        yield return new WaitForSeconds(time);
        Main_Manager.Instance.SwitchState(StageState.Level);
    }
    private void Open_Chest()
    {
        Clear();
        transform.FindDeepChild("Chest_Image").GetComponent<Image>().sprite = SaveSystem.GetSprite("Chest_Opened");
        chest_panel.SetActive(false);
        title.text = "Unlocked Chest";
        Main_Manager.Instance.SetInventory(true,true);
    }
    public void Pressed_LeftTap()
    {
        Unlock(Arrow_Direction.Left);
    }
    public void Pressed_RightTap()
    {
        Unlock(Arrow_Direction.Right);
    }
    public void StartLockPicking()
    {
        if (currentState != ChestState.Prepare) return;

        chest_panel.SetActive(true);
        Create_Slots(slots_amount);

        title.text = "Lockpicking...";

        currentState = ChestState.LockPicking;;
    }

    #region Scroll
    private IEnumerator Lerp_Width(float time, RectTransform rect, float width)
    {
        Vector2 originalScale = rect.sizeDelta;
        Vector2 targetScale = new Vector2(width, width);
        float originalTime = time;


        while (time > 0.0f)
        {
            time -= Time.deltaTime;
            rect.sizeDelta = Vector3.Lerp(targetScale, originalScale, time / originalTime);
            rect.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y);
            yield return null;

        }
    }
    private IEnumerator Scroll_Slot(RectTransform rect, Vector2 position, float width, float timeToMove, Color target_Color, bool forward)
    {
        StartCoroutine(Lerp_Width(timeToMove, rect, width));

        Vector2 currentPos = rect.anchoredPosition;
        Color original_Color = rect.GetComponent<Image>().color;

        float t = 0f;

        while (t <= 1)
        {
            t += Time.deltaTime / timeToMove;
            rect.anchoredPosition = Vector2.Lerp(currentPos, position, t);
            rect.GetComponent<Image>().color = Color.Lerp(original_Color, target_Color, t);
            rect.GetChild(0).GetComponent<Image>().color = rect.GetComponent<Image>().color;
            yield return null;
        }

        Chest_Slot slot = rect.GetComponent<Chest_Slot>();

        if ((slot.slot_ID == 0 || slot.slot_ID == 1) && !forward)
            slot.SetImage(false);

        scrolling = false;
    }
    private void Scroll_Forward()
    {
        //If we use scrolling method,
        //when its already running,
        //different bugs will appear
        if (!scrolling)
        {
            scrolling = true;
            //Set tempWidth and ID
            for (int i = 0; i < slots.Count; i++)
            {
                Chest_Slot chest_Slot = slots[i].GetComponent<Chest_Slot>();
                RectTransform slot_Rect = slots[i].GetComponent<RectTransform>();
                chest_Slot.slot_ID--;

                if (chest_Slot.slot_ID == 0 || chest_Slot.slot_ID == 1)
                    chest_Slot.temp_Width = slot_Rect.sizeDelta.x + size;
                else if (chest_Slot.slot_ID == -1 || chest_Slot.slot_ID == -2)
                    chest_Slot.temp_Width = slot_Rect.sizeDelta.x - size;
                else chest_Slot.temp_Width = slot_Rect.sizeDelta.x;

                slot_Rect.GetComponent<Chest_Slot>().previousPos = slot_Rect.anchoredPosition;
            }
            for (int i = 0; i < slots.Count; i++)
            {
                RectTransform slot_Rect = slots[i].GetComponent<RectTransform>();

                Chest_Slot current_Slot = slots[i].GetComponent<Chest_Slot>();
                Chest_Slot previous_Slot = i > 0 ? slots[i - 1].GetComponent<Chest_Slot>() : slots[i + 1].GetComponent<Chest_Slot>();
                Color color = current_Slot.slot_ID == 0 ? Color.white : this.HexToColor("787878FF");

                if (i == 0)
                    StartCoroutine(Scroll_Slot(slot_Rect, new Vector2(slot_Rect.anchoredPosition.x - previous_Slot.temp_Width / 2 - current_Slot.temp_Width / 2 - distance, 391), current_Slot.temp_Width, forward_scroll_time, color, true));
                else
                    StartCoroutine(Scroll_Slot(slot_Rect, new Vector2(previous_Slot.previousPos.x, 391), current_Slot.temp_Width, forward_scroll_time, color, true));

            }
        }
    }
    private void Scroll_Back()
    {
        if (!scrolling)
        {
            scrolling = true;
            //Set tempWidth and ID
            for (int i = 0; i < slots.Count; i++)
            {
                Chest_Slot chest_Slot = slots[i].GetComponent<Chest_Slot>();
                RectTransform slot_Rect = slots[i].GetComponent<RectTransform>();
                chest_Slot.slot_ID++;

                if (chest_Slot.slot_ID == -1 || chest_Slot.slot_ID == 0)
                    chest_Slot.temp_Width = slot_Rect.sizeDelta.x + size;
                else if (chest_Slot.slot_ID == 1 || chest_Slot.slot_ID == 2)
                    chest_Slot.temp_Width = slot_Rect.sizeDelta.x - size;
                else chest_Slot.temp_Width = slot_Rect.sizeDelta.x;


                slot_Rect.GetComponent<Chest_Slot>().previousPos = slot_Rect.anchoredPosition;
            }
            for (int i = slots.Count - 1; i >= 0; i--)
            {
                RectTransform slot_Rect = slots[i].GetComponent<RectTransform>();

                Chest_Slot current_Slot = slots[i].GetComponent<Chest_Slot>();
                Chest_Slot previous_Slot;
                if (i == slots.Count - 1)
                    previous_Slot = slots[i - 1].GetComponent<Chest_Slot>();
                else
                    previous_Slot = slots[i + 1].GetComponent<Chest_Slot>();

                Color color = current_Slot.slot_ID == 0 ? Color.white : this.HexToColor("787878FF");

                //if(current_Slot.slot_ID == 0)
                //  current_Slot.SetImage(false);

                if (i == slots.Count - 1)
                    StartCoroutine(Scroll_Slot(slot_Rect, new Vector2(slot_Rect.anchoredPosition.x + previous_Slot.temp_Width / 2 + current_Slot.temp_Width / 2 + distance, 391), current_Slot.temp_Width, backward_scroll_time, color, false));
                else
                    StartCoroutine(Scroll_Slot(slot_Rect, new Vector2(previous_Slot.previousPos.x, 391), current_Slot.temp_Width, backward_scroll_time, color, false));
            }

        }
    }
    private IEnumerator MultiScroll()
    {
        Chest_Slot chest_Slot = slots[0].GetComponent<Chest_Slot>();
        scrolling = true;
        yield return new WaitForSeconds(0.5f);
        scrolling = false;
        while (chest_Slot.slot_ID != 0)
        {
            Scroll_Back();
            yield return null;
        }
        chest_Slot.SetImage(false);
    }
    #endregion
}
