﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest_Slot:MonoBehaviour{

    public float temp_Width { get; set; }
    public int slot_ID { get; set; }
    
    public bool unlocked { get;private set;}
    [HideInInspector]
    public Arrow_Direction direction;
    public Vector2 previousPos { get; set; }

    public void SetDirection()
    {
        direction = (Arrow_Direction)Random.Range(0, 2); 
    }

    public bool Unlocked(Arrow_Direction dir)
    {
        SetImage(true);

        if (direction == dir)
        {
            transform.GetChild(0).GetComponent<Image>().sprite =
                SaveSystem.GetSprite("Right");

            unlocked = true;
            return true;
        }



        transform.GetChild(0).GetComponent<Image>().sprite =
            SaveSystem.GetSprite("Wrong");

        unlocked = false;

        return false;
    }

    public void SetImage(bool active)
    {
        transform.GetChild(0).GetComponent<Image>().enabled = active;
    }
}
public enum Arrow_Direction
{
    Left,
    Right
}
