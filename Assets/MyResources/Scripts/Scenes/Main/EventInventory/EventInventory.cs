﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class EventInventory : MonoBehaviour
{
    #region Variables

    [HideInInspector]
    public SaveSystem save_system;

    private GameObject inventory_panel_slot;

    private GameObject reward_panel_slot;
    [HideInInspector]
    public List<GameObject> inventory_slot_list = new List<GameObject>();
    [HideInInspector]
    public List<Item> inventory_item_list = new List<Item>();

    //If we dont check this in OnEnable, will cause different bugs,
    //because, OnEnable is called before Start().
    private bool initialised;

    #endregion

    #region Init

    void Start()
    {
        Init();      
    }
    void OnEnable()
    {
        if(initialised)
        {                     
            DeleteItems(0);
            SetSlots();
            InitialiseInventory();           
        }
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            DeleteItems(0);
            SetSlots();
            InitialiseInventory();
        }
    }
    private void Init()
    {
        save_system = GameObject.Find("Save_System").GetComponent<SaveSystem>();
        
        inventory_panel_slot = transform.FindDeepChild("Inventory_Items").Find("Slots_Panel").gameObject;
        reward_panel_slot = transform.FindDeepChild("Reward_Items").Find("Slots_Panel").gameObject;

        CreateInventory();
        InitialiseInventory();
        initialised = true;
    }

    #endregion
   

    #region Inventory
    private void CreateInventory()
    {
        for (int i = 0; i < 15; i++)
        {
            if (i < 10)
            {
                inventory_item_list.Add(new Item());
                inventory_slot_list.Add(Instantiate(Resources.Load("Prefabs/Slot_Reward", typeof(GameObject)) as GameObject));
                inventory_slot_list[i].name = "Slot#" + (i + 1);

                inventory_slot_list[i].transform.GetComponent<EventSlot>().item_id = i;
                inventory_slot_list[i].transform.SetParent(inventory_panel_slot.transform, false);

                switch (Main_Manager.Instance.currentState)
                {
                    case StageState.Merchant:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(true, TooltipType.Sellable);
                        break;
                    default:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(true, TooltipType.Usable);
                        break;
                }
            }

            if (i >= 10)
            {
                inventory_item_list.Add(new Item());
                inventory_slot_list.Add(Instantiate(Resources.Load("Prefabs/Slot_Reward", typeof(GameObject)) as GameObject));
                inventory_slot_list[i].name = "Slot#" + (i + 1);

                inventory_slot_list[i].transform.GetComponent<EventSlot>().item_id = i;
                inventory_slot_list[i].transform.SetParent(reward_panel_slot.transform, false);

                switch (Main_Manager.Instance.currentState)
                {
                    case StageState.Merchant:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(false, TooltipType.Buyable);
                        break;
                    default:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(true, TooltipType.Usable);
                        break;
                }
            }

        }

    }
    private void InitialiseInventory()
    {      
        Info_Load_Gold();
        AddSavedItems();
        AddEventItems();
    }

    private void SetSlots()
    {
        for (int i = 0; i < 15; i++)
        {
            if (i < 10)
            {
                switch (Main_Manager.Instance.currentState)
                {
                    case StageState.Merchant:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(true, TooltipType.Sellable);
                        break;
                    default:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(true, TooltipType.Usable);
                        break;
                }
            }

            if (i >= 10)
            {

                switch (Main_Manager.Instance.currentState)
                {
                    case StageState.Merchant:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(false, TooltipType.Buyable);
                        break;
                    default:
                        inventory_slot_list[i].GetComponent<EventSlot>().SetSlot(true, TooltipType.Usable);
                        break;
                }
            }
        }
    }

    #endregion

    #region Other
    private bool SlotsAreFull()
    {
        for (int i = 0; i < 10; i++)
        {
            if (inventory_item_list[i].id == 0)
                return false;
        }
        return true;
    }
    public void Info_Load_Gold()
    {
        GameObject.Find("Gold_quantity").transform.Find("Amount").GetComponent<TextMeshProUGUI>().text = SaveSystem.GetChar().GetStat("Gold").baseValue.ToString();
    }
    #endregion

    #region Items
    private void DeleteItems(int index)
    {
        for (int i = index; i < inventory_slot_list.Count; i++)
        {
            if (inventory_slot_list[i].transform.childCount > 0)
                Destroy(inventory_slot_list[i].transform.GetChild(0).gameObject);
            inventory_item_list[i] = new Item();
        }
    }
    private void DeleteItem(int index)
    {
        if (inventory_slot_list[index].transform.childCount > 0)
            Destroy(inventory_slot_list[index].transform.GetChild(0).gameObject);

        inventory_item_list[index] = new Item();
    }
    public void AddEventItems()
    {
        List<Reward> eventItems = new List<Reward>();

        switch (Main_Manager.Instance.currentState)
        {
            case StageState.Chest:

                Add_Item("Magic Rune", 10);
                for (int i = 0; i < 4; i++)
                {
                    Add_Item("Emerald", 10);
                }

                break;
            case StageState.Merchant:
                Add_Item("Health Potion", 10);
                Add_Item("Rune Scroll", 10);
                Add_Item("Runic Book", 10);
                Add_Item("Magic Rune", 10,false);
                break;
            default:
                Monster monster = new Monster(SaveSystem.Instance.currentMonster);
                eventItems = monster.Rewards.OrderBy(w => w.priority).ToList();
                RandomItems(eventItems);
                break;
        }

    }
    private void RandomItems(List<Reward> eventItems)
    {
        int index = 0;

        List<Reward> runes = new List<Reward>();
        for (int i = eventItems.Count - 1; i >= 0; i--)
        {
            if (eventItems[i].GetType() == "Rune")
            {
                runes.Add(eventItems[i]);
                eventItems.RemoveAt(i);
            }               
        }
        runes = runes.OrderBy(w => w.priority).ToList();

        for (int i = 0; i < runes.Count; i++)
        {
            float randomChance = Random.value * 100;
            //Debug.Log("random: " + randomChance + " / chanceToGet: " + monster.Rewards[i].chanceToGet);
            if (randomChance < runes[i].chanceToGet && index < 5)
            {
                Add_Item(runes[i].itemName, 10);
                index++;
                //Debug.Log(runes[i].itemName);
                break;
            }
        }

        eventItems = eventItems.OrderBy(w => w.priority).ToList();

        for (int i = 0; i < eventItems.Count; i++)
        {
            float randomChance = Random.value * 100;
            //Debug.Log("random: " + randomChance + " / chanceToGet: " + monster.Rewards[i].chanceToGet);
            if (randomChance < eventItems[i].chanceToGet && index < 5)
            {
                Add_Item(eventItems[i].itemName, 10);
                index++;
            }
        }
    }
    public void Add_Item(string name,int index,bool identifyRune = true)
    {
        Item candidateItem = Database_Items.GetItemByName(name);

        if (candidateItem.itemType == "Rune" && identifyRune)
        {
            Debug.Log(Database_Saves.Instance.GetHero().GetPassive(3, PassiveType.General).currentLevel);
            switch (candidateItem.rarity)
            {
                case Rarity.Magic:
                    if (Database_Saves.Instance.GetHero().GetPassive(3, PassiveType.General).currentLevel == 1)
                        SaveSystem.Instance.IdentifyRune(candidateItem);
                        break;
            }
        }

        for (int i = index; i < inventory_item_list.Count; i++)
        {
            if (inventory_item_list[i].id == 0)
            {
                // To do: Fix the itemInfo sorting bug
                inventory_item_list[i] = candidateItem;
                GameObject new_item = Instantiate(Resources.Load("Prefabs/Item_Reward", typeof(GameObject)) as GameObject);
                new_item.GetComponent<EventItem>().itemInfo = candidateItem;
                new_item.GetComponent<EventItem>().slot = i;
                new_item.GetComponent<Image>().sprite = SaveSystem.GetSprite(candidateItem.spriteName);
                new_item.name = candidateItem.itemName;
                new_item.transform.SetParent(inventory_slot_list[i].transform, false);
                new_item.GetComponent<EventItem>().SetSlot(inventory_slot_list[i].GetComponent<EventSlot>().isDraggble, inventory_slot_list[i].GetComponent<EventSlot>().tooltipType);
                new_item.GetComponent<RectTransform>().anchoredPosition = new Vector2(58, 58);
                break;
            }
        }
    }
    public void Add_Item(Item item,int index)
    {
        Item candidateItem = new Item(item);

        for (int i = index; i < inventory_item_list.Count; i++)
        {
            if (inventory_item_list[i].id == 0)
            {
                // To do: Fix the itemInfo sorting bug
                inventory_item_list[i] = candidateItem;
                GameObject new_item = Instantiate(Resources.Load("Prefabs/Item_Reward", typeof(GameObject)) as GameObject);
                new_item.GetComponent<EventItem>().itemInfo = candidateItem;
                new_item.GetComponent<EventItem>().slot = i;
                new_item.GetComponent<Image>().sprite = SaveSystem.GetSprite(candidateItem.spriteName);
                new_item.name = candidateItem.itemName;
                new_item.transform.SetParent(inventory_slot_list[i].transform, false);
                new_item.GetComponent<EventItem>().SetSlot(inventory_slot_list[i].GetComponent<EventSlot>().isDraggble, inventory_slot_list[i].GetComponent<EventSlot>().tooltipType);
                new_item.GetComponent<RectTransform>().anchoredPosition = new Vector2(58, 58);
                break;
            }
        }
    }
    public bool Bought(Item item, bool identifyRune = true)
    {
        Item candidateItem = new Item(item);

        if (candidateItem.itemType == "Rune" && identifyRune)
        {
            Debug.Log(Database_Saves.Instance.GetHero().GetPassive(3, PassiveType.General).currentLevel);
            switch (candidateItem.rarity)
            {
                case Rarity.Magic:
                    if (Database_Saves.Instance.GetHero().GetPassive(3, PassiveType.General).currentLevel == 1)
                        SaveSystem.Instance.IdentifyRune(candidateItem);
                    break;
            }
        }

        for (int i = 0; i < 10; i++)
        {
            if (inventory_item_list[i].id == 0)
            {
                // To do: Fix the itemInfo sorting bug
                inventory_item_list[i] = candidateItem;
                GameObject new_item = Instantiate(Resources.Load("Prefabs/Item_Reward", typeof(GameObject)) as GameObject);
                new_item.GetComponent<EventItem>().itemInfo = candidateItem;
                new_item.GetComponent<EventItem>().slot = i;
                new_item.GetComponent<Image>().sprite = SaveSystem.GetSprite(candidateItem.spriteName);
                new_item.name = candidateItem.itemName;
                new_item.transform.SetParent(inventory_slot_list[i].transform, false);
                new_item.GetComponent<EventItem>().SetSlot(inventory_slot_list[i].GetComponent<EventSlot>().isDraggble, inventory_slot_list[i].GetComponent<EventSlot>().tooltipType);
                new_item.GetComponent<RectTransform>().anchoredPosition = new Vector2(58, 58);
                return true;
            }
        }
        return false;
    }
    #endregion

    #region Buttons
    public void TakeAll()
    {
        if (Main_Manager.Instance.showTutorial)
        {
            SceneManager.LoadScene(0);
            return;
        }

        if (!SlotsAreFull())
        {
            for (int i = 10; i < 15; i++)
            {
                if (inventory_slot_list[i].transform.childCount > 0 && inventory_slot_list[i].transform.GetChild(0).GetComponent<EventItem>().itemInfo.id != 0)
                {
                    Add_Item(inventory_slot_list[i].transform.GetChild(0).GetComponent<EventItem>().itemInfo, 0);
                    DeleteItem(i);
                }
            }
        }

        for (int i = 10; i < 15; i++)
        {
            if (inventory_item_list[i].id != 0)
                return;
        }

        parse_information();
        FindObjectOfType<EventTooltip>().DeactivateAndDisableScroll(false);


        Main_Manager.Instance.SwitchState(StageState.Level);
    }
    public void Close()
    {
        if(Main_Manager.Instance.showTutorial)
            return;

        switch (Main_Manager.Instance.currentState)
        {
            case StageState.Merchant:
                parse_information();
                Main_Manager.Instance.ToggleButtonPanel(true);
                FindObjectOfType<EventTooltip>().Deactivate();
                gameObject.SetActive(false);
                break;
            default:
                parse_information();
                FindObjectOfType<EventTooltip>().DeactivateAndDisableScroll(false);
                Main_Manager.Instance.SwitchState(StageState.Level);
                break;
        }
    }


    #endregion

    #region Save
    private Item[] parse_inventory()
    {
        Item[] inventory = new Item[10];


        for (int a = 0; a < 10; a++)
        {
            //inventory[a] = inventory_item_list[a].ID;
            if (inventory_slot_list[a].transform.childCount > 0)
            {
                inventory[a] = inventory_slot_list[a].transform.GetChild(0).GetComponent<EventItem>().itemInfo;
                //Debug.Log(inventory_slot_list[a].transform.GetChild(0).GetComponent<InventoryItem>().itemInfo.Title);
            }
            else
                inventory[a] = new Item();
        }

        return inventory;
    }
    public void parse_information()
    {
        save_system.SetCharInventory(parse_inventory());
    }

    private void AddSavedItems()
    {

        for (int i = 0; i < SaveSystem.Instance.charInventory.Length; i++)
        {
            if (SaveSystem.Instance.charInventory[i] != null && SaveSystem.Instance.charInventory[i].id != 0)
                Add_Item(SaveSystem.Instance.charInventory[i], i);
        }
    }
    #endregion
}
