﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EventItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
{

    public Item itemInfo;
    public int slot;

    private EventInventory eventInventory;
    public EventTooltip tooltip;

    private bool isDraggble;
    [HideInInspector]
    public TooltipType tooltipType;

    private Vector3 offset;
    private Vector3 screenPoint;

    void Start()
    {
        eventInventory = GameObject.Find("EventInventory").GetComponent<EventInventory>();
        tooltip = FindObjectOfType<EventTooltip>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!isDraggble) return;

        FindObjectOfType<EventTooltip>().Deactivate();
        if (itemInfo != null)
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

            this.transform.SetParent(transform.parent.parent.parent, false);
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

    }
    public void OnDrag(PointerEventData eventData)
    {
        if (!isDraggble) return;

        for (int i = 0; i < eventInventory.inventory_slot_list.Count; i++)
        {
            eventInventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        }
        if (itemInfo != null)
        {
            Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
            transform.position = cursorPosition;
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        if (!isDraggble) return;

        gameObject.transform.SetParent(eventInventory.inventory_slot_list[slot].transform, false);
        transform.position = eventInventory.inventory_slot_list[slot].transform.position;
        GetComponent<RectTransform>().sizeDelta = new Vector2(94, 94);
        GetComponent<CanvasGroup>().blocksRaycasts = true;

    }
    public void OnPointerClick(PointerEventData eventData)
    {

        if (itemInfo.itemType == "Rune" && tooltip.scrollIsUsed)
        {
            if (SaveSystem.Instance.IdentifyRune(itemInfo))
            {
                tooltip.scrollIsUsed = false;
                tooltip.DestroyItem(tooltip.getItem());
            }
            else
                tooltip.scrollIsUsed = false;

        }
        else
            tooltip.scrollIsUsed = false;

        for (int i = 0; i < eventInventory.inventory_slot_list.Count; i++)
        {
            if (eventInventory.inventory_slot_list[i] != transform.parent)
            {
                eventInventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
            }
        }

        transform.parent.GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
        tooltip.Activate(this,tooltipType);

    }
    public void SetSlot(bool isDraggble, TooltipType tooltipType)
    {
        this.isDraggble = isDraggble;
        this.tooltipType = tooltipType;
    }
}
