﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class EventTooltip : MonoBehaviour
{
    #region Variables
    private EventInventory eventInventory;
    private GameObject tooltip;
    

    private EventItem currentItem;
    private TextMeshProUGUI tooltip_description;

    private Rune_Slot currentRuneSlot;

    private TextMeshProUGUI buttonText;

    private GameObject panel_tooltip;
    private GameObject potion_panel;
    private GameObject price_panel;

    [HideInInspector]
    public bool scrollIsUsed;
    #endregion
    void Start()
    {
        Init();
    }

    private void Init()
    {
        eventInventory = GetComponent<EventInventory>();

        tooltip = transform.getChild(gameObject, "Tooltip_Reward");

        potion_panel = transform.FindDeepChild("Potion_Panel").gameObject;

        panel_tooltip = transform.FindDeepChild("Panel_tooltip").gameObject;

        price_panel = transform.FindDeepChild("Price_Panel").gameObject;

        tooltip_description = transform.getChild(gameObject, "Tooltip_Description").GetComponent<TextMeshProUGUI>();

        buttonText = GameObject.Find("Button").transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        tooltip.SetActive(false);
    }

    private void Information_Set(EventItem eventItem)
    {
        if (currentItem.itemInfo.itemType != "Weapon")
            tooltip_description.GetComponent<TextMeshProUGUI>().text = "<color=orange><b><size=60>" + currentItem.itemInfo.itemName + "</color></b></size>\n" + currentItem.itemInfo.description;
        else
            tooltip_description.GetComponent<TextMeshProUGUI>().text = "<color=orange><b><size=60>" + currentItem.itemInfo.itemName + "</color></b></size>\n" + GetDescription(currentItem.itemInfo);

        potion_panel.SetActive(false);
        buttonText.transform.parent.gameObject.SetActive(true);
        price_panel.SetActive(false);
        panel_tooltip.SetActive(true);

        switch (eventItem.tooltipType)
        {
            case TooltipType.Buyable:
                price_panel.SetActive(true);
                price_panel.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = currentItem.itemInfo.price.ToString();
                buttonText.text = "Buy";
                break;
            case TooltipType.Sellable:
                price_panel.SetActive(true);
                price_panel.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = currentItem.itemInfo.price.ToString();
                buttonText.text = "Close";
                break;
            case TooltipType.Usable:
                switch (eventItem.itemInfo.itemType)
                {
                    case "Scroll":
                        buttonText.text = "Use";
                        break;
                    case "Treasure":                      
                        buttonText.text = "Use";
                        break;
                    case "Book":
                        buttonText.text = "Use";
                        break;
                    default:
                        buttonText.text = "Close";
                        break;
                }
                break;
        }
    }

    public void ButtonUse()
    {
        switch (currentItem.tooltipType)
        {
            case TooltipType.Buyable:
                if (SaveSystem.GetChar().GetStat("Gold").baseValue >= currentItem.itemInfo.price)
                {
                    if (eventInventory.Bought(currentItem.itemInfo))
                    {
                        SaveSystem.GetChar().GetStat("Gold").ChangeCurrent(-currentItem.itemInfo.price);
                        GameObject.Find("Gold_quantity").transform.Find("Amount").GetComponent<TextMeshProUGUI>().text = SaveSystem.GetChar().GetStat("Gold").baseValue.ToString();
                        //Deactivate(false);
                    }
                }
                else
                    tooltip_description.text = "<color=orange><b><size=60>" + currentItem.itemInfo.itemName + "</color></b></size>" + "\n<color=white><size=40>" + "Not enough gold"; 
                break;
            case TooltipType.Sellable:
                Deactivate();
                break;
            case TooltipType.Usable:
                switch (getItem().itemInfo.itemType)
                {
                    case "Scroll":
                        scrollIsUsed = true;
                        for (int i = 0; i < eventInventory.inventory_slot_list.Count; i++)
                        {
                            if (eventInventory.inventory_slot_list[i].transform.childCount > 0)
                            {
                                Item item = eventInventory.inventory_slot_list[i].transform.GetChild(0).GetComponent<EventItem>().itemInfo;

                                //If checking slot contains unidentified rune
                                if (item.itemType == "Rune" && !item.ContainsProperties())
                                {
                                    eventInventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
                                }
                            }
                        }
                        Deactivate();
                        break;
                    case "Book":
                        if (SaveSystem.GetChar().GetStat("Runeslots").baseValue < 6)
                            SaveSystem.GetChar().GetStat("Runeslots").ChangeCurrent(1);

                        DestroyItem(currentItem);
                        Deactivate();
                        break;
                    case "Treasure":
                        SaveSystem.GetChar().GetStat("Gold").ChangeCurrent(+currentItem.itemInfo.price);
                        DestroyItem(currentItem);
                        Deactivate();
                        GameObject.Find("Gold_quantity").transform.Find("Amount").GetComponent<TextMeshProUGUI>().text = SaveSystem.GetChar().GetStat("Gold").baseValue.ToString();
                        break;
                    default:
                        Deactivate();
                        break;
                }
                break;
        }     
    }
    public void Activate(EventItem eventItem,TooltipType type)
    {
        //Link for a deactivation
        currentItem = eventItem;

        tooltip.SetActive(true);
        Information_Set(eventItem);
    }

    public EventItem getItem()
    {
        return currentItem;
    }
    public void Deactivate()
    {
        if (currentItem)
            currentItem.transform.parent.GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        panel_tooltip.SetActive(false);
        tooltip.SetActive(false);
    }
    public void DestroyItem(EventItem eventItem)
    {
        Destroy(eventInventory.inventory_slot_list[eventItem.slot].transform.GetChild(0).gameObject);
        eventInventory.inventory_item_list[eventItem.slot] = new Item();
        Deactivate();
    }
    public void DeactivateAndDisableScroll(bool panelActive)
    {
        scrollIsUsed = false;
        for (int i = 0; i < eventInventory.inventory_slot_list.Count; i++)
        {
            eventInventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        }
        panel_tooltip.SetActive(false);
        tooltip.SetActive(false);
    }
   
    public void SetRuneSlot(Rune_Slot slot)
    {
        currentRuneSlot = slot;
    }
    public Rune_Slot GetRuneSlot()
    {
        return currentRuneSlot;
    }
    public string GetDescription(Item item)
    {
        string description = "";
        for (int i = 0; i < item.properties.Count; i++)
        {
            string sign = item.properties[i].value > 0 ? "+" : "";
            string percents = item.properties[i].drawPercents ? "%" : "";

            switch (item.properties[i].propertyType)
            {
                case PropertyType.Passive:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].name + "\n";
                    break;
                case PropertyType.Use:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].name + "\n";
                    break;
                case PropertyType.Crit:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].target + " " + item.properties[i].name + " After Crit\n";
                    break;
                case PropertyType.Block:
                    description += sign + item.properties[i].value + percents + " " + item.properties[i].target + " " + item.properties[i].name + " After Block\n";
                    break;
            }
        }
        return description;
    }
}
