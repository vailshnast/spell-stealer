﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class EventSlot : MonoBehaviour, IDropHandler, IPointerClickHandler
{

    private EventInventory eventInventory;
    public int item_id;
    [HideInInspector]
    public bool isDraggble;
    [HideInInspector]
    public TooltipType tooltipType;

    void Start()
    {
        eventInventory = GameObject.Find("EventInventory").GetComponent<EventInventory>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        EventItem buffered_item = eventData.pointerDrag.GetComponent<EventItem>();

        if(!isDraggble) return;

        if (eventInventory.inventory_item_list[item_id].id == 0)
        {
            eventInventory.inventory_item_list[buffered_item.slot] = new Item();
            eventInventory.inventory_item_list[item_id] = buffered_item.itemInfo;           
            buffered_item.slot = item_id;
        }
        else
        {
            if (transform.childCount > 0)
            {
                Transform duplicated_item = transform.GetChild(0);

                duplicated_item.GetComponent<EventItem>().slot = buffered_item.slot;
                duplicated_item.transform.SetParent(eventInventory.inventory_slot_list[buffered_item.slot].transform, false);
                duplicated_item.position = eventInventory.inventory_slot_list[buffered_item.slot].transform.position;
                buffered_item.slot = item_id;
                buffered_item.transform.SetParent(transform, false);
                buffered_item.transform.position = transform.position;

                eventInventory.inventory_item_list[buffered_item.slot] = duplicated_item.GetComponent<EventItem>().itemInfo;
               
                eventInventory.inventory_item_list[item_id] = buffered_item.itemInfo;
            }
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (transform.childCount == 0)
        {
            for (int i = 0; i < eventInventory.inventory_slot_list.Count; i++)
            {
                if (eventInventory.inventory_slot_list[i] != transform.parent)
                {
                    eventInventory.inventory_slot_list[i].GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
                }
            }
            FindObjectOfType<EventTooltip>().scrollIsUsed = false;
            FindObjectOfType<EventTooltip>().Deactivate();
        }
    }
    public void SetSlot(bool isDraggble, TooltipType _tooltipType)
    {
        this.isDraggble = isDraggble;
        this.tooltipType = _tooltipType;
    }
}
