﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class Statistics : MonoBehaviour {


    public static Statistics Instance { get; private set; }  
    public StatisticInfo statisticInfo { get; private set; }

    // Use this for initialization
	void Start ()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        statisticInfo = new StatisticInfo(
            new StatisticalUnit(100),
            new StatisticalUnit(50),
            new StatisticalUnit(50),
            new StatisticalUnit(1),
            new StatisticalUnit(50),
            new StatisticalUnit(20),
            new StatisticalUnit(20));
    }

    public void AddExpirience()
    {       
        statisticInfo.AddExpirience();
    }
}
public class StatisticInfo
{
    public StatisticalUnit bossesKilled { get; set; }
    public StatisticalUnit monstersKilled { get; set; }
    public StatisticalUnit roomsPassed { get; set; }
    public StatisticalUnit goldCollected { get; set; }
    public StatisticalUnit spellsStolen { get; set; }
    public StatisticalUnit successBlocks { get; set; }
    public StatisticalUnit critsLanded { get; set; }

    private readonly List<StatisticalUnit> statistics;
    public int TotalExp
    {
        get
        {
            return statistics.Sum(t => t.Exp);
        }
    }
    public StatisticInfo(StatisticalUnit bossesKilled, StatisticalUnit monstersKilled, StatisticalUnit roomsPassed, StatisticalUnit goldCollected, StatisticalUnit spellsStolen, StatisticalUnit successBlocks, StatisticalUnit critsLanded)
    {
        this.bossesKilled = bossesKilled;
        this.monstersKilled = monstersKilled;
        this.roomsPassed = roomsPassed;
        this.goldCollected = goldCollected;
        this.spellsStolen = spellsStolen;
        this.successBlocks = successBlocks;
        this.critsLanded = critsLanded;
        statistics = new List<StatisticalUnit>
        {
            bossesKilled,
            monstersKilled,
            roomsPassed,
            goldCollected,
            spellsStolen,
            successBlocks,
            critsLanded
        };
    }

    public void AddExpirience()
    {
        CharacterStats save = Database_Saves.Instance.GetHero();

        save.Experience += TotalExp;
        save.GetStat("Gold").SetBaseValue(SaveSystem.GetChar().GetStat("Gold").baseValue / 100 * save.GetPassive(2, PassiveType.General).currentLevel * 10);
        Database_Saves.Instance.Save();
        for (int i = 0; i < statistics.Count; i++)
        {
            statistics[i].ResetValue();
        }
    }
}
public class StatisticalUnit
{
    public  int value { get; set; }
    public int expMultiplyer { get; private set; }
    public string Value
    {
        get { return value >= 1000 ? Math.Round((float) (value / 1000.0), 1) + "K" : value.ToString(); }
    }
    public int Exp
    {
        get
        {
            return value * expMultiplyer;
        }
    }
    public StatisticalUnit(int expMultiplyer)
    {
        this.expMultiplyer = expMultiplyer;
        value = 0;
    }

    public void ResetValue()
    {
        value = 0;
    }
}