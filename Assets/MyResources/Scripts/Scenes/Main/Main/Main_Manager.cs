﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

using Random = UnityEngine.Random;


public class Main_Manager : MonoBehaviour
{
    #region References

    public static Main_Manager Instance { get; private set; }

    public StageState currentState { get; private set; }

    [HideInInspector]
    public LevelManager levelManager { get; set; }

    #region GameObjects

    private GameObject inventory;
    private GameObject skilltree;

    private GameObject spellStealTooltip;
    private GameObject restartTooltip;

    private GameObject background;
    private GameObject fade_panel;
    private GameObject experience;

    private GameObject button_panel;
    private GameObject eventInventory;
    private GameObject title;

    private GameObject reward;
    private GameObject chest;
    private GameObject merchant;
    private GameObject altar;
    private GameObject map;
    private GameObject spellList;

    #endregion


    private List<Button> buttons;

    private List<StageState> levels;

    public string selectedEvent;

    private Button currentButton;
    private Button previousButton;

    private bool skipping;
    private bool intialised;
    [HideInInspector]
    public bool showTutorial;

    #endregion

    #region Init

    void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        #region References

        inventory = transform.FindDeepChild("Inventory").gameObject;
        skilltree = transform.FindDeepChild("Skill_Tree").gameObject;
        spellStealTooltip = transform.FindDeepChild("Slot_Tooltip").gameObject;
        restartTooltip = transform.FindDeepChild("Reset_Tooltip").gameObject;
        background = GameObject.Find("Event Background");
        spellList = transform.FindDeepChild("Spell List").gameObject;
        fade_panel = transform.FindDeepChild("Global_Fade").gameObject;
        experience = transform.FindDeepChild("Experience").gameObject;
        button_panel = transform.FindDeepChild("Button_Panel").gameObject;
        eventInventory = transform.FindDeepChild("EventInventory").gameObject;
        reward = transform.FindDeepChild("Reward").gameObject;
        chest = transform.FindDeepChild("Chest").gameObject;
        merchant = transform.FindDeepChild("Merchant").gameObject;
        title = transform.FindDeepChild("Title_Bar").gameObject;
        altar = transform.FindDeepChild("Altar").gameObject;
        map = transform.FindDeepChild("Map").gameObject;

        #endregion

        buttons = new List<Button>
        {
            new Button("Inventory_Button", inventory),
            new Button("Tree_Button", skilltree),
            new Button("Spell_Steal_Button", spellStealTooltip),
            new Button("Move_Button", null),
            new Button("Exit_Button", null),
            new Button("Map_Button", null),
            new Button("Skip_Button", null),
            new Button("Options_Button", restartTooltip),
            new Button("Trash_Button", null),
            new Button("Spell_List_Button", spellList)
        };
        GetButton("Move_Button").button.SetActive(false);
        GetButton("Spell_Steal_Button").button.SetActive(false);
        GetButton("Tree_Button").button.SetActive(false);
        GetButton("Skip_Button").button.SetActive(false);
        GetButton("Exit_Button").button.SetActive(false);
        GetButton("Trash_Button").button.SetActive(false);
        GetButton("Spell_List_Button").button.SetActive(false);

        experience.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text =
            Database_Saves.Instance.GetHero().Experience.ToString();


        selectedEvent = "Mage Guild";

        Item weapon = Database_Items.GetItemByName(Database_Saves.Instance.GetHero().weaponName);
        weapon.Use(true, PropertyType.Passive);
        levelManager = new LevelManager(30);
        currentState = StageState.Map;


        showTutorial = Database_Saves.Instance.GetHero().showTutorial;

        Database_Saves.Instance.GetHero().showTutorial = false;
        Database_Saves.Instance.Save();

        intialised = true;
    }

    void OnEnable()
    {
        if (!intialised)
            return;


        SwitchState(currentState == StageState.Battle ? StageState.Reward : StageState.Level);
    }

    int Count()
    {
        int count = 0;
        for (int i = 0; i < SaveSystem.Instance.charInventory.Length; i++)
        {
            if (SaveSystem.Instance.charInventory[i].id != 0)
                count++;
        }
        return count;
    }
    #endregion
    #region State
    public void SwitchState(StageState state)
    {
        if (currentState == state)
        {
            Debug.Log("same state");
            return;
        }

        ExitState(currentState);

        EnterState(state);

        currentState = state;
    }
    private void ExitState(StageState state)
    {
        switch (state)
        {
            case StageState.Level:               
                SetSprite("Opened Door");
                break;
            case StageState.Battle:
                fade_panel.GetComponent<Image>().color = Color.clear;
                fade_panel.GetComponent<Image>().enabled = false;
                break;
            case StageState.Reward:
                reward.SetActive(false);
                Statistics.Instance.statisticInfo.roomsPassed.value++;
                break;
            case StageState.Altar:
                altar.SetActive(false);
                Statistics.Instance.statisticInfo.roomsPassed.value++;
                break;
            case StageState.Chest:
                chest.SetActive(false);             
                Statistics.Instance.statisticInfo.roomsPassed.value++;
                break;
            case StageState.Merchant:
                merchant.SetActive(false);             
                Statistics.Instance.statisticInfo.roomsPassed.value++;
                break;
        }
    }
    private void EnterState(StageState state)
    {
        switch (state)
        {
            case StageState.Level:

                if (currentState == StageState.Map)
                    StartCoroutine(FadeEvent(1.25f, state));
                else
                {
                    GetButton("Options_Button").button.SetActive(true);
                    GetButton("Move_Button").button.SetActive(true);
                    GetButton("Spell_Steal_Button").button.SetActive(true);
                    GetButton("Spell_List_Button").button.SetActive(true);

                    previousButton = null;

                    title.SetActive(false);
                    button_panel.SetActive(true);
                    GetButton("Skip_Button").button.SetActive(false);
                    eventInventory.SetActive(false);

                    HighlightSkillList();

                    SetSprite("Closed Door");
                }
                break;
            case StageState.Reward:
                title.SetActive(true);
                title.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Reward";
                title.transform.SetParent(reward.transform, false);
                title.transform.SetSiblingIndex(1);
                SetSprite("Closed Door");
                button_panel.SetActive(false);
                reward.SetActive(true);
                reward.transform.GetChild(2).GetComponent<Image>().sprite = SaveSystem.GetSprite(SaveSystem.Instance.currentMonster.MonsterName + " Death");
                reward.transform.GetChild(2).GetComponent<Image>().SetNativeSize();
                SetInventory(true, true);
                break;
            default:
                CloseOpenedPanels();
                StartCoroutine(FadeEvent(1.25f, state));
                break;
        }
    }
    private IEnumerator FadeEvent(float time_to_fade, StageState state)
    {
        StageState previousState = currentState;

        float t = 0f;
        Image img = fade_panel.GetComponent<Image>();
        img.enabled = true;

        Color original_Color = img.color;
        Color target_Color = new Color(img.color.r, img.color.g, img.color.b, 1);

        img.raycastTarget = true;

        while (t <= 1)
        {
            t += Time.deltaTime / time_to_fade;
            img.color = Color.Lerp(original_Color, target_Color, t);
            yield return null;
        }

        img.raycastTarget = false;

        switch (previousState)
        {
            case StageState.Map:
                GetButton("Map_Button").button.SetActive(false);
                GetButton("Options_Button").button.SetActive(false);
                map.SetActive(false);
                DisableSubMenus();
                break;
            case StageState.SkillTree:
                experience.SetActive(false);
                GetButton("Exit_Button").button.SetActive(false);
                GetButton("Tree_Button").button.SetActive(false);
                DisableSubMenus();
                break;
        }
        #region Enter

        switch (state)
        {
            case StageState.Chest:
                title.SetActive(true);
                title.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Locked Chest";
                title.transform.SetParent(chest.transform, false);
                title.transform.SetSiblingIndex(1);
                chest.SetActive(true);
                img.color = original_Color;
                SetSprite("Closed Door");
                GetButton("Skip_Button").button.SetActive(true);
                break;
            case StageState.Merchant:
                title.SetActive(true);
                title.transform.SetParent(merchant.transform, false);
                title.transform.SetSiblingIndex(1);
                title.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Merchant";

                merchant.SetActive(true);
                img.color = original_Color;
                SetSprite("Closed Door");
                GetButton("Skip_Button").button.SetActive(true);

                break;
            case StageState.Altar:
                title.SetActive(true);
                title.transform.SetParent(altar.transform, false);
                title.transform.SetSiblingIndex(1);
                altar.SetActive(true);
                img.color = original_Color;
                SetSprite("Closed Door");
                GetButton("Skip_Button").button.SetActive(true);
                break;
            case StageState.Map:
                SetSprite("Map Background");
                GetButton("Map_Button").button.SetActive(true);
                GetButton("Options_Button").button.SetActive(true);
                map.SetActive(true);
                img.ChangeAlpha(0);
                break;
            case StageState.SkillTree:
                SetSprite("Tree Background");
                GetButton("Tree_Button").button.SetActive(true);
                GetButton("Options_Button").button.SetActive(false);
                GetButton("Exit_Button").button.SetActive(true);
                experience.SetActive(true);
                img.ChangeAlpha(0);
                break;
            case StageState.Level:
                GetButton("Options_Button").button.SetActive(true);
                GetButton("Move_Button").button.SetActive(true);
                GetButton("Spell_List_Button").button.SetActive(true);
                GetButton("Spell_Steal_Button").button.SetActive(true);

                HighlightSkillList();

                previousButton = null;

                title.SetActive(false);
                button_panel.SetActive(true);
                GetButton("Skip_Button").button.SetActive(false);
                eventInventory.SetActive(false);

                SetSprite("Closed Door");
                img.ChangeAlpha(0);
                if (showTutorial)
                    Tutorial.Instance.Procceed();
                break;          
            case StageState.Final:
                SceneSwitcher.Instance.Load_Scene("Final");
                break;
            case StageState.Battle:
                if(showTutorial)
                Tutorial.Instance.Procceed();
                SceneSwitcher.Instance.Load_Scene("Battle");
                SetSprite("Closed Door");
                break;
        }
        #endregion
    }
    #endregion
    #region Button
    public void ToggleRestartTooltip()
    {
        if (showTutorial)
            return;

        if (restartTooltip.activeInHierarchy)
        {
            restartTooltip.SetActive(false);
        }

        SwitchButton(GetButton("Options_Button"));
    }
    public void Reset()
    {
        SceneManager.LoadScene(0);
    }
    public void ToggleSpellList()
    {
        if(showTutorial)
            return;

        if (spellList.activeInHierarchy)       
            SpellList.Instance.Close();
        SwitchButton(GetButton("Spell_List_Button"));
    }
    public void ToggleInventory()
    {
        if (showTutorial)
            return;

        if (inventory.activeInHierarchy)
        {
            Inventory.Instance.parse_information();
            InventoryTooltip.Instance.DeactivateAndDisableScroll(false);
            
            GetButton("Trash_Button").button.SetActive(false);
        }
        else if (currentState != StageState.Map && currentState != StageState.SkillTree)
            GetButton("Trash_Button").button.SetActive(true);

        SwitchButton(GetButton("Inventory_Button"));

    }
    public void ToggleSkillTree()
    {
        if (showTutorial)
            return;

        if (skilltree.activeInHierarchy)
        {
            SkillTreeInfo.Instance.CloseToolTip();
        }

        SwitchButton(GetButton("Tree_Button"));       
    }
    public void ToggleSpellStealTooltip()
    {
        if (showTutorial)
            return;

        SwitchButton(GetButton("Spell_Steal_Button"));
        spellStealTooltip.transform.FindDeepChild("Tooltip_Text").GetComponent<TextMeshProUGUI>().text = "<color=orange>Spell Steal</color>\nConsumes mana to disrupt and steal opponents spell";
    }
    public void CloseTooltip()
    {
        if (currentButton == null) return;
        currentButton.obj.SetActive(false);
        currentButton.button.GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
        currentButton.active = false;
    }
    public void Skip()
    {
        if (skipping) return;

        StartCoroutine(SkipCoroutine());
    }
    public void ToggleButtonPanel(bool active)
    {
        button_panel.SetActive(active);
    }
    private IEnumerator SkipCoroutine()
    {
        Graphic[] graphicComponents = { };

        CloseOpenedPanels();
        ToggleButtons(false);

        skipping = true;

        Graphic img = null;

        switch (currentState)
        {
            case StageState.Chest:
                graphicComponents = chest.GetComponentsInChildren<Graphic>();
                img = graphicComponents.FirstOrDefault(t => t.name == "Chest_Image");
                break;
            case StageState.Merchant:
                graphicComponents = merchant.GetComponentsInChildren<Graphic>();
                img = graphicComponents.FirstOrDefault(t => t.name == "Merchant_Image");
                break;
            case StageState.Altar:
                graphicComponents = altar.GetComponentsInChildren<Graphic>();
                img = graphicComponents.FirstOrDefault(t => t.name == "Altar_Image");
                break;
        }       

        for (int i = 0; i < graphicComponents.Length; i++)
        {
            graphicComponents[i].CrossFadeAlpha(0, 1.25f, true);
        }
        img.raycastTarget = false;
        yield return new WaitForSeconds(1.25f);
        img.raycastTarget = true;
        ToggleButtons(true);
        skipping = false;
        SwitchState(StageState.Level);
    }
    public void ToggleButtons(bool active)
    {
        Graphic[] buttons = button_panel.GetComponentsInChildren<Graphic>();

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].raycastTarget = active;
        }
    }
    private void SwitchButton(Button btn)
    {
        currentButton = btn;


        if (previousButton != null && previousButton != currentButton)
        {
            if (previousButton.name == "Inventory_Button")
            {
                Inventory.Instance.parse_information();
                InventoryTooltip.Instance.DeactivateAndDisableScroll(false);
                GetButton("Trash_Button").button.SetActive(false);
            }

            previousButton.obj.SetActive(false);
            previousButton.button.GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
            previousButton.active = false;
        }
        if (btn.active)
        {
            currentButton.obj.SetActive(false);
            currentButton.button.GetComponent<Image>().sprite = SaveSystem.GetSprite("BasicSlot");
            currentButton.active = false;
        }
        else
        {
            currentButton.obj.SetActive(true);
            currentButton.button.GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
            currentButton.active = true;
        }
        previousButton = currentButton;
    }
    public void CloseOpenedPanels()
    {
        if (inventory.activeInHierarchy)
        {
            Inventory.Instance.parse_information();

            SwitchButton(GetButton("Inventory_Button"));
            GetButton("Trash_Button").button.SetActive(false);
            return;
        }

        if (spellList.activeInHierarchy)
        {
            SkillTreeInfo.Instance.CloseToolTip();
            SwitchButton(GetButton("Spell_List_Button"));
            
        }

        CloseTooltip();  
    }
    public Button GetButton(string name)
    {
        return buttons.FirstOrDefault(b => b.name == name);
    }
    #endregion
    #region Other
    public void Activate_Event()
    {
        switch (currentState)
        {
            case StageState.SkillTree:
                SwitchState(StageState.Level);
                break;
            case StageState.Level:
                SwitchState(levelManager.Move());
                break;
        }
    }
    private void DisableSubMenus()
    {
        if (inventory.activeInHierarchy)
        {
            Inventory.Instance.parse_information();
            InventoryTooltip.Instance.DeactivateAndDisableScroll(false);
            SwitchButton(GetButton("Inventory_Button"));
            GetButton("Trash_Button").button.SetActive(false);
        }
        if (skilltree.activeInHierarchy)
        {
            SkillTreeInfo.Instance.CloseToolTip();
            SwitchButton(GetButton("Tree_Button"));
        }
    }
    public void MovePointer(string name)
    {
        if (showTutorial)
        {
            if (selectedEvent == "Undead Lair")         
                return;
        }         

        Image image = transform.FindDeepChild(selectedEvent).GetComponent<Image>();

        image.ChangeAlpha(0.5f);

        selectedEvent = name;

        image = transform.FindDeepChild(selectedEvent).GetComponent<Image>();

        image.ChangeAlpha(1f);

        transform.FindDeepChild("Pointer").GetComponent<RectTransform>().anchoredPosition = image.GetComponent<RectTransform>().anchoredPosition;

        if (showTutorial)
        {
            if (selectedEvent == "Undead Lair")
            {
                GetButton("Map_Button").button.GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
                Tutorial.Instance.Procceed();               
            }
        }
    }
    public void EnterEvent()
    {
        if (showTutorial)
        {
            if (selectedEvent == "Undead Lair")
            {
                SwitchState(StageState.Level);
                return;
            }
            return;
        }

        switch (selectedEvent)
        {
            case "Mage Guild":
                SwitchState(StageState.SkillTree);
                break;
            case "Undead Lair":
                SwitchState(StageState.Level);
                break;
            case "Order of Light":

                break;
            case "Abyss":

                break;
        }
    }

    private void HighlightSkillList()
    {
        List<SpellInfo> spells = Database_Saves.Instance.GetHero().spellDatabase;

        if (spells.Any(t => t.stolen && !t.viewed))
        {
            GetButton("Spell_List_Button").button.GetComponent<Image>().sprite = SaveSystem.GetSprite("YellowSlot");
        }
    }
    public void EnterMap()
    {
        SwitchState(StageState.Map);
    }
    public void SetInventory(bool active,bool takeAll)
    {
        eventInventory.SetActive(active);
        eventInventory.transform.FindDeepChild("Take_All_Button").gameObject.SetActive(takeAll);
    }

    public void SetSprite(string name)
    {

        int currentLevel = levelManager.currentLevel;

        switch (name)
        {
            case "Opened Door":
                currentLevel--;
                if (currentLevel.IsBetween(0, 9))
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Graveyard Opened");
                else if (currentLevel.IsBetween(10, 19))
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Crypt Opened");
                else if (currentLevel.IsBetween(20, 40))
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Catacombs Opened");
                else
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Boss");
                break;
            case "Closed Door":
                if (currentLevel.IsBetween(0, 9))
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Graveyard Closed");
                else if (currentLevel.IsBetween(10, 19))
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Crypt Closed");
                else if (currentLevel.IsBetween(20, 40))
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Catacombs Closed");
                else
                    background.GetComponent<Image>().sprite = SaveSystem.GetSprite("Boss");
                break;
            default:
                background.GetComponent<Image>().sprite = SaveSystem.GetSprite(name);
                break;
        }


        //background.GetComponent<Image>().SetNativeSize();
    }

    #endregion
}
public class Button
{
    public string name;
    public GameObject obj;
    public GameObject button;
    public bool active;

    public Button(string name, GameObject obj)
    {
        active = false;
        this.name = name;
        this.obj = obj;
        button = GameObject.Find(name);
    }
}
public class LevelManager
{
    public int currentLevel { get; private set; }

    private StageState[] levels { get; set; }

    public LevelManager(int levelAmount)
    {
        levels = new StageState[levelAmount + 2];
        Dictionary<StageState, int> levelFrequency = new Dictionary<StageState, int>
        {
            {StageState.Chest, 60},
            {StageState.Altar, 40}
        };

        #region MapGeneration

        for (int i = 0; i < levels.Length; i++)
        {
            if ((i + 1) % 10 == 0)
                levels[i] = StageState.Merchant;
            else
                levels[i] = StageState.Battle;
        }

        int checkpoints = levelAmount / 10;
        if(!Database_Saves.Instance.GetHero().showTutorial)
        {
            for (int i = 0; i < checkpoints; i++)
            {
                int eventCount = Random.Range(2, 4);

                for (int j = 0; j < eventCount; j++)
                {
                    StageState randomEvent = WeightedRandomizer.From(levelFrequency).Random();

                    int index = Random.Range(i * 10, (i + 1) * 10 - 1);

                    if (isAbleToSpawnEvent(index))
                    {
                        levels[index] = randomEvent;
                        continue;
                    }

                    j--;
                }
            }
        } 
        

        for (int i = 0; i < levels.Length; i++)
        {
            Debug.Log(levels[i]);
        }

        #endregion

        levels[levels.Length - 2] = StageState.Battle;
        levels[levels.Length - 1] = StageState.Final;
    }

    private bool isAbleToSpawnEvent(int index)
    {       
        if (levels[index] == StageState.Altar || levels[index] == StageState.Chest)
            return false;

        if (index > 0 && (levels[index - 1] == StageState.Altar || levels[index - 1] == StageState.Chest))
            return false;

        if (levels[index + 1] == StageState.Altar || levels[index + 1] == StageState.Chest)
            return false;

        return true;
    }

    public StageState Move()
    {        
        currentLevel++;
        Debug.Log("cur level: " + currentLevel);
        return levels[currentLevel-1];
    }
}