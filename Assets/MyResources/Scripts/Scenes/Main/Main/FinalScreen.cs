﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using  TMPro;
using UnityEngine.SceneManagement;

public class FinalScreen : MonoBehaviour {
    private TextMeshProUGUI stats_text;

    private bool intitialized;
    // Use this for initialization
    void Start () {
	    stats_text = GameObject.Find("Stats").GetComponent<TextMeshProUGUI>();
        ShowStatistics();
        intitialized = true;
    }

    void OnEnable()
    {
        if (intitialized)
            ShowStatistics();
    }

    private void ShowStatistics()
    {
        StatisticInfo statisticInfo = Statistics.Instance.statisticInfo;
        statisticInfo.goldCollected.value = SaveSystem.GetChar().GetStat("Gold").baseValue;
        stats_text.text =
            "<align=center><pos=-10%><line-height=270%><size=60>Escaped</size></align>\n" +
            "</line-height>" +
            "Bosses killed" + "<pos=75%>" + statisticInfo.bossesKilled.Value + "\n" +
            "Monsters killed" + "<pos=75%>" + statisticInfo.monstersKilled.Value + "\n" +
            "Rooms passed" + "<pos=75%>" + statisticInfo.roomsPassed.Value + "\n" +
            "Gold collected" + "<pos=75%>" + statisticInfo.goldCollected.Value + "\n" +
            "Spells stealed" + "<pos=75%>" + statisticInfo.spellsStolen.Value + "\n" +
            "Succsess blocks" + "<pos=75%>" + statisticInfo.successBlocks.Value + "\n" +
            "Crits landed" + "<pos=75%>" + statisticInfo.critsLanded.Value + "\n" +
            "<line-height=90%>\n" +
            "IN TOTAL: +" + statisticInfo.TotalExp;
    }

    public void Restart()
    {
        SaveSystem.isPlayerDead = false;
        SceneManager.LoadScene(0);
    }

}
