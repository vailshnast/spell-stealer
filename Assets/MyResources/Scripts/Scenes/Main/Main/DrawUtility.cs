﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using LitJson;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Vectrosity;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class DrawUtility : MonoBehaviour
{
    #region Variables

    public static DrawUtility Instance { get; private set; }

    public float brushRadius;
    private float distance;
    public float distanceTolerance;
    public float pointsTolerance;

    private bool draw;

    [HideInInspector]
    public bool isGameStarted;
    private bool drawStarted;
    private float totalDistance;

    public ParticleSystem particle;

    public VectorObject2D line;
    List<SymbolPoint> points = new List<SymbolPoint>();
    private Vector2 lastPosition;

    private int symbolsDrawn;

    private SymbolDatabase symbolDatabase;
    private Symbol currentSymbol;

    private bool initialized;

    #endregion
    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;

        GetComponent<Image>().sprite = SaveSystem.GetSprite("Lamda");
        GetComponent<Image>().enabled = false;
        TextAsset txtAsset = (TextAsset)Resources.Load("Other/Symbols", typeof(TextAsset));
        symbolDatabase = JsonMapper.ToObject<SymbolDatabase>(JsonMapper.ToObject(txtAsset.text).ToJson());

        if (Application.platform == RuntimePlatform.Android)
            brushRadius = brushRadius * ((float)Screen.width / 720);

        initialized = true;
    }

    void OnEnable()
    {
        if(initialized)
            GetComponent<Image>().CrossFadeAlpha(0, 0f, true);
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && drawStarted)
        {
            draw = true;
            
            
            lastPosition = Input.mousePosition;

            Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 4));
            particle.transform.position = new Vector3(pos.x, pos.y, 4);
             
        }
        if (Input.GetMouseButtonUp(0) && draw && Battle_Manager.Instance.current_State == BattleState.Channeling)
            Check();

        if (draw)
            CheckPoints();
        
    }
    private void CheckPoints()
    {
        if(lastPosition.x == Input.mousePosition.x && lastPosition.y == Input.mousePosition.y)
            return;

        Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 4));
        if (!particle.gameObject.activeInHierarchy)
        {           
            particle.transform.position = new Vector3(pos.x, pos.y, 4);
            particle.gameObject.SetActive(true);
        }
        else
        particle.transform.position = new Vector3(pos.x, pos.y, 4);


        Vector2 newPosition = Input.mousePosition;

        distance += (newPosition - lastPosition).magnitude;

        lastPosition = Input.mousePosition;

        for (int i = 0; i < points.Count; i++)
        {
            if (Vector2.Distance(Input.mousePosition, points[i].position) < brushRadius)
            {
                points[i].success = true;
                line.vectorLine.SetColor(new Color32(0, 255, 0, 255), i, i);
            }                
        }
        
    }
    private void Check()
    {
        int successPoints = 0;
        for (int i = 0; i < points.Count; i++)
        {
            if (points[i].success)
            {
                points[i].success = false;
                successPoints++;
            }
        }
        

        bool success = successPoints >= points.Count - points.Count / 100 * pointsTolerance && distance <= totalDistance + totalDistance / 100 * distanceTolerance;       

       /* Debug.Log("min successPoints: " + (points.Count - points.Count / 100 * pointsTolerance) + " max Distance: " + (totalDistance + totalDistance / 100 * distanceTolerance));
        Debug.Log("all points: " + points.Count);
        Debug.Log("success points: " + successPoints);
        Debug.Log("total points distance: " + totalDistance);
        Debug.Log("traveled mouse distance: " + distance);
        Debug.Log("success: " + success);
       */    
        distance = 0;
        draw = false;

        if (success)
        {
            symbolsDrawn++;
            if (symbolsDrawn == Battle_Manager.Instance.current_Spell.symbolCount)
            {
                Battle_Manager.Instance.StealSpell(true);
                ToggleGame(false,currentSymbol.name+ " Effect");
                symbolsDrawn = 0;
                line.vectorLine.SetColor(new Color32(255, 255, 255, 0));
                transform.GetChild(0).GetComponent<Image>().raycastTarget = false;
                drawStarted = false;
                
            }
            else
            {
                SetupSymbol();
                line.vectorLine.SetColor(new Color32(255, 0, 0, 255));
                particle.gameObject.SetActive(false);
            }
        }
        else
        {
            particle.gameObject.SetActive(false);
            line.vectorLine.SetColor(new Color32(255, 0, 0, 255));
        }
            


    }
    public void ToggleGame(bool toggle, string symbolName = null)
    {
        if (toggle)
        {
            isGameStarted = true;
            SetupSymbol();
            GetComponent<Image>().CrossFadeAlpha(1, 0, true);
            GetComponent<Image>().enabled = true;
            transform.GetChild(0).GetComponent<Image>().raycastTarget = true;
        }
        else
        {
            isGameStarted = false;
            distance = 0;
            draw = false;
            symbolsDrawn = 0;
            line.vectorLine.SetColor(new Color32(255, 255, 255, 0));
            transform.GetChild(0).GetComponent<Image>().raycastTarget = false;

            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].success)
                {
                    points[i].success = false;
                }
            }
            if (symbolName != null)                
            {
                GetComponent<Image>().sprite = SaveSystem.GetSprite(symbolName);
                GetComponent<Image>().SetNativeSize();
                GetComponent<Image>().CrossFadeAlpha(0, 1f, true);
            }
            else
                GetComponent<Image>().enabled = false;

            if (!drawStarted)
                return;
            //StartCoroutine("Stop");
            particle.gameObject.SetActive(false);
            drawStarted = false;
        }
    }
    private IEnumerator Stop()
    {        
        if (particle.GetComponent<ParticleSystem>().isPlaying)
            particle.GetComponent<ParticleSystem>().Stop();

        while (particle.GetComponent<ParticleSystem>().particleCount > 1)
        {
            yield return null;
        }
        //Debug.Log("1");
        particle.gameObject.SetActive(false);
    }
    public void StartDraw()
    {
        Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 4));
        particle.transform.position = new Vector3(pos.x, pos.y, 4);
        particle.gameObject.SetActive(true);       
        drawStarted = true;
    }
    private Symbol RandomSymbol(SpellType type)
    {
        Symbol currentSymbol;
        List<Symbol> sortedSymbols = symbolDatabase.symbols.Where(t => t.type == type && !t.IsSymbolUsed()).ToList();
        
        if (sortedSymbols.Count == 0)
        {
            for (int i = 0; i < symbolDatabase.symbols.Count; i++)
            {
                if (symbolDatabase.symbols[i].type == type)
                {
                    symbolDatabase.symbols[i].SetUsage(false);
                    sortedSymbols.Add(symbolDatabase.symbols[i]);
                    Debug.Log(sortedSymbols.Count);
                }
            }
            currentSymbol = sortedSymbols[Random.Range(0, sortedSymbols.Count)];
            currentSymbol.SetUsage(true);
            return currentSymbol;
        }

        
        currentSymbol = sortedSymbols[Random.Range(0, sortedSymbols.Count)];
        currentSymbol.SetUsage(true);
        return currentSymbol;
    }
    private void SetupSymbol()
    {
        currentSymbol = RandomSymbol(Battle_Manager.Instance.current_Spell.type);

        GetComponent<Image>().sprite = SaveSystem.GetSprite(currentSymbol.name);
        GetComponent<Image>().SetNativeSize();

        List<Vector2> corners = new List<Vector2>();

        for (int i = 0; i < currentSymbol.points.Count; i++)
        {
            corners.Add(Camera.main.WorldToScreenPoint(new Vector3((float) currentSymbol.points[i].x, (float) currentSymbol.points[i].y, 0)));
        }

        List<Vector2> linePoints = new List<Vector2>();

        const int KOEF = 3;
        points.Clear();
        for (int i = 0; i < corners.Count - 1; i++)
        {
            float length = (corners[i] - corners[i + 1]).magnitude;
            float pointsCount = length / KOEF;
            for (int j = 0; j < pointsCount; j++)
            {
                points.Add(new SymbolPoint(Vector2.Lerp(corners[i], corners[i + 1], (float)1 / pointsCount * j)));
                linePoints.Add(Vector2.Lerp(corners[i], corners[i + 1], (float)1 / pointsCount * j));
            }
        }
        totalDistance = 0;
        for (int i = 0; i < points.Count - 1; i++)
        {
            totalDistance += (points[i].position - points[i + 1].position).magnitude;
        }

        line.vectorLine.SetColor(new Color32(255, 0, 0, 255));
        line.vectorLine.points2 = linePoints;
        line.vectorLine.Draw();        
    }
}

public class SymbolPoint
{
    public Vector2 position;
    public bool success;

    public SymbolPoint(Vector2 position)
    {
        this.position = position;
    }
}


