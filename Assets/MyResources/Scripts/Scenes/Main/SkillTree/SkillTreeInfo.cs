﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class SkillTreeInfo : MonoBehaviour
{
    public static SkillTreeInfo Instance;

    private List<Passive> passives;
    private int[] passiveLevels;
    private int currentLevel;
    private GameObject tooltip;
    private int currentExp;
    public Passive currentPassive;

    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;
    }




    void Start()
    {
        tooltip = transform.FindDeepChild("Tooltip_Tree").gameObject;
        InitialisePassives();
    }

    #region Passives

    public void InitialisePassives()
    {
        passives = new List<Passive>
        {
            new Stat_Passive(new Passive_Info("Health", PassiveType.Main, 0, 5, 20, 100), SaveSystem.GetChar().GetStat("Health"), true),

            new Stat_Passive(new Passive_Info("Mana", PassiveType.Magic, 1, 5, 4, 500), SaveSystem.GetChar().GetStat("Mana")),
            new Stat_Passive(new Passive_Info("Magic", PassiveType.Magic, 2, 10, 10, 400), SaveSystem.GetChar().GetStat("Magic")),
            new Stat_Passive(new Passive_Info("Spells Mana Cost", PassiveType.Magic, 3, -1, 5, 1000), SaveSystem.GetChar().GetStat("Mana Cost")),
            new Stat_Passive(new Passive_Info("Charges", PassiveType.Magic, 4, 1, 10000), SaveSystem.GetChar().GetStat("Charges")),

            new Stat_Passive(new Passive_Info("Attack", PassiveType.Attack, 1, 2, 10, 400), SaveSystem.GetChar().GetStat("Damage")),
            new Stat_Passive(new Passive_Info("Defence", PassiveType.Attack, 3, 3, 10, 500), SaveSystem.GetChar().GetStat("Defence")),
            new Stat_Passive(new Passive_Info("Power", PassiveType.Attack, 2, 10, 10, 400), SaveSystem.GetChar().GetStat("Power")),
            new Stat_Passive(new Passive_Info("Crit", PassiveType.Attack, 4, -1, 5, 1500, "Hits to Crit"), SaveSystem.GetChar().GetStat("Crit")),

            new Passive(new Passive_Info("Alchemy", PassiveType.General, 1, 3, 750, "Potion")),
            new Passive(new Passive_Info("Gold Recover", PassiveType.General, 2, 10, 5, 500, "Recover gold")),
            new Passive(new Passive_Info("Rune Reader", PassiveType.General, 3, 1, 5500,"Identify Level")),
            new Stat_Passive(new Passive_Info("RuneSlot", PassiveType.General, 4, 4, 10000), SaveSystem.GetChar().GetStat("Runeslots"))

        };
        passiveLevels = new[] {1500, 7500, 16000, 40000};

        currentLevel = 0;

        SetTreeStats();
        Add_Tree_Items();
    }

    private Passive FindPassive(string name)
    {
        return passives.FirstOrDefault(b => b.info.passive_Name == name);
    }

    private Passive FindPassive(int tier, PassiveType type)
    {
        return passives.FirstOrDefault(b => b.info.passive_tier == tier && b.info.type == type);
    }


    #endregion

    #region Tree

    public void SaveTree(bool saveFiller)
    {
        List<Passive_Save> tree = passives.Select(t => new Passive_Save(t.info.type, t.info.maxLevel, t.currentLevel, t.info.passive_tier, t.isUnlocked)).ToList();


        if (saveFiller)
        {
            Image fade = GameObject.Find("Fade").GetComponent<Image>();
            Database_Saves.Instance.GetHero().SaveFiller(currentExp, fade.fillAmount,currentLevel);
        }


        Database_Saves.Instance.GetHero().SaveTree(tree);

        Database_Saves.Instance.Save();
    }

    public void InitSkillTree()
    {
        tooltip.SetActive(false);
        for (int i = 0; i < passives.Count; i++)
        {
            passives[i].SetSlot();
        }

        Image fade = GameObject.Find("Fade").GetComponent<Image>();

        List<Passive_Save> tree = Database_Saves.Instance.GetHero().tree;

        currentExp = Database_Saves.Instance.GetHero().currentExp;
        fade.fillAmount = (float) Database_Saves.Instance.GetHero().fadeFillerAmount;
        currentLevel = Database_Saves.Instance.GetHero().currentLevel;

        for (int i = 0; i < tree.Count; i++)
        {
            if (tree[i].unlocked)
            {
                Passive passive = FindPassive(tree[i].tier, tree[i].type);
                passive.Unlock();
            }
        }
    }

    private void SetTreeStats()
    {
        List<Passive_Save> tree = Database_Saves.Instance.GetHero().tree;

        if (tree == null)
        {
            SaveTree(false);
        }
        else
        {
            for (int i = 0; i < tree.Count; i++)
            {
                if (tree[i].unlocked)
                {
                    Passive passive = FindPassive(tree[i].tier, tree[i].type);
                    for (int j = 0; j < tree[i].currentLevel; j++)
                    {
                        passive.Load();
                    }
                }
            }

        }
    }

    #endregion

    public void ShowTooltip(string passiveName)
    {
        currentPassive = FindPassive(passiveName);
        tooltip.SetActive(true);

        SetText();
    }

    public void CloseToolTip()
    {
        tooltip.SetActive(false);
    }
    private void SetText()
    {
        string sign = currentPassive.info.upgradeValue > 0 ? "+" : "";

        string passive_Name = currentPassive.info.addtnl_Name ?? currentPassive.info.passive_Name;

        string name_descrp = currentPassive.info.passive_Name + " LvL " + currentPassive.currentLevel;

        string main_descrp = sign + currentPassive.currentLevel * currentPassive.info.upgradeValue + " " + passive_Name;

        string addtnl_descrp = sign + currentPassive.info.upgradeValue + " " + passive_Name + " per level)";

        tooltip.transform.FindDeepChild("Skill_Description").GetComponent<TextMeshProUGUI>().text = name_descrp + "\n" + main_descrp + "<line-height=50%>\n<size=75%>(" + addtnl_descrp;
        tooltip.transform.FindDeepChild("Exp_Ammount").GetComponent<TextMeshProUGUI>().text = currentPassive.ExpPrice().ToString();
    }

    private void Add_Tree_Items()
    {
        CharacterStats ch = Database_Saves.Instance.GetHero();

        for (int i = 0; i < SaveSystem.Instance.charInventory.Length; i++)
        {
            SaveSystem.Instance.charInventory[i] = new Item();
        }

        Passive_Save potions = ch.GetPassive(1, PassiveType.General);
        //Passive_Save runes = ch.GetPassive(3, PassiveType.General);

        for (int i = 0; i < potions.currentLevel; i++)
        {
            SaveSystem.Instance.AddItemInfo(2, 0);
        }
        /* switch (runes.currentLevel)
        {
            case 1:
                SaveSystem.Instance.AddItemInfo(3, 0);
                break;
            case 2:
                SaveSystem.Instance.AddItemInfo(4, 0);
                break;
            case 3:
                SaveSystem.Instance.AddItemInfo(5, 0);
                break;
        }*/
    }

    public void Upgrade()
    {
        if (currentPassive.isUnlocked && currentPassive.EnoughExp() && currentPassive.currentLevel != currentPassive.info.maxLevel)
        {
            Image fade = GameObject.Find("Fade").GetComponent<Image>();
            if (currentLevel < passiveLevels.Length)
            {
                if (currentExp + currentPassive.ExpPrice() > passiveLevels[currentLevel])
                {
                    currentLevel++;
                    fade.fillAmount = 1 - 0.2428f * currentLevel;

                    FindPassive(currentLevel, PassiveType.Attack).Unlock();
                    FindPassive(currentLevel, PassiveType.Magic).Unlock();
                    FindPassive(currentLevel, PassiveType.General).Unlock();

                    if (currentLevel < passiveLevels.Length)
                        fade.fillAmount -= (float)(currentPassive.ExpPrice() - (passiveLevels[currentLevel - 1] - currentExp)) / passiveLevels[currentLevel] * 0.2428f;

                    currentExp = currentPassive.ExpPrice() - (passiveLevels[currentLevel - 1] - currentExp);
                }
                else
                {
                    if (currentExp + currentPassive.ExpPrice() == passiveLevels[currentLevel])
                    {
                        //Debug.Log("Equal");

                        currentExp = 0;
                        currentLevel++;

                        FindPassive(currentLevel, PassiveType.Attack).Unlock();
                        FindPassive(currentLevel, PassiveType.Magic).Unlock();
                        FindPassive(currentLevel, PassiveType.General).Unlock();

                        fade.fillAmount = 1 - 0.2428f * currentLevel;
                    }
                    else
                    {
                        currentExp += currentPassive.ExpPrice();
                        if (currentLevel < passiveLevels.Length)
                            fade.fillAmount -= (float)currentPassive.ExpPrice() / passiveLevels[currentLevel] * 0.2428f;
                    }
                }
            }
            

            if (currentLevel >= passiveLevels.Length)
                fade.fillAmount = 0;


            currentPassive.Upgrade();
            SetText();
            SaveTree(true);
            Add_Tree_Items();
        }
    }
}

public class Passive
{
    public int currentLevel { get; private set; }
    public Passive_Info info { get; private set; }
    public GameObject slot { get; private set; }
    public bool isUnlocked { get; protected set; }
    public Passive(Passive_Info info)
    {
        this.info = info;
        //slot = GameObject.Find(info.passive_Name).transform.parent.gameObject;
        currentLevel = 0;
    }
    public virtual void Upgrade()
    {
        Upgraded();
    }
    public void Unlock()
    {
        isUnlocked = true;

        slot.transform.FindDeepChild(info.passive_Name).GetComponent<Image>().color = new Color(1, 1, 1, 1);
        slot.GetComponent<Image>().sprite = SaveSystem.GetSprite("SemiYellow");
        slot.transform.Find("Skill_Level").GetComponent<TextMeshProUGUI>().enabled = true;
    }
    public bool EnoughExp()
    {
        return Database_Saves.Instance.GetHero().Experience >= ExpPrice();
    }
    public int ExpPrice()
    {
        return info.experiencePrice + info.experiencePrice * currentLevel;
    }
    protected bool Upgraded()
    {

        if (currentLevel < info.maxLevel && EnoughExp())
        {
            Database_Saves.Instance.GetHero().Experience -= ExpPrice();
            GameObject.Find("Experience").transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = Database_Saves.Instance.GetHero().Experience.ToString();
            currentLevel++;
            //Debug.Log("Upgraded " + info.passive_Name + " " + currentLevel);

            SetTextValues();
            return true;
        }
        return false;
    }
    public virtual void Load()
    {
        if (currentLevel < info.maxLevel)
        {
            currentLevel++;
            //Debug.Log("Upgraded " + info.passive_Name + " " + currentLevel);
        }
    }
    public void SetTextValues()
    {
        slot.transform.Find("Skill_Level").GetComponent<TextMeshProUGUI>().text = currentLevel + "/" + info.maxLevel;
    }
    public void SetSlot()
    {
        slot = GameObject.Find(info.passive_Name).transform.parent.gameObject;
        slot.transform.Find("Skill_Level").GetComponent<TextMeshProUGUI>().text = currentLevel + "/" + info.maxLevel;
    }
}
public class Stat_Passive : Passive
{   
    private Stat stat { get; set; }

    public Stat_Passive(Passive_Info info, Stat stat) : base(info)
    {
        this.stat = stat;
    }
    public Stat_Passive(Passive_Info info, Stat stat,bool isUnlocked) : base(info)
    {
        this.stat = stat;
        this.isUnlocked = isUnlocked;
    }
    public override void Load()
    {
        base.Load();
        stat.AdjustMax(info.upgradeValue);
        stat.ChangeCurrent(info.upgradeValue);     
    }

    public override void Upgrade()
    {
        if (Upgraded())
        {
            //Debug.Log("Before " + stat.name + " " + stat.baseValue);
            stat.AdjustMax(info.upgradeValue);
            stat.ChangeCurrent(info.upgradeValue);           
            //Debug.Log("After " + stat.name + " " + stat.baseValue);
        }
    }
}
public class Passive_Info
{
    public PassiveType type { get; private set; }
    public int passive_tier { get; private set; }
    public string passive_Name { get; private set; }
    public string addtnl_Name { get; private set; }
    public int upgradeValue { get; private set; }
    public int maxLevel { get; private set; }
    public int experiencePrice { get; private set; }

    public Passive_Info(string passive_Name, PassiveType type, int passive_tier, int maxLevel, int experiencePrice)
    {
        this.passive_tier = passive_tier;
        this.passive_Name = passive_Name;

        upgradeValue = 1;
        this.maxLevel = maxLevel;
        this.experiencePrice = experiencePrice;

        this.type = type;
    }
    public Passive_Info(string passive_Name, PassiveType type, int passive_tier, int maxLevel, int experiencePrice, string addtnl_Name)
    {
        this.passive_tier = passive_tier;
        this.passive_Name = passive_Name;
        this.addtnl_Name = addtnl_Name;

        upgradeValue = 1;
        this.maxLevel = maxLevel;
        this.experiencePrice = experiencePrice;

        this.type = type;
    }
    public Passive_Info(string passive_Name, PassiveType type, int passive_tier, int upgradeValue, int maxLevel, int experiencePrice)
    {
        this.passive_tier = passive_tier;
        this.passive_Name = passive_Name;

        this.upgradeValue = upgradeValue;
        this.maxLevel = maxLevel;
        this.experiencePrice = experiencePrice;
        
        this.type = type;
    }
    public Passive_Info(string passive_Name, PassiveType type, int passive_tier, int upgradeValue, int maxLevel, int experiencePrice, string addtnl_Name)
    {
        this.passive_tier = passive_tier;
        this.passive_Name = passive_Name;
        this.addtnl_Name = addtnl_Name;

        this.upgradeValue = upgradeValue;
        this.maxLevel = maxLevel;
        this.experiencePrice = experiencePrice;

        this.type = type;
    }
}
public class Passive_Save
{
    public PassiveType type { get; set; }
    public int maxLevel { get; set; }
    public int currentLevel { get; set; }
    public bool unlocked { get; set; }
    public int tier { get; set; }
    public Passive_Save(PassiveType type, int maxLevel,int currentLevel, int tier,bool unlocked)
    {
        this.type = type;
        this.maxLevel = maxLevel;
        this.currentLevel = currentLevel;
        this.tier = tier;
        this.unlocked = unlocked;
    }

    public Passive_Save()
    {

    }
}